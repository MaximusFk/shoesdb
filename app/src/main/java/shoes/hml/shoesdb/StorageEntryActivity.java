package shoes.hml.shoesdb;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;

import java.util.HashMap;
import java.util.Map;

import shoes.hml.shoesdb.handlers.Storage;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 17.05.16.
 */
public class StorageEntryActivity extends Activity implements View.OnClickListener, View.OnLongClickListener {

    Storage.StorageEntity entity;
    HashMap<Button, Integer> sizeButtons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_storage_entity);
        entity = (Storage.StorageEntity) this.getIntent().getSerializableExtra(Storage.StorageEntity.ACCESS);
        ShoesItem item = (ShoesItem) this.getIntent().getSerializableExtra(ShoesItem.ACCESS);
        if(entity != null) {
            item = entity.getItem();
        }

        sizeButtons = new HashMap();
        GridView gridView = (GridView) findViewById(R.id._storage_sizes);
        for(int size = item.getMinSize(); size <= item.getMaxSize(); ++size) {
            Button button = new Button(this);
            button.setText(size + ": " + entity.getCountBySize(size));
            button.setOnClickListener(this);
            button.setOnLongClickListener(this);
            sizeButtons.put(button, size);
            gridView.addView(button);
        }
    }

    private int getSize(Button button) {
        for(Map.Entry<Button, Integer> entry : this.sizeButtons.entrySet()) {
            if(entry.getKey().hashCode() == button.hashCode())
                return entry.getValue();
        }
        return -1;
    }

    @Override
    public void onClick(View v) {
        if(v instanceof Button) {
            int size = getSize((Button) v);
            if(size == -1)
                return;
            entity.addCount(size, 1);
            ((Button) v).setText(size + ": " + entity.getCountBySize(size));
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }
}
