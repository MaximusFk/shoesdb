package shoes.hml.shoesdb.log;

import java.util.logging.Level;

/**
 * Created by maximusfk on 22.02.16.
 */
public class Logger {
    private static java.util.logging.Logger logger;

    public static void sendLog(String text) {
        logger.log(Level.WARNING, text);
    }

    public static void sendError(String text) {
        logger.log(Level.SEVERE, text);
    }
}
