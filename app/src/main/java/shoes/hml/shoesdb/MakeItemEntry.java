package shoes.hml.shoesdb;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import shoes.hml.shoesdb.adapters.ShoesItemListAdapter;
import shoes.hml.shoesdb.handlers.ShoesItemsList;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesItem;
import shoes.hml.shoesdb.views.ShoesItemView;

public class MakeItemEntry extends ActionBarActivity {

    ShoesItemListAdapter adapter;
    ShoesItemsList itemsList;
    ListView listView;
    ShoesItem item = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_item_entry);
        itemsList = (ShoesItemsList) getIntent().getSerializableExtra(ShoesItemsList.ACCESS);
        adapter = new ShoesItemListAdapter(this, itemsList);
        listView = (ListView) findViewById(R.id.listView3);
        listView.setAdapter(adapter);
        Toolbar bar = (Toolbar) findViewById(R.id.action_bar);
        Button accept = new Button(this);
        accept.setText(getString(R.string.answ_ok));
        accept.setBackgroundColor(Color.TRANSPARENT);
        accept.setTextColor(Color.WHITE);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item != null) {
                    Intent result = new Intent();
                    long count = AddShoesItem.getLong(((EditText) findViewById(R.id.itemCount)).getText().toString());
                    ItemEntry entry = new ItemEntry(item, count);
                    result.putExtra(ItemEntry.ACCESS, entry);
                    setResult(Main2Activity.ResultType.CREATE_ITEM_ENTRY.getId(), result);
                    finish();
                }
            }
        });
        Button cancel = new Button(this);
        cancel.setText(getString(R.string.answ_cancel));
        cancel.setBackgroundColor(Color.TRANSPARENT);
        cancel.setTextColor(Color.WHITE);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        bar.addView(accept);
        bar.addView(cancel);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listView.setSelection(position);
                resetSelected();
                setItemSelected(view);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(item == null)
                    return;
                for(int i = visibleItemCount; i > 0; --i) {
                    ShoesItemView view1 = (ShoesItemView) listView.getChildAt(firstVisibleItem++);
                    if(view1 == null)
                        continue;
                    if(view1.getItem().equals(item))
                        view1.setBackgroundColor(Color.GRAY);
                }
            }
        });
    }

    void setItemSelected(View view) {
        ShoesItemView itemView = (ShoesItemView) view;
        itemView.setBackgroundColor(Color.GRAY);
        item = itemView.getItem();
    }

    void resetSelected() {
        for(int i = 0; i < listView.getChildCount(); ++i) {
            ShoesItemView itemView = (ShoesItemView) listView.getChildAt(i);
            itemView.setBackgroundColor(Color.TRANSPARENT);
        }
    }
}
