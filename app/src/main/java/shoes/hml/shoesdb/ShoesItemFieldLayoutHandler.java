package shoes.hml.shoesdb;

import android.content.Context;
import android.graphics.Color;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;

import shoes.hml.shoesdb.handlers.MainHandler;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 10.04.16.
 */
public class ShoesItemFieldLayoutHandler implements View.OnClickListener, PopupWindow.OnDismissListener {

    Context parent;
    LinearLayout layout;
    PopupWindow popupWindow;
    ArrayList<ShoesItem> items = new ArrayList<>();
    MainHandler handler;
    ShoesItemField itemField;
    View defaultAnchor;

    public ShoesItemFieldLayoutHandler(Context parent, PopupWindow popupWindow, MainHandler handler) {
        this.parent = parent;
        this.popupWindow = popupWindow;
        this.handler = handler;
        layout = (LinearLayout) LayoutInflater.from(parent).inflate(R.layout.shoes_item_field_set, null);
        layout.findViewById(R.id.button3).setOnClickListener(this);
        layout.findViewById(R.id.button4).setOnClickListener(this);
        this.popupWindow.setContentView(layout);
        this.popupWindow.setOnDismissListener(this);
    }

    private void show(View anchor) {
        if(popupWindow != null) {
            if(popupWindow.isShowing())
                popupWindow.dismiss();
            EditText text = ((EditText) layout.findViewById(R.id.editText4));
            popupWindow.setWindowLayoutMode(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            popupWindow.setWidth(layout.getWidth());
            popupWindow.setHeight(layout.getHeight());
            popupWindow.showAsDropDown(anchor != null ? anchor : defaultAnchor, 0, -(anchor.getHeight() / 2));
            popupWindow.setFocusable(true);
            popupWindow.setClippingEnabled(true);
            popupWindow.update();
            text.setInputType(itemField.getInputType());
            if(!items.isEmpty())
                text.setText(getValue(itemField, items.get(0)));
            ((TextView)layout.findViewById(R.id.shoes_item_field_name)).setText(itemField.getId());
        }
    }

    public void show(View anchor, ShoesItem item, ShoesItemField field) {
        items.add(item);
        if(field != null)
            itemField = field;
        show(anchor);
    }

    public void show(View anchor, ArrayList<ShoesItem> items1, ShoesItemField field) {
        items = (ArrayList<ShoesItem>) items1.clone();
        if(field != null)
            itemField = field;
        show(anchor);
    }

    public void show() {
        show(defaultAnchor);
    }

    public boolean isShowing() {
        return popupWindow != null ? popupWindow.isShowing() : false;
    }

    public void dissmiss() {
        if(popupWindow != null && popupWindow.isShowing())
            popupWindow.dismiss();
    }

    private void setValue(String value, ShoesItemField field) {
        float ffield;
        int ifield;
        switch (field) {
            case Cost:
                ffield = AddShoesItem.getFloat(value);
                for(ShoesItem item : items) {
                    item.setCost(ffield);
                    handler.getItemsList().updateItem(item);
                }
                break;
            case Label:
                for(ShoesItem item : items) {
                    item.setLabel(value);
                    handler.getItemsList().updateItem(item);
                }
                break;
            case Manufacturer:
                for(ShoesItem item : items) {
                    item.setManufacturer(value);
                    handler.getItemsList().updateItem(item);
                }
                break;
            case MaxSize:
                ifield = AddShoesItem.getInt(value);
                for(ShoesItem item : items) {
                    item.setMaxSize(ifield);
                    handler.getItemsList().updateItem(item);
                }
                break;
            case MinSize:
                ifield = AddShoesItem.getInt(value);
                for(ShoesItem item : items) {
                    item.setMinSize(ifield);
                    handler.getItemsList().updateItem(item);
                }
                break;
            case Name:
                for(ShoesItem item : items) {
                    item.setName(value);
                    handler.getItemsList().updateItem(item);
                }
                break;
            case Retail:
                ffield = AddShoesItem.getFloat(value);
                for(ShoesItem item : items) {
                    item.setPriceRetail(ffield);
                    handler.getItemsList().updateItem(item);
                }
                break;
            case Whole:
                ffield = AddShoesItem.getFloat(value);
                for(ShoesItem item : items) {
                    item.setPriceWhole(ffield);
                    handler.getItemsList().updateItem(item);
                }
                break;
        }
    }

    public String getValue(ShoesItemField field, ShoesItem item) {
        StringBuilder builder = new StringBuilder();
        switch (field) {
            case Cost:
                return builder.append(item.getCost()).toString();
            case Label:
                return builder.append(item.getLabel()).toString();
            case Manufacturer:
                return builder.append(item.getManufacturer()).toString();
            case MaxSize:
                return builder.append(item.getMaxSize()).toString();
            case MinSize:
                return builder.append(item.getMinSize()).toString();
            case Name:
                return builder.append(item.getName()).toString();
            case Retail:
                return builder.append(item.getPriceRetail()).toString();
            case Whole:
                return builder.append(item.getPriceWhole()).toString();
        }
        return "";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button3:
                String value = ((EditText)layout.findViewById(R.id.editText4)).getText().toString();
                setValue(value, itemField);
            case R.id.button4:
                popupWindow.dismiss();
                break;
        }
    }

    public void setDefaultFieldType(ShoesItemField fieldType) {
        itemField = fieldType;
    }

    public void setDefaultAnchor(View anchor) {
        defaultAnchor = anchor;
    }

    public void addItem(ShoesItem item) {
        assert (item != null);
        items.add(item);
    }

    @Override
    public void onDismiss() {
        items.clear();
    }

    public enum ShoesItemField {
        Name(R.string.shoes_name, InputType.TYPE_TEXT_VARIATION_PERSON_NAME),
        MinSize(R.string.shoes_size_min, InputType.TYPE_CLASS_NUMBER),
        MaxSize(R.string.shoes_size_max, InputType.TYPE_CLASS_NUMBER),
        Label(R.string.shoes_label, InputType.TYPE_TEXT_VARIATION_PERSON_NAME),
        Manufacturer(R.string.shoes_manufacturer, InputType.TYPE_TEXT_VARIATION_PERSON_NAME),
        Whole(R.string.shoes_whole, InputType.TYPE_CLASS_NUMBER),
        Retail(R.string.shoes_retail, InputType.TYPE_CLASS_NUMBER),
        Cost(R.string.shoes_price, InputType.TYPE_CLASS_NUMBER);

        int id;
        int inputType;
        static class FieldAdapter extends BaseAdapter {

            View parent;

            public void setParent(View parent) {
                this.parent = parent;
            }

            @Override
            public int getCount() {
                return ShoesItemField.values().length;
            }

            @Override
            public Object getItem(int position) {
                return ShoesItemField.values()[position];
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public boolean hasStableIds() {
                return true;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = new TextView(Main2Activity.once);
                textView.setText(Main2Activity.once.getString(ShoesItemField.values()[position].getId()));
                textView.setBackgroundColor(Color.WHITE);
                textView.setMinHeight(45);
                if(parent != null)
                    textView.setMinWidth(parent.getWidth());
                return textView;
            }

            @Override
            public int getItemViewType(int position) {
                return 0;
            }

            @Override
            public int getViewTypeCount() {
                return 1;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }
        };

        private static FieldAdapter adapter = new FieldAdapter();

        public static BaseAdapter getAdapter() {
            return adapter;
        }

        public static BaseAdapter getAdapter(View parent) {
            adapter.setParent(parent);
            return adapter;
        }

        ShoesItemField(int id, int inputType) {
            this.id = id;
            this.inputType = inputType;
        }

        public int getInputType() {
            return inputType;
        }

        public int getId() {
            return id;
        }
    }
}
