package shoes.hml.shoesdb.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import shoes.hml.shoesdb.actions.Draft;
import shoes.hml.shoesdb.handlers.DraftList;

/**
 * Created by maximusfk on 24.03.16.
 */
public class DraftListAdapter extends BaseAdapter {

    Context parent;
    DraftList draftList;

    public DraftListAdapter(Context parent, DraftList list) {
        this.draftList = list;
        this.parent = parent;
    }

    @Override
    public int getCount() {
        return draftList.getItems().size();
    }

    @Override
    public Object getItem(int position) {
        return draftList.getItems().get(position);
    }

    @Override
    public long getItemId(int position) {
        return draftList.getItems().get(position).getID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = new TextView(this.parent);
        Draft draft = (Draft) getItem(position);
        //SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd/MM/YYYY");
        java.text.DateFormat format = java.text.DateFormat.getDateInstance(java.text.DateFormat.LONG);
        String text = draft.getLabel() + ": " + format.format(draft.getCustomDate()) + "\t\t" + draft.getCount();
        textView.setText(text);
        textView.setMinimumHeight(40);
        return textView;
    }
}
