package shoes.hml.shoesdb.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import shoes.hml.shoesdb.actions.EntityMove;
import shoes.hml.shoesdb.handlers.MoveList;

/**
 * Created by maximusfk on 17.04.16.
 */
public class MoveActionAdapter extends BaseAdapter implements ListAdapter {

    ArrayList<EntityMove> moves;
    Context parent;

    public MoveActionAdapter(Context parent, MoveList list, Class<? extends EntityMove> filter) {
        this.parent = parent;
        this.moves = list.getMoves(filter);
    }

    public MoveActionAdapter(Context parent, MoveList list, int moveTypeID) {
        this.parent = parent;
        this.moves = list.getMoves(moveTypeID);
    }

    public MoveActionAdapter(Context parent, ArrayList<EntityMove> moves) {
        this.parent = parent;
        this.moves = moves;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return moves.size();
    }

    @Override
    public Object getItem(int position) {
        return moves.get(position);
    }

    @Override
    public long getItemId(int position) {
        return moves.get(position).getID();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = new TextView(this.parent);
        EntityMove move = (EntityMove) getItem(position);
        textView.setText(move.toString());
        textView.setMinHeight(45);
        return textView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return moves.isEmpty();
    }
}
