package shoes.hml.shoesdb.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import shoes.hml.shoesdb.handlers.ShoesItemsList;
import shoes.hml.shoesdb.handlers.comparators.ComparatorList;
import shoes.hml.shoesdb.items.ShoesItem;
import shoes.hml.shoesdb.views.ShoesItemView;

/**
 * Created by maximusfk on 15.02.16.
 */
public class ShoesItemListAdapter extends BaseAdapter implements SpinnerAdapter {

    private ShoesItemsList itemList;
    private ArrayList<ShoesItem> items;
    private Context parent;
    private Comparator comparator;

    public ShoesItemListAdapter(Context parent, ShoesItemsList items) {
        this.itemList = items;
        this.items = items.getItems();
        this.parent = parent;
        sort(ShoesComparator.ByName);
    }

    @Override
    public void notifyDataSetChanged() {
        items = itemList.getItems();
        Collections.sort(items, this.comparator);
        super.notifyDataSetChanged();
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return position >=0 && position < items.size();
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    public ShoesItem getShoesItem(int position) {
        return (ShoesItem) getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return this.items.get(position).getID();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ShoesItemView view = new ShoesItemView(this.parent, (ShoesItem) getItem(position));
        view.setMinimumHeight(60);
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView textView = new TextView(this.parent);
        ShoesItem item = getShoesItem(position);
        String label = !item.getLabel().isEmpty() ? "(" + item.getLabel() + ")" : "";
        textView.setText(new StringBuilder().append(item.getName()).append(label).append(' ').append(item.getMainColor().getLocalizeName())
                .append('/').append(item.getSecondColor().getLocalizeName()).append(' ').append(item.getMinSize()).append('-').append(item.getMaxSize()));
        textView.setMinHeight(45);
        ShoesItemView itemView = new ShoesItemView(this.parent, item);
        itemView.setViewVariant(ShoesItemView.ShoesItemViewVariant.LINE);
        itemView.setMinimumHeight(45);
        return itemView;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    public int getPosition(ShoesItem item) {
        for(int pos = 0; pos < items.size(); pos++)
            if(items.get(pos).equals(item))
                return pos;
        return -1;
    }

    public void sort(ShoesComparator comparator) {
        this.comparator = new ComparatorList<>(
                ShoesComparator.ByCategory.getInvertComparator(),
                comparator.getComparator());
        Collections.sort(items, this.comparator);
        super.notifyDataSetChanged();
    }
}
