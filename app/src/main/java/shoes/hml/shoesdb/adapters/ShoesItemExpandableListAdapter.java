package shoes.hml.shoesdb.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import shoes.hml.shoesdb.handlers.ShoesItemsList;
import shoes.hml.shoesdb.items.ShoesColor;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 06.02.16.
 */
public class ShoesItemExpandableListAdapter extends BaseExpandableListAdapter {

    ShoesItemsList list;
    Context parent;

    public ShoesItemExpandableListAdapter(Context parent, ShoesItemsList list) {
        this.list = list;
        this.parent = parent;
    }

    @Override
    public int getGroupCount() {
        return list.getItems().size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 8;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return list.getItems().get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ShoesItem item = list.getItems().get(groupPosition);
        switch (childPosition) {
            case 0:
                return item.getName();
            case 1:
                return item.getMaxSize();
            case 2:
                return item.getMinSize();
            case 3:
                return item.getPriceRetail();
            case 4:
                return item.getPriceWhole();
            case 5:
                return item.getCost();
            case 6:
                return item.getMainColor();
            case 7:
                return item.getSecondColor();
        }
        return new Integer(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        TextView textView = new TextView(this.parent);
        ShoesItem item = (ShoesItem) getGroup(groupPosition);
        String color1 = item.getMainColor() != ShoesColor.None ? item.getMainColor().name() : "";
        String color2 = item.getSecondColor() != ShoesColor.None ? "/" + item.getSecondColor().name() : "";
        textView.setText(item.getName() + color1 + color2);
        return textView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        TextView textView = new TextView(this.parent);
        textView.setText(getChild(groupPosition, childPosition).toString());
        //ShoesItemView view = new ShoesItemView(this.parent, getChild(groupPosition, childPosition));
        //view.setMinimumHeight(20);
        return null;//view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
