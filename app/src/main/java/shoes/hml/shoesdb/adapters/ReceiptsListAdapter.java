package shoes.hml.shoesdb.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;

import shoes.hml.shoesdb.actions.ReceiptMove;
import shoes.hml.shoesdb.handlers.ReceiptList;

/**
 * Created by maximusfk on 03.03.16.
 */
public class ReceiptsListAdapter extends BaseAdapter {

    private ReceiptList moveList;
    private Context parent;

    public ReceiptsListAdapter(Context parent, ReceiptList moveList) {
        this.parent = parent;
        this.moveList = moveList;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return moveList.getMoves().size();
    }

    @Override
    public Object getItem(int position) {
        return moveList.getMoves().get(position);
    }

    @Override
    public long getItemId(int position) {
        return moveList.getMoves().get(position).getID();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = new TextView(this.parent);
        ReceiptMove move = (ReceiptMove) getItem(position);
        DateFormat format = java.text.DateFormat.getDateInstance(DateFormat.FULL);
        String text = format.format(move.getCustomDate()) + "\t\t" + move.getCountForProvider() + '/' + move.getCount();
        textView.setText(text);
        textView.setMinimumHeight(35);
        return textView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return moveList.getMoves().isEmpty();
    }
}
