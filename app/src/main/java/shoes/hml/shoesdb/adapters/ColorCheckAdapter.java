package shoes.hml.shoesdb.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ListAdapter;

import shoes.hml.shoesdb.items.ShoesColor;

/**
 * Created by maximusfk on 04.04.16.
 */
public class ColorCheckAdapter implements ListAdapter {

    Context parent;

    public ColorCheckAdapter(Context parent) {
        this.parent = parent;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return ShoesColor.values().length;
    }

    @Override
    public Object getItem(int position) {
        return ShoesColor.values()[position];
    }

    @Override
    public long getItemId(int position) {
        return ShoesColor.values()[position].getColorId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CheckedTextView checkedTextView = new CheckedTextView(this.parent);
        checkedTextView.setText(ShoesColor.values()[position].getLocalizeName());
        checkedTextView.setHeight(45);
        return checkedTextView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
