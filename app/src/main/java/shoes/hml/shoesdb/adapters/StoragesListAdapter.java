package shoes.hml.shoesdb.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import shoes.hml.shoesdb.handlers.Storage;
import shoes.hml.shoesdb.handlers.StorageList;

/**
 * Created by maximusfk on 03.03.16.
 */
public class StoragesListAdapter extends BaseAdapter {

    Context parent;
    StorageList storageList;

    public StoragesListAdapter(Context parent, StorageList list) {
        this.parent = parent;
        storageList = list;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public int getCount() {
        return storageList.getStorages().size();
    }

    @Override
    public Object getItem(int position) {
        return storageList.getStorages().get(position);
    }

    @Override
    public long getItemId(int position) {
        return storageList.getStorages().get(position).getID();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = new TextView(this.parent);
        Storage storage = (Storage) getItem(position);
        view.setText(storage.getStorageName());
        view.setMinHeight(45);
        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return storageList.getStorages().isEmpty();
    }
}
