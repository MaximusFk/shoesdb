package shoes.hml.shoesdb.adapters;

import android.support.annotation.StringRes;

import java.util.Comparator;

import shoes.hml.shoesdb.AppSettings;
import shoes.hml.shoesdb.R;
import shoes.hml.shoesdb.handlers.comparators.ComparatorInvert;
import shoes.hml.shoesdb.handlers.comparators.shoes_items.ShoesColorComparator;
import shoes.hml.shoesdb.handlers.comparators.shoes_items.ShoesIDComparator;
import shoes.hml.shoesdb.handlers.comparators.shoes_items.ShoesLabelComparator;
import shoes.hml.shoesdb.handlers.comparators.shoes_items.ShoesNameComparator;
import shoes.hml.shoesdb.handlers.comparators.shoes_items.ShoesCategoryComparator;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 02.10.16.
 */

public enum ShoesComparator {
    ByName(R.string.text_sort_by_name, new ShoesNameComparator()),
    ByColor(R.string.text_sort_by_color, new ShoesColorComparator()),
    ByID(R.string.text_sort_by_id, new ShoesIDComparator()),
    ByCategory(R.string.text_sort_by_category, new ShoesCategoryComparator()),
    ByLabel(R.string.text_sort_by_label, new ShoesLabelComparator());

    Comparator<ShoesItem> comparator;
    @StringRes int stringId;

    ShoesComparator(int stringId, Comparator<ShoesItem> comparator) {
        this.stringId = stringId;
        this.comparator = comparator;
    }

    public Comparator<ShoesItem> getComparator() {
        return comparator;
    }

    public Comparator<ShoesItem> getInvertComparator() {
        return new ComparatorInvert<>(comparator);
    }

    public @StringRes int getStringId() {
        return stringId;
    }

    @Override
    public String toString() {
        return AppSettings.getString(getStringId());
    }
}
