package shoes.hml.shoesdb.adapters;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.database.DataSetObserver;
import android.support.annotation.XmlRes;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.Collections;

import shoes.hml.shoesdb.R;
import shoes.hml.shoesdb.actions.Draft;
import shoes.hml.shoesdb.actions.ItemEntryMove;
import shoes.hml.shoesdb.actions.ReceiptMove;
import shoes.hml.shoesdb.handlers.comparators.ComparatorList;
import shoes.hml.shoesdb.handlers.comparators.item_entries.ItemShoesComparator;
import shoes.hml.shoesdb.handlers.comparators.shoes_items.ShoesColorComparator;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesColor;
import shoes.hml.shoesdb.items.ShoesItem;
import shoes.hml.shoesdb.views.ItemEntry_w_add;

/**
 * Created by maximusfk on 14.03.16.
 */
public class ReceiptItemEntryAdapter extends BaseAdapter implements View.OnClickListener {

    ArrayList<ItemEntry> itemEntries;
    Context parent;
    OnItemClick button = null;
    OnItemClick over = null;
    boolean showAddItemButton = true;
    ItemEntry_w_add.OnAddButtonClick addButtonClick;
    ItemEntryMove move;

    public ReceiptItemEntryAdapter(Context parent) {
        itemEntries = new ArrayList<>();
        this.parent = parent;
    }

    public ReceiptItemEntryAdapter(Context parent, ReceiptMove move) {
        itemEntries = move.getItems();
        this.parent = parent;
        this.move = move;
        sort();
    }

    public ReceiptItemEntryAdapter(Context parent, Draft draft) {
        itemEntries = draft.getItems();
        this.parent = parent;
        this.move = draft;
        sort();
    }

    public ReceiptItemEntryAdapter(Context parent, ItemEntryMove entryMove) {
        itemEntries = entryMove.getItems();
        this.parent = parent;
        this.move = entryMove;
        sort();
    }

    public ReceiptItemEntryAdapter(Context parent, ArrayList<ItemEntry> entries) {
        itemEntries = entries;
        this.parent = parent;
        this.move = null;
        sort();
    }

    public ReceiptItemEntryAdapter(Context parent, ArrayList<ItemEntry> entries, boolean showAddItemButton) {
        itemEntries = entries;
        this.parent = parent;
        this.showAddItemButton = showAddItemButton;
        sort();
    }

    public void setAddButtonClick(ItemEntry_w_add.OnAddButtonClick addButtonClick) {
        this.addButtonClick = addButtonClick;
    }

    public boolean isShowAddItemButton() {
        return showAddItemButton;
    }

    public void setShowAddItemButton(boolean showAddItemButton) {
        this.showAddItemButton = showAddItemButton;
    }

    @Override
    public void notifyDataSetChanged() {
        if(this.move != null)
            itemEntries = move.getItems();
        sort();
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    public void sort(/* TODO: Добавить аргумент перечисление способа сортировки*/) {
        Collections.sort(
                itemEntries,
                new ItemShoesComparator(
                        new ComparatorList<>(
                                ShoesComparator.ByCategory.getInvertComparator(),
                                ShoesComparator.ByName.getComparator(),
                                ShoesComparator.ByColor.getComparator(),
                                ShoesComparator.ByLabel.getComparator()
                        )
                )
        );
        super.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return itemEntries.size() + (showAddItemButton ? 1 : 0);
    }

    @Override
    public Object getItem(int position) {
        return position < itemEntries.size() ? itemEntries.get(position) : null;
    }

    public ItemEntry getItemEntry(int position) {
        return (ItemEntry) getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position < itemEntries.size() ? itemEntries.get(position).getEntryID() : 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if(position < itemEntries.size()) {
            ItemEntry entry = itemEntries.get(position);
//            String color = entry.getItem().getMainColor() != ShoesColor.None ? entry.getItem().getMainColor().getLocalizeName() : "";
//            color += entry.getItem().getSecondColor() != ShoesColor.None ? "/" + entry.getItem().getSecondColor().getLocalizeName() : "";
//            String label = !entry.getItem().getLabel().isEmpty() ? "(" + entry.getItem().getLabel() + ")" : "";
//            TextView textView = new TextView(this.parent);
//            textView.setText(new StringBuilder().append(entry.getItem().getName()).append(label).append(' ')
//                    .append(color).append('\t').append(entry.getCount()));
//            LinearLayout layout = (LinearLayout) LayoutInflater.from(this.parent).inflate(R.layout.item_entry_view_w_add, null);
//            TextView name = (TextView) layout.findViewById(R.id.itemName),
//                    count = (TextView) layout.findViewById(R.id.count);
//            name.setText(entry.getItem().getName() + label + ' ' + color);
//            count.setText(new StringBuilder().append(entry.getCount()).toString());
//            layout.setMinimumHeight(40);
//            textView.setMinHeight(40);

            ItemEntry_w_add itemEntry_w_add = new ItemEntry_w_add(
                    this.parent,
                    entry,
                    parent.getResources().getLayout(R.layout.item_entry_view_w_add)
            );
            itemEntry_w_add.setOnAddButtonClickListener(addButtonClick);
            itemEntry_w_add.setListPosition(position);

            //view = textView;
            view = itemEntry_w_add;
            view.setMinimumHeight(40);
        }
        else {
            TextView textView = new TextView(this.parent);
            textView.setText("+ " + this.parent.getString(R.string.text_add_new_entry));
            textView.setMinHeight(40);
            view = textView;
        }
        return view;
    }

//    public void setAddButtonOnClickListener(int position, View.OnClickListener addButtonOnClickListener) {
//        if(!isAddButton(position)) {
//            LinearLayout layout = (LinearLayout) getView(position, null, null);
//            Button button = (Button) layout.findViewById(R.id.add_to_this);
//            button.setOnClickListener(addButtonOnClickListener);
//        }
//    }
//
//    public void setOnClickListener(int position, View.OnClickListener listener) {
//        if(!isAddButton(position)) {
//            LinearLayout layout = (LinearLayout) getView(position, null, null);
//            TextView name = (TextView) layout.findViewById(R.id.itemName),
//                    count = (TextView) layout.findViewById(R.id.count);
//            name
//        }
//    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return itemEntries.isEmpty();
    }

    public boolean isAddButton(int position) {
        if(position < itemEntries.size() && position >= 0)
            return false;
        return true;
    }

    @Override
    public void onClick(View v) {

    }

    public interface OnItemClick {
        void onItemClick(ItemEntry entry, int position);
    }
}
