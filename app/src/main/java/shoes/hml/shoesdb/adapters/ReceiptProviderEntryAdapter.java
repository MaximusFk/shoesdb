package shoes.hml.shoesdb.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import shoes.hml.shoesdb.R;
import shoes.hml.shoesdb.actions.ReceiptMove;

/**
 * Created by maximusfk on 16.03.16.
 */
public class ReceiptProviderEntryAdapter extends BaseAdapter {

    private ArrayList<ReceiptMove.ProviderEntry> entries;
    private Context parent;
    private ReceiptMove move;

    public ReceiptProviderEntryAdapter(Context parent) {
        this.entries = new ArrayList<>();
        this.parent = parent;
    }

    public ReceiptProviderEntryAdapter(Context parent, ReceiptMove move) {
        this.entries = move.getProviderEntries();
        this.parent = parent;
        this.move = move;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        super.unregisterDataSetObserver(observer);
    }

    @Override
    public int getCount() {
        return entries.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return position < entries.size() ? entries.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position < entries.size() ? entries.get(position).getParentID() : 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = new TextView(this.parent);
        if(position < entries.size()) {
            ReceiptMove.ProviderEntry entry = entries.get(position);
            textView.setText(new StringBuilder().append(entry.getName()).append("\t\t").append(entry.getCount()));
        }
        else {
            textView.setText("+ " + this.parent.getString(R.string.text_add_new_entry));
        }
        return textView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return entries.isEmpty();
    }

    public boolean isAddButton(int position) {
        if(position < entries.size() && position >= 0)
            return false;
        return true;
    }
}
