package shoes.hml.shoesdb.actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;

import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 05.06.16.
 */
public abstract class ItemEntryMove implements EntityMove, Iterable<ItemEntry> {
    TreeMap<Long, ItemEntry> items;

    protected ItemEntryMove() {
        items = new TreeMap<>();
    }

    protected ItemEntryMove(ShoesDBReaderHelper helper, long ID) {
        items = helper.getItemEntries(ID);
    }

    public ArrayList<ItemEntry> getItems() {
        return new ArrayList<>(items.values());
    }

    public void addItemWIDB(ShoesItem entry, long count, ShoesDBReaderHelper readerHelper) {
        ItemEntry newEntry = new ItemEntry(entry, count);
        newEntry.setParentID(this.getID());
        readerHelper.addEntry(newEntry);
        items.put(newEntry.getEntryID(), newEntry);
    }

    public void addItem(ShoesItem item, long count) {
        if(findItemEntry(item) != null) {
            findItemEntry(item).addCount(count);
        } else {
            ItemEntry newEntry = new ItemEntry(item, count);
            newEntry.setParentID(getID());
            items.put(newEntry.getEntryID(), newEntry);
        }
    }

    public void addItemWIDB(ItemEntry entry, ShoesDBReaderHelper readerHelper) {
        readerHelper.addEntry(entry);
        addItem(entry);
    }

    public void addItem(ItemEntry entry) {
        if(existItemEntry(entry)) {
            findItemEntry(entry.getEntryID()).addCount(entry.getCount());
        } else if(existItemEntry(entry.getItem())) {
            findItemEntry(entry.getItem()).addCount(entry.getCount());
        } else {
            entry.setParentID(getID());
            items.put(entry.getEntryID(), entry);
        }
    }

    private void addItem(ShoesItem item, long count, long entryID) {
        if(findItemEntry(item) != null) {
            findItemEntry(item).addCount(count);
        } else {
            ItemEntry newEntry = new ItemEntry(item, count, entryID);
            newEntry.setParentID(getID());
            items.put(newEntry.getEntryID(), newEntry);
        }
    }

    public void removeItem(ItemEntry entry) {
        items.remove(entry.getEntryID());
    }

    public ItemEntry findItemEntry(ShoesItem item) {
        for(ItemEntry entry : items.values()) {
            if(entry.getItem().equals(item))
                return entry;
        }
        return null;
    }

    public ItemEntry findItemEntry(long entryId) {
        return items.get(entryId);
    }

    public boolean existItemEntry(long entryId) {
        return items.containsKey(entryId);
    }

    public boolean existItemEntry(ShoesItem item) {
        for(ItemEntry entry : items.values()) {
            if(entry.getItem().equals(item))
                return true;
        }
        return false;
    }

    public void removeItemEntry(long entryId) {
        items.remove(entryId);
    }

    public boolean existItemEntry(ItemEntry entry) {
        return existItemEntry(entry.getEntryID());
    }

    @Override
    public long getCount() {
        long count = 0;
        for(ItemEntry entry : items.values())
            if(entry != null)
                count += entry.getCount();
        return count;
    }

    public void updateFromAnother(ItemEntryMove another) {
        for(ItemEntry entry : another) {
            if(this.existItemEntry(entry.getItem()))
                findItemEntry(entry.getItem()).setCount(another.getCount());
            else
                addItem(entry);
        }
    }

    protected void copyFromAnother(ItemEntryMove another) {
        for(ItemEntry entry : another) {
            addItem(entry.getItem(), entry.getCount(), entry.getEntryID());
        }
    }

    @Override
    public Iterator<ItemEntry> iterator() {
        return items.values().iterator();
    }
}
