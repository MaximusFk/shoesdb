package shoes.hml.shoesdb.actions;

import android.content.ContentValues;

import java.text.DateFormat;
import java.util.Date;

import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.handlers.Storage;
import shoes.hml.shoesdb.items.ItemEntry;

/**
 * Created by maximusfk on 26.06.16.
 */
public class WriteOff extends ItemEntryMove implements MoveBehavior {


    public static final String TABLE_NAME = "write-off_move";
    public static final String MOVE_ID = "move_id";
    public static final String MAKE_DATE = "make_date";
    public static final String CUSTOM_DATE = "custom_date";
    public static final String HOLD = "is_hold";
    public static final String STORAGE = "parent_storage";
    public static final String LABEL = "label";

    public static final int move_type = 8615685;

    Date makeDate;
    Date customDate;
    Storage storage;
    boolean hold;
    String label;
    long ID = (new java.util.Random()).nextLong();

    public WriteOff(long ID, Date makeDate, Date customDate, Storage storage, boolean hold, String label, ShoesDBReaderHelper helper) {
        super(helper, ID);
        this.ID = ID;
        this.makeDate = makeDate;
        this.customDate = customDate;
        this.storage = storage;
        this.hold = hold;
        this.label = label;
    }

    public WriteOff(Date makeDate, Date customDate, Storage storage, boolean hold, String label) {
        this.makeDate = makeDate;
        this.customDate = customDate;
        this.storage = storage;
        this.hold = hold;
        this.label = label;
    }

    public WriteOff(Date makeDate, Date customDate, Storage storage, boolean hold) {
        this.makeDate = makeDate;
        this.customDate = customDate;
        this.storage = storage;
        this.hold = hold;
    }

    public WriteOff(Draft draft, boolean hold) {
        this.makeDate = draft.makeDate;
        this.customDate = draft.customDate;
        this.label = draft.label;
        this.copyFromAnother(draft);
        this.hold = hold;
    }

    public WriteOff(Draft draft) {
        this(draft, false);
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public long getID() {
        return ID;
    }

    @Override
    public Date getMakeDate() {
        return makeDate;
    }

    @Override
    public Date getCustomDate() {
        return customDate;
    }

    @Override
    public int getMoveTypeID() {
        return move_type;
    }

    @Override
    public void setCustomDate(Date date) {
        customDate = date;
    }

    @Override
    public ContentValues getContent() {
        ContentValues values = new ContentValues();
        values.put(MOVE_ID, ID);
        values.put(MAKE_DATE, makeDate.getTime());
        values.put(CUSTOM_DATE, customDate.getTime());
        values.put(LABEL, label);
        values.put(HOLD, hold);
        if(storage != null)
            values.put(STORAGE, storage.getID());
        return values;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPriorityIdKey() {
        return MOVE_ID;
    }

    @Override
    public String getPriorityIdKeyWithValue() {
        return getPriorityIdKey() + "=" + ID;
    }

    @Override
    public String toString() {
        DateFormat format = DateFormat.getDateInstance(DateFormat.FULL);
        String text = "W " + format.format(this.customDate) + "\t\t" + getLabel() + ':' + getCount();
        return text;
    }

    @Override
    public boolean compute(ItemEntry _old, ItemEntry _new) {
        if(_old.isItem(_new)) {
            _old.setCount(
                    _old.getCount() - _new.getCount()
            );
            return true;
        } else
            return false;
    }
}
