package shoes.hml.shoesdb.actions;

import android.content.ContentValues;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;

import shoes.hml.shoesdb.Main2Activity;
import shoes.hml.shoesdb.R;
import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.handlers.Storage;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 20.03.16.
 */
public class Draft extends ItemEntryMove implements Comparable, Serializable {

    public final static String ACCESS = "draft";

    public final static String TABLE_NAME = "drafts";
    public final static String DRAFT_ID = "draft_id";
    public final static String LABEL = "label";
    public final static String MAKE_DATE = "make_date";
    public final static String CUSTOM_DATE = "custom_date";

    public final static int draftMoveID = 6816186;


    String label;
    long ID;
    Date makeDate;
    Date customDate;

    public Draft(String label, Date customDate, Date makeDate) {
        this.label = label;
        this.customDate = customDate;
        this.makeDate = makeDate;
        this.ID = (new java.util.Random()).nextLong();
    }

    public Draft(String label, Date customDate, Date makeDate, long ID, ShoesDBReaderHelper helper) {
        super(helper, ID);
        this.label = label;
        this.customDate = customDate;
        this.makeDate = makeDate;
        this.ID = ID;
    }

    public Draft(String label, Date customDate, Date makeDate, long ID) {
        this.label = label;
        this.customDate = customDate;
        this.makeDate = makeDate;
        this.ID = ID;
    }

    public Draft(String label, Date makeDate) {
        this.label = label;
        this.makeDate = makeDate;
        this.ID = (new java.util.Random()).nextLong();
    }

    public Draft(Draft draft) {
        this.ID = draft.getID();
        this.label = draft.getLabel();
        this.makeDate = draft.getMakeDate();
        this.customDate = draft.getCustomDate();
        this.copyFromAnother(draft);
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    public void insertDataFrom(Draft another) {
        this.customDate = another.customDate;
        this.label = another.label;
        this.makeDate = another.makeDate;
    }

    @Override
    public long getID() {
        return ID;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof Draft) {
            Draft another = (Draft) o;
            return this.getID() == another.getID();
        }
        return false;
    }

    @Override
    public Date getMakeDate() {
        return makeDate;
    }

    @Override
    public Date getCustomDate() {
        return customDate;
    }

    @Override
    public int getMoveTypeID() {
        return draftMoveID;
    }

    @Override
    public void setCustomDate(Date date) {
        customDate = date;
    }

    @Override
    public ContentValues getContent() {
        ContentValues values = new ContentValues();
        values.put(DRAFT_ID, ID);
        values.put(LABEL, label);
        values.put(MAKE_DATE, makeDate.getTime());
        values.put(CUSTOM_DATE, customDate.getTime());
        return values;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPriorityIdKey() {
        return DRAFT_ID;
    }

    @Override
    public String getPriorityIdKeyWithValue() {
        return getPriorityIdKey() + '=' + ID;
    }

    @Override
    public int compareTo(Object another) {
        if(another instanceof Draft) {
            Draft an = (Draft) another;
            int eq = 0;
            eq = an.getCustomDate().compareTo(this.getCustomDate());
            if(eq == 0)
                eq = this.label.compareTo(an.label);
            if(eq == 0)
                eq = this.getCount() != an.getCount() ? (this.getCount() < an.getCount() ? -1 : 1) : 0;
            return eq;
        }
        return 0;
    }

    @Override
    public String toString() {
        DateFormat format = DateFormat.getDateInstance(DateFormat.FULL);
        String text = "D " + format.format(this.customDate) + "\t\t" + getLabel() + ':' + getCount();
        return text;
    }

    public String toSenderString() {
        String text = this.getLabel() + ", ";
        text += DateFormat.getDateInstance(DateFormat.FULL).format(customDate) + '\n';
        for(ItemEntry entry : getItems())
            text += "  " + entry.toString() + '\n';
        text += Main2Activity.once.getString(R.string.text_count) + ": " + this.getCount();
        return text;
    }

    @Override
    public Object clone() {
        return new Draft(this);
    }
}
