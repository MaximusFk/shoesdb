package shoes.hml.shoesdb.actions;

import java.util.Date;

import shoes.hml.shoesdb.database.ContentValuesSerializable;
import shoes.hml.shoesdb.handlers.Storage;

/**
 * Created by maximusfk on 22.02.16.
 */
public interface EntityMove extends ContentValuesSerializable {
    long getID();
    long getCount();
    Date getMakeDate();
    Date getCustomDate();
    int getMoveTypeID();

    void setCustomDate(Date date);
}
