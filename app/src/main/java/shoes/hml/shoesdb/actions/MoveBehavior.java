package shoes.hml.shoesdb.actions;

import shoes.hml.shoesdb.items.ItemEntry;

/**
 * Created by maximusfk on 09.09.16.
 */
public interface MoveBehavior {
    boolean compute(ItemEntry _old, final ItemEntry _new);
}
