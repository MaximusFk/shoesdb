package shoes.hml.shoesdb.actions;

import android.content.ContentValues;

import java.util.ArrayList;
import java.util.Date;

import shoes.hml.shoesdb.handlers.Storage;

/**
 * Created by maximusfk on 24.02.16.
 */
public class EntityMoveDraft implements EntityMove {

    public final static String MOVE_ID = "move_id";

    long ID;
    Date makeDate;
    Date editDate;
    Storage.StorageEntity entity;

    final static int ENTITY_MOVE_DRAFT_TYPE = 1;

    @Override
    public long getCount() {
        return entity.getProductsCount();
    }

    @Override
    public Date getMakeDate() {
        return makeDate;
    }

    @Override
    public Date getCustomDate() {
        return editDate;
    }

    @Override
    public int getMoveTypeID() {
        return ENTITY_MOVE_DRAFT_TYPE;
    }

    @Override
    public void setCustomDate(Date date) {
        this.editDate = date;
    }

    @Override
    public long getID() {
        return ID;
    }

    @Override
    public String getPriorityIdKey() {
        return MOVE_ID;
    }

    @Override
    public ContentValues getContent() {
        return null;
    }

    @Override
    public String getTableName() {
        return null;
    }

    @Override
    public String getPriorityIdKeyWithValue() {
        return getPriorityIdKey() + '=' + getID();
    }

    public static class EntityMoveDraftArray extends EntityMoveDraft {

        private ArrayList<Storage.StorageEntity> entities;
        long ID;

        static final int ENTITY_MOVE_DRAFT_TYPE_ARRAY = 2;

        public EntityMoveDraftArray() {
            entities = new ArrayList<>();
        }

        @Override
        public int getMoveTypeID() {
            return ENTITY_MOVE_DRAFT_TYPE_ARRAY;
        }

        public void addEntry(Storage.StorageEntity entry) {
            entities.add(entry);
        }

        public int getSize() {
            return entities.size();
        }

        public void clear() {
            entities.clear();
        }

        @Override
        public ContentValues getContent() {
            return null;
        }

        @Override
        public String getTableName() {
            return null;
        }

        @Override
        public long getID() {
            return ID;
        }
    }
}
