package shoes.hml.shoesdb.actions;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import shoes.hml.shoesdb.Main2Activity;
import shoes.hml.shoesdb.R;
import shoes.hml.shoesdb.client.MoveTarget;
import shoes.hml.shoesdb.database.ContentValuesSerializable;
import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.handlers.Storage;
import shoes.hml.shoesdb.items.ItemEntry;

/**
 * Created by maximusfk on 26.02.16.
 */
public class ReceiptMove extends ItemEntryMove implements MoveBehavior, Serializable, Comparable {

    public final static String ACCESS = "receiptMove";

    public final static String TABLE_NAME = "Receipts";
    public final static String MOVE_ID = "move_id";
    public final static String CREATE_DATE = "create_date";
    public final static String CUSTOM_DATE = "custom_date";
    public final static String TO_TARGET_ID = "to_id";
    public final static String FROM_TARGET_ID = "from_id";
    public final static String HOLD = "is_hold";

    public final static int moveType_Receipt = 51651;

    Date createDate;
    Date customDate;
    long ID = (new java.util.Random().nextLong());
    ArrayList<ProviderEntry> providerEntries = new ArrayList<>();
    MoveTarget fromStorage = null;
    MoveTarget toStorage = null;
    boolean hold;

    private static ArrayList<String> history = new ArrayList<>();

    private static void addToHistory(String providerName) {
        if(!history.contains(providerName))
            history.add(providerName);
    }

    public static ArrayList<String> getAvaibleProviders() {
        return (ArrayList<String>) history.clone();
    }

    public static boolean hasAvaibleProviders() {
        return !history.isEmpty();
    }

    public ReceiptMove(Storage toStorage, Date createDate, Date customDate, long ID, ShoesDBReaderHelper helper) {
        super(helper, ID);
        this.toStorage = toStorage;
        this.customDate = customDate;
        this.createDate = createDate;
        this.ID = ID;
    }

    public ReceiptMove(long ID, Date createDate, Date customDate, ShoesDBReaderHelper helper) {
        super(helper, ID);
        this.ID = ID;
        this.createDate = createDate;
        this.customDate = customDate;
    }

    public ReceiptMove(Storage toStorage, Date createDate, Date customDate) {
        this.toStorage = toStorage;
        this.customDate = customDate;
        this.createDate = createDate;
    }

    public ReceiptMove(ReceiptMove move) {
        this.toStorage = move.getTo();
        this.fromStorage = move.getFrom();
        this.ID = move.getID();
        this.customDate = move.getCustomDate();
        this.createDate = move.getMakeDate();
        this.providerEntries = new ArrayList<>();
        for(ProviderEntry entry : move.getProviderEntries())
            this.addProvider((ProviderEntry) entry.clone());
        this.copyFromAnother(move);
    }

    public MoveTarget getTo() {
        return toStorage;
    }

    public <Target extends MoveTarget> void setTo(Target to) {
        this.toStorage = to;
    }

    public MoveTarget getFrom() {
        return fromStorage;
    }

    public <Target extends MoveTarget> void setFrom(Target from) {
        this.fromStorage = from;
    }

    public void setProviderEntries(ArrayList<ProviderEntry> entries) {
        this.providerEntries = entries;
        for(ProviderEntry entry : this.providerEntries)
            addToHistory(entry.getName());
    }

    public ArrayList<ProviderEntry> getProviderEntries() {
        return providerEntries;
    }

    public void addProviderWIDB(ProviderEntry entry, ShoesDBReaderHelper readerHelper) {
        if(entry.getParentID() != getID())
            entry.setParentID(getID());
        providerEntries.add(entry);
        readerHelper.addEntry(entry);
        addToHistory(entry.getName());
    }

    public void addProvider(ProviderEntry entry) {
        if(existProviderEntry(entry)) {
            findProviderEntry(entry).addCount(entry.getCount());
        } else {
            providerEntries.add(entry);
            addToHistory(entry.getName());
        }
    }

    public void removeProviderEntry(ProviderEntry entry) {
        for(int i =  0; i < providerEntries.size(); i++) {
            if(providerEntries.get(i).getName().equals(entry.getName())) {
                providerEntries.remove(i);
                break;
            }
        }
    }

    public long getCountForProvider() {
        long count = 0;
        for(ProviderEntry entry : providerEntries)
            if(entry != null)
                count += entry.getCount();
        return count;
    }

    @Override
    public Date getMakeDate() {
        return createDate;
    }

    @Override
    public Date getCustomDate() {
        return customDate;
    }

    @Override
    public int getMoveTypeID() {
        return moveType_Receipt;
    }

    @Override
    public void setCustomDate(Date date) {
        customDate = date;
    }

    @Override
    public ContentValues getContent() {
        ContentValues values = new ContentValues();
        values.put(MOVE_ID, ID);
        values.put(CREATE_DATE, createDate.getTime());
        values.put(CUSTOM_DATE, customDate.getTime());
        if(toStorage != null)
            values.put(TO_TARGET_ID, toStorage.getID());
        if(fromStorage != null)
            values.put(FROM_TARGET_ID, fromStorage.getID());
        return values;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPriorityIdKeyWithValue() {
        return getPriorityIdKey() + '=' + ID;
    }

    @Override
    public long getID() {
        return ID;
    }

    @Override
    public String getPriorityIdKey() {
        return MOVE_ID;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof ReceiptMove) {
            ReceiptMove ob = (ReceiptMove) o;
            return ob.getID() == this.getID();
        }
        return false;
    }

    public void insertDataFrom(ReceiptMove move) {
        this.setCustomDate(move.getCustomDate());
        this.createDate = move.createDate;
        this.fromStorage = move.fromStorage;
        this.toStorage = move.toStorage;
    }

    public ProviderEntry findProviderEntry(ProviderEntry entry) {
        for(ProviderEntry e : providerEntries) {
            if(e.uniqueID == entry.uniqueID)
                return e;
        }
        return null;
    }

    public boolean existProviderEntry(ProviderEntry entry) {
        return findProviderEntry(entry) != null;
    }

    @Override
    public int compareTo(Object another) {
        if(another instanceof ReceiptMove) {
            ReceiptMove an = (ReceiptMove) another;
            int eq = 0;
            eq = an.getCustomDate().compareTo(this.getCustomDate());
            if(eq == 0)
                eq = this.getCountForProvider() != an.getCountForProvider() ? (this.getCountForProvider() < an.getCountForProvider() ? -1 : 1) : 0;
            if(eq == 0)
                eq = this.getCount() != an.getCount() ? (this.getCount() < an.getCount() ? -1 : 1) : 0;
            return eq;

        }
        return 0;
    }

    @Override
    public String toString() {
        DateFormat format = DateFormat.getDateInstance(DateFormat.FULL);
        String text = "R " + format.format(this.getCustomDate()) + "\t\t" + this.getCountForProvider() + '/' + this.getCount();
        return text;
    }

    public String toSenderString() {
        String string = DateFormat.getDateInstance(DateFormat.FULL).format(customDate) + '\n';
        for(ProviderEntry entry : getProviderEntries())
            string += "  " + entry.getName() + "   " + entry.getCount() + '\n';
        string += '\n';
        for(ItemEntry entry : getItems())
            string += "  " + entry.toString() + '\n';
        string += Main2Activity.once.getString(R.string.text_count) + ':' + '\n';
        string += "  " + Main2Activity.once.getString(R.string.text_counted) + ": " + this.getCount() + Main2Activity.once.getString(R.string.text_pairs) + '\n';
        string += "  " + Main2Activity.once.getString(R.string.text_all) + ": " + this.getCountForProvider() +
                Main2Activity.once.getString(R.string.text_pairs);
        return string;
    }

    @Override
    public Object clone() {
        return new ReceiptMove(this);
    }

    @Override
    public boolean compute(ItemEntry _old, ItemEntry _new) {
        if(_old.isItem(_new)) {
            _old.setCount(
                    _old.getCount() + _new.getCount()
            );
            return true;
        } else
            return false;
    }

    public static class ProviderEntry implements ContentValuesSerializable, Serializable {

        public static final String ACCESS = "ProviderEntity";

        public static final String TABLE_NAME = "ProviderEntities";
        public static final String PARENT_ID = "parent_id";
        public static final String ENTRY_ID = "entry_id";
        public static final String ITEMS_COUNT = "count";
        public static final String PROVIDER_NAME = "name";

        long parentID;
        long count;
        String name;

        long uniqueID = new java.util.Random().nextLong();

        public ProviderEntry(String name, long count, long parentID) {
            this.name = name;
            this.count = count;
            this.parentID = parentID;
        }

        public ProviderEntry(String name, long count, long parentID, long uniqueID) {
            this.name = name;
            this.count = count;
            this.parentID = parentID;
            this.uniqueID = uniqueID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public long getCount() {
            return count;
        }

        public void setCount(long count) {
            this.count = count;
        }

        public void addCount(long count) {
            this.count += count;
        }

        public long getParentID() {
            return parentID;
        }

        public void setParentID(long parentID) {
            this.parentID = parentID;
        }

        public void insertDataFrom(ProviderEntry another) {
            if(this.getParentID() == another.getParentID()) {
                this.name = another.getName();
                this.count = another.getCount();
            }
        }

        public long getUniqueID() {
            return uniqueID;
        }

        @Override
        public ContentValues getContent() {
            ContentValues values = new ContentValues();
            values.put(PARENT_ID, parentID);
            values.put(ITEMS_COUNT, count);
            values.put(PROVIDER_NAME, name);
            values.put(ENTRY_ID, uniqueID);
            return values;
        }

        @Override
        public String getTableName() {
            return TABLE_NAME;
        }

        @Override
        public String getPriorityIdKey() {
            return ENTRY_ID;
        }

        @Override
        public String getPriorityIdKeyWithValue() {
            return this.getPriorityIdKey() + '=' + uniqueID;
        }

        @Override
        public Object clone() {
            ProviderEntry entry = new ProviderEntry(this.getName(), this.getCount(), this.getParentID(), this.getUniqueID());
            return entry;
        }
    }

}
