package shoes.hml.shoesdb.actions;

import android.content.ContentValues;
import android.support.annotation.StringRes;

import java.text.DateFormat;
import java.util.Date;

import shoes.hml.shoesdb.Main2Activity;
import shoes.hml.shoesdb.R;
import shoes.hml.shoesdb.client.MoveTarget;
import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.handlers.Storage;
import shoes.hml.shoesdb.items.ItemEntry;

/**
 * Created by maximusfk on 04.04.16.
 */
public class SaleMove extends ItemEntryMove implements MoveBehavior {

    public static final String ACCESS = "SALEMOVE";

    public static final String TABLE_NAME = "Sale_Moves";
    public static final String MOVE_ID = "move_id";
    public final static String CREATE_DATE = "create_date";
    public final static String CUSTOM_DATE = "custom_date";
    public final static String DEBIT = "debit";
    public final static String DEBITOFF = "debit_off";
    public final static String TO_TARGET_ID = "to_id";
    public final static String FROM_TARGET_ID = "from_id";
    public final static String SALE_TYPE_ID = "sale_type_id";
    public final static String HOLD = "is_hold";

    public static final int moveTypeID = 651868610;


    private long moveId = (new java.util.Random()).nextLong();
    private MoveTarget target = null;
    private Storage from = null;
    private Date makeDate;
    private Date customDate;
    private float debit;
    private float debitOff;
    private boolean hold = false;
    private SaleType saleType = SaleType.WHOLE;

    public SaleMove(Draft draft) {
        this.customDate = draft.customDate;
        this.makeDate = draft.makeDate;
        this.copyFromAnother(draft);
    }

    public SaleMove(Storage from, Date makeDate, Date customDate, SaleType saleType) {
        this.from = from;
        this.makeDate = makeDate;
        this.customDate = customDate;
        this.saleType = saleType;
    }

    public SaleMove(Storage from, Date makeDate, Date customDate, float debit, SaleType saleType) {
        this.from = from;
        this.makeDate = makeDate;
        this.customDate = customDate;
        this.debit = debit;
        this.saleType = saleType;
    }

    public SaleMove(Date makeDate, Date customDate, float debit, SaleType saleType) {
        this.makeDate = makeDate;
        this.customDate = customDate;
        this.debit = debit;
        this.saleType = saleType;
    }

    public SaleMove(long ID, ShoesDBReaderHelper helper) {
        super(helper, ID);
        this.moveId = ID;
    }

    public SaleMove(long ID, Date makeDate, Date customDate, ShoesDBReaderHelper helper) {
        super(helper, ID);
        this.moveId = ID;
        this.makeDate = makeDate;
        this.customDate = customDate;
    }

    public SaleMove(long moveId, Date makeDate, Date customDate, float debit, ShoesDBReaderHelper helper) {
        super(helper, moveId);
        this.moveId = moveId;
        this.makeDate = makeDate;
        this.customDate = customDate;
        this.debit = debit;
    }

    public SaleMove(SaleMove saleMove) {
        this.moveId = saleMove.getID();
        this.makeDate = saleMove.getMakeDate();
        this.customDate = saleMove.getCustomDate();
        this.from = saleMove.getStorage();
        this.target = saleMove.getTarget();
        this.debit = saleMove.getDebit();
        this.debitOff = saleMove.getDebitOff();
        this.hold = saleMove.hold;
        this.saleType = saleMove.getSaleType();
        this.copyFromAnother(saleMove);
    }

    public Storage getStorage() {
        return from;
    }

    public void setStorage(Storage from) {
        this.from = from;
    }

    public MoveTarget getTarget() {
        return target;
    }

    public void setTarget(MoveTarget target) {
        this.target = target;
    }

    public float getDebit() {
        return debit;
    }

    public void setDebit(float debit) {
        this.debit = debit;
    }

    public float getDebitOff() {
        return debitOff;
    }

    public void setDebitOff(float debitOff) {
        this.debitOff = debitOff;
    }

    public SaleType getSaleType() {
        return saleType;
    }

    public void setSaleType(SaleType saleType) {
        this.saleType = saleType;
    }

    public float getCreditPrice() {
        float credit = 0.f;
        for(ItemEntry entry : getItems()) {
            if(saleType == SaleType.RETAIL)
                credit += entry.getRetailSum();
            else
                credit += entry.getWholeSum();
        }
        return credit;
    }

    @Override
    public long getID() {
        return moveId;
    }

    @Override
    public Date getMakeDate() {
        return makeDate;
    }

    @Override
    public Date getCustomDate() {
        return customDate;
    }

    @Override
    public int getMoveTypeID() {
        return moveTypeID;
    }

    @Override
    public void setCustomDate(Date date) {
        customDate = date;
    }

    @Override
    public ContentValues getContent() {
        ContentValues values = new ContentValues();
        values.put(CREATE_DATE, makeDate.getTime());
        values.put(CUSTOM_DATE, customDate.getTime());
        values.put(MOVE_ID, moveId);
        values.put(DEBIT, debit);
        values.put(DEBITOFF, debitOff);
        values.put(HOLD, hold);
        values.put(SALE_TYPE_ID, saleType.getTypeId());
        if(from != null)
            values.put(FROM_TARGET_ID, from.getID());
        if(target != null)
            values.put(TO_TARGET_ID, target.getID());
        return values;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPriorityIdKey() {
        return MOVE_ID;
    }

    @Override
    public String getPriorityIdKeyWithValue() {
        return getPriorityIdKey() + '=' + getID();
    }

    @Override
    public String toString() {
        DateFormat format = DateFormat.getDateInstance(DateFormat.FULL);
        String text = "S " + Main2Activity.once.getString(saleType.getStringId()) +
                '\t' + format.format(this.customDate) + ": " + this.getCount();
        return text;
    }

    @Override
    public Object clone() {
        SaleMove newMove = new SaleMove(
                (Date)this.getMakeDate().clone(),
                (Date)this.getCustomDate().clone(),
                this.getDebit(),
                this.getSaleType()
        );
        newMove.moveId = this.moveId;
        newMove.debitOff = this.debitOff;
        newMove.from = this.from;
        newMove.target = this.target;
        newMove.hold = this.hold;
        newMove.copyFromAnother(this);
        return newMove;
    }

    @Override
    public boolean compute(ItemEntry _old, ItemEntry _new) {
        if(_old.isItem(_new)) {
            _old.setCount(
                    _old.getCount() - _new.getCount()
            );
            return true;
        } else
            return false;
    }

    public enum SaleType {
        RETAIL((byte)0, R.string.shoes_retail),
        WHOLE((byte)1, R.string.shoes_whole);

        SaleType(byte ID, @StringRes int res) {
            typeId = ID;
            stringId = res;
        }
        byte typeId;
        @StringRes int stringId;

        public @StringRes int getStringId() {
            return stringId;
        }

        public byte getTypeId() {
            return typeId;
        }

        static public SaleType getTypeByID(byte id) {
            switch (id) {
                case 0:
                    return RETAIL;
                case 1:
                    return WHOLE;
                default:
                    return null;
            }
        }
    }
}
