package shoes.hml.shoesdb;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

/**
 * Created by maximusfk on 30.04.16.
 */
public class AppSettingActivity extends Activity {

    ListView settingsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_app_setting);
        settingsList = (ListView) findViewById(R.id.settings_list_view);
    }
}
