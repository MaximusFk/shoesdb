package shoes.hml.shoesdb;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

/**
 * Created by maximusfk on 16.03.16.
 */
public class MakeProviderView extends ViewGroup {

    public MakeProviderView(Context context) {
        super(context);
    }

    public MakeProviderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MakeProviderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

    }
}
