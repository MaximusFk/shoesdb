package shoes.hml.shoesdb.utils;

/**
 * Created by maximusfk on 07.06.16.
 */
public class ActivityArgs2Param<Type1, Type2> {
    private Type1 arg1 = null;
    private Type2 arg2 = null;

    public Type1 getArg1() {
        return arg1;
    }

    public void setArg1(Type1 arg1) {
        this.arg1 = arg1;
    }

    public Type2 getArg2() {
        return arg2;
    }

    public void setArg2(Type2 arg2) {
        this.arg2 = arg2;
    }

    public void clear() {
        arg1 = null;
        arg2 = null;
    }
}
