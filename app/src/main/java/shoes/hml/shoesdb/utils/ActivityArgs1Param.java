package shoes.hml.shoesdb.utils;

/**
 * Created by maximusfk on 07.06.16.
 */
public class ActivityArgs1Param<Type> {
    private Type arg = null;

    public ActivityArgs1Param(){}

    public ActivityArgs1Param(Type arg) {
        this.arg = arg;
    }

    public Type getArg() {
        return arg;
    }

    public void setArg(Type arg) {
        this.arg = arg;
    }

    public void clear() {
        arg = null;
    }
}
