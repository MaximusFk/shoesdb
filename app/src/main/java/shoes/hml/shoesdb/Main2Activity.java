package shoes.hml.shoesdb;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Toast;

import java.util.ArrayList;

import shoes.hml.shoesdb.actions.Draft;
import shoes.hml.shoesdb.actions.EntityMove;
import shoes.hml.shoesdb.actions.ReceiptMove;
import shoes.hml.shoesdb.actions.SaleMove;
import shoes.hml.shoesdb.adapters.DraftListAdapter;
import shoes.hml.shoesdb.adapters.MoveActionAdapter;
import shoes.hml.shoesdb.adapters.ReceiptsListAdapter;
import shoes.hml.shoesdb.adapters.ShoesComparator;
import shoes.hml.shoesdb.adapters.ShoesItemListAdapter;
import shoes.hml.shoesdb.adapters.StoragesListAdapter;
import shoes.hml.shoesdb.handlers.MainHandler;
import shoes.hml.shoesdb.handlers.ShoesItemsList;
import shoes.hml.shoesdb.handlers.Storage;
import shoes.hml.shoesdb.items.ShoesItem;
import shoes.hml.shoesdb.utils.ActivityArgs2Param;

public class Main2Activity extends ActionBarActivity
        implements PopupMenu.OnMenuItemClickListener, PopupMenu.OnDismissListener {
    private MainHandler mainHandler;
    private BaseAdapter listAdapter;
    private ListView listView;
    private PopupWindow choseFieldType;
    private int listPositionContext = -1;

    private ListType listType;

    public static final ActivityArgs2Param<ListType, MainHandler> args = new ActivityArgs2Param<>();

    ImageButton menuButton;

    private ShoesItemFieldLayoutHandler fieldEditor;

    public static String sSize;
    public static String sPrice;
    public static String sName;

    public static Main2Activity once;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        once = this;

        sPrice = getString(R.string.shoes_price);
        sSize = getString(R.string.shoes_size);
        sName = getString(R.string.shoes_model);

        listType = args.getArg1();
        mainHandler = args.getArg2();

        fieldEditor = new ShoesItemFieldLayoutHandler(this, new PopupWindow(this), mainHandler);

        ListView fieldList = new ListView(this);
        fieldList.setAdapter(ShoesItemFieldLayoutHandler.ShoesItemField.getAdapter(fieldList));
        fieldList.setClickable(true);
        fieldList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                fieldEditor.setDefaultFieldType((ShoesItemFieldLayoutHandler.ShoesItemField) parent.getAdapter().getItem(position));
                choseFieldType.dismiss();
                fieldEditor.show();
            }
        });
        choseFieldType = new PopupWindow(this);
        choseFieldType.setContentView(fieldList);
        choseFieldType.setClippingEnabled(true);
        choseFieldType.setWindowLayoutMode(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        choseFieldType.setTouchable(true);
        choseFieldType.setFocusable(true);

        listView = new ListView(this);
        switch (listType) {
            case SHOES:
                listAdapter = new ShoesItemListAdapter(this, mainHandler.getItemsList());
                break;
            case RECEIPTS:
                listAdapter = new ReceiptsListAdapter(this, mainHandler.getReceiptMoveList());
                break;
            case DRAFTS:
                listAdapter = new DraftListAdapter(this, mainHandler.getDraftList());
                break;
            case STORAGE:
                listAdapter = new StoragesListAdapter(this, mainHandler.getStorageList());
                break;
            case SALES:
                listAdapter = new MoveActionAdapter(this, mainHandler.getMoveList(), SaleMove.class);
                break;
            default:
                listAdapter = null;
        }
        FrameLayout cont = (FrameLayout) findViewById(R.id.container);
        listView.setPadding(0, 3, 0, 1);
        cont.addView(listView);
        listView.setAdapter(listAdapter);
        updateListAdapter();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Adapter padapter = parent.getAdapter();
                if (padapter instanceof ShoesItemListAdapter) {
                    ShoesItemListAdapter adapter = (ShoesItemListAdapter) padapter;
                    ShoesItem item = adapter.getShoesItem(position);
                    startShowShoesItemActivity(item);
                }
                else if(padapter instanceof ReceiptsListAdapter) {
                    startEditReceiptMoveActivity((ReceiptMove) padapter.getItem(position));
                }
                else if(padapter instanceof DraftListAdapter) {
                    startEditDraftActivity((Draft) padapter.getItem(position));
                }
                else if(padapter instanceof MoveActionAdapter) {
                    EntityMove move = (EntityMove) padapter.getItem(position);
                    if(move instanceof Draft)
                        startEditDraftActivity((Draft) move);
                    else if(move instanceof ReceiptMove)
                        startEditReceiptMoveActivity((ReceiptMove) move);
                    else if(move instanceof SaleMove)
                        startSaleActivity((SaleMove) move);
                }
                else if(padapter instanceof StoragesListAdapter) {
                    startStorageActivity((Storage) padapter.getItem(position));
                }
            }
        });
        Toolbar actionBar = (Toolbar) findViewById(R.id.action_bar);
        final ImageButton button = new ImageButton(this);
        button.setImageResource(R.drawable.ic_add_document_button);
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setMinimumHeight(70);
        button.setMinimumWidth(70);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (listType) {
                    case SHOES:
                        startCreateShoesItemActivity();
                        break;
                    case RECEIPTS:
                        startCreateReceiptMoveActivity();
                        break;
                    case DRAFTS:
                        startCreateDraftActivity();
                        break;
                    case STORAGE:
                        startCreateStorageActivity();
                        break;
                    case SALES:
                        startCreateSaleActivity();
                        break;
                }
            }
        });
        menuButton = new ImageButton(this);
        menuButton.setImageResource(R.drawable.ic_menu_button);
        menuButton.setBackgroundColor(Color.TRANSPARENT);
        menuButton.setPadding(160, 0, 0, 0);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listType == ListType.SHOES) {
                    showAdditionMenu();
                }
            }
        });
        actionBar.addView(button);
        actionBar.addView(menuButton);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                onOpenListViewContextMenu(view, position, id);
                return true;
            }
        });
    }

    private void showAdditionMenu() {
        PopupMenu additionMenu = new PopupMenu(this, findViewById(R.id.action_bar));
        additionMenu.inflate(R.menu.shoes_list_menu);
        additionMenu.setOnMenuItemClickListener(this);
        additionMenu.setOnDismissListener(this);
        additionMenu.show();
    }

    @Override
    public void onBackPressed() {
        if(choseFieldType.isShowing()) {
            choseFieldType.dismiss();
        }
        else if(fieldEditor.isShowing()) {
            fieldEditor.dissmiss();
        }
        else {
            super.onBackPressed();
        }
    }

//    @Override
//    public void onNavigationDrawerItemSelected(int position) {
//        // update the main content by replacing fragments
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
//                .commit();
//    }

//    public void onSectionAttached(int number) {
//        switch (number) {
//            case 1:
//                mTitle = getString(R.string.ShoesItems);
//                listView.setAdapter(itemListAdapter);
//                break;
//            case 2:
//                mTitle = getString(R.string.ReceiptsMoves);
//                listView.setAdapter(receiptsListAdapter);
//                break;
//            case 3:
//                mTitle = getString(R.string.DraftMoves);
//                listView.setAdapter(draftListAdapter);
//                break;
//            case 4:
//                mTitle = getString(R.string.Storages);
//                listView.setAdapter(storagesListAdapter);
//                break;
//            case 5:
//                mTitle = getString(R.string.SaleMoves);
//        }
//        restoreActionBar();
//    }

    private void startShowShoesItemActivity(ShoesItem item) {
        if(item == null)
            return;
        Intent intent = new Intent(this, ShoesItemShow.class);
        intent.putExtra(ShoesItem.ACCESS, item);
        startActivityForResult(intent, ResultType.ITEM.getId());
    }

    public void startCreateShoesItemActivity() {
        AddShoesItem.handler.setArg(mainHandler);
        startActivityForResult(new Intent(this, AddShoesItem.class), ResultType.CREATE_ITEM.getId());
    }

    public void startEditShoesItemActivity(ShoesItem item) {
        if(item == null)
            return;
        Intent intent = new Intent(this, AddShoesItem.class);
        intent.putExtra(ShoesItem.ACCESS, item);
        AddShoesItem.handler.setArg(mainHandler);
        startActivityForResult(intent, ResultType.EDIT_ITEM.getId());
    }

    public void startCreateReceiptMoveActivity() {
        Intent intent = new Intent(this, AddReceipt.class);
        //intent.putExtra(MainHandler.ACCESS, mainHandler);
        AddReceipt.args.setArg1(mainHandler);
        startActivityForResult(intent, ResultType.CREATE_RECEIPT.getId());
    }

    public void startEditReceiptMoveActivity(ReceiptMove move) {
        Intent intent = new Intent(this, AddReceipt.class);
        //intent.putExtra(MainHandler.ACCESS, mainHandler);
        //intent.putExtra(ReceiptMove.ACCESS, move);
        AddReceipt.args.setArg1(mainHandler);
        AddReceipt.args.setArg2(move);
        startActivityForResult(intent, ResultType.EDIT_RECEIPT.getId());
    }

    public void startCreateDraftActivity() {
        Intent intent = new Intent(this, AddDraft.class);
        //intent.putExtra(MainHandler.ACCESS, mainHandler);
        AddDraft.args.setArg1(mainHandler);
        startActivityForResult(intent, ResultType.CREATE_DRAFT.getId());
    }

    public void startEditDraftActivity(Draft draft) {
        Intent intent = new Intent(this, AddDraft.class);
        //intent.putExtra(MainHandler.ACCESS, mainHandler);
        //intent.putExtra(Draft.ACCESS, draft);
        AddDraft.args.setArg1(mainHandler);
        AddDraft.args.setArg2(draft);
        startActivityForResult(intent, ResultType.EDIT_DRAFT.getId());
    }

    public void startCreateStorageActivity() {
        Intent intent = new Intent(this, StorageActivity.class);
        intent.putExtra(MainHandler.ACCESS, mainHandler);
        startActivityForResult(intent, ResultType.CREATE_STORAGE.getId());
    }

    public void startStorageActivity(Storage storage) {
        Intent intent = new Intent(this, StorageActivity.class);
        intent.putExtra(MainHandler.ACCESS, mainHandler);
        intent.putExtra(Storage.ACCESS, storage);
        startActivityForResult(intent, ResultType.EDIT_STORAGE.getId());
    }

    public void startCreateSaleActivity() {
        Intent intent = new Intent(this, SaleActivity.class);
        SaleActivity.args.setArg1(mainHandler);
        startActivityForResult(intent, ResultType.CREATE_SALE.getId());
    }

    public void startSaleActivity(SaleMove saleMove) {
        Intent intent = new Intent(this, SaleActivity.class);
        SaleActivity.args.setArg1(mainHandler);
        SaleActivity.args.setArg2(saleMove);
        startActivityForResult(intent, ResultType.EDIT_SALE.getId());
    }

    public void startCategoriesActivity() {
        Intent intent = new Intent(this, ShoesCategoriesActivity.class);
        ShoesCategoriesActivity.handler.setArg(mainHandler);
        startActivity(intent);
    }

    public ShoesItemsList getItems() {
        return mainHandler.getItemsList();
    }

    public void updateListAdapter() {
        if(listAdapter != null)
            listAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        if(resultCode != Activity.RESULT_OK)
            return;
        ResultType type = ResultType.getResultType(requestCode);
        if(type == null)
            return;
        ShoesItem item;
        ReceiptMove receiptMove;
        Draft draft;
        Storage storage;
        switch (type) {
            case CREATE_ITEM:
                item = (ShoesItem) result.getSerializableExtra(ShoesItem.ACCESS);
                if(item != null) {
                    mainHandler.getItemsList().addShoesItem(item);
                    updateListAdapter();
                    Toast.makeText(this, getString(R.string.popup_sitem_created), Toast.LENGTH_SHORT).show();
                }
                break;
            case EDIT_ITEM:
                item = (ShoesItem) result.getSerializableExtra(ShoesItem.ACCESS);
                if(item != null) {
                    mainHandler.getItemsList().updateItem(item);
                    updateListAdapter();
                    Toast.makeText(this, getString(R.string.popup_sitem_edited), Toast.LENGTH_SHORT).show();
                }
                break;
            case DELETE_ITEM:
                item = (ShoesItem) result.getSerializableExtra(ShoesItem.ACCESS);
                if(item != null) {
                    mainHandler.getItemsList().deleteItem(item);
                    updateListAdapter();
                    Toast.makeText(this, getString(R.string.popup_sitem_deleted), Toast.LENGTH_SHORT).show();
                }
                break;
            case CREATE_RECEIPT:
                //receiptMove = (ReceiptMove) result.getSerializableExtra(ReceiptMove.ACCESS);
                receiptMove = AddReceipt.result.getArg();
                if(receiptMove != null) {
                    mainHandler.getReceiptMoveList().addMove(receiptMove);
                    updateListAdapter();
                    Toast.makeText(this, getString(R.string.popup_sitem_created), Toast.LENGTH_SHORT).show();
                }
                break;
            case EDIT_RECEIPT:
                //receiptMove = (ReceiptMove) result.getSerializableExtra(ReceiptMove.ACCESS);
                receiptMove = AddReceipt.result.getArg();
                if (receiptMove != null) {
                    mainHandler.getReceiptMoveList().updateMove(receiptMove);
                    updateListAdapter();
                    Toast.makeText(this, getString(R.string.popup_sitem_edited), Toast.LENGTH_SHORT).show();
                }
                break;
            case CREATE_DRAFT:
                //draft = (Draft) result.getSerializableExtra(Draft.ACCESS);
                draft = AddDraft.result.getArg();
                if(draft != null) {
                    mainHandler.getDraftList().addDraft(draft);
                    updateListAdapter();
                    Toast.makeText(this, getString(R.string.popup_sitem_created), Toast.LENGTH_SHORT).show();
                }
                break;
            case EDIT_DRAFT:
                //draft = (Draft) result.getSerializableExtra(Draft.ACCESS);
                draft = AddDraft.result.getArg();
                if (draft != null) {
                    mainHandler.getDraftList().updateDraft(draft);
                    updateListAdapter();
                    Toast.makeText(this, getString(R.string.popup_sitem_edited), Toast.LENGTH_SHORT).show();
                }
                break;
            case CREATE_STORAGE:
                storage = (Storage) result.getSerializableExtra(Storage.ACCESS);
                if(storage != null) {
                    mainHandler.getStorageList().addStorage(storage);
                    updateListAdapter();
                    Toast.makeText(this, getString(R.string.popup_sitem_created), Toast.LENGTH_SHORT).show();
                }
                break;
            case EDIT_STORAGE:
                storage = (Storage) result.getSerializableExtra(Storage.ACCESS);
                if (storage != null) {
                    mainHandler.getStorageList().updateStorage(storage);
                    Toast.makeText(this, getString(R.string.popup_sitem_edited), Toast.LENGTH_SHORT).show();
                }
                break;
            case CREATE_SALE:
            {
                SaleMove move = SaleActivity.result.getArg();
                if(move != null) {
                    mainHandler.getMoveList().addNewMove(move);
                    updateListAdapter();
                    Toast.makeText(this, getString(R.string.popup_sitem_created), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case EDIT_SALE:
            {
                SaleMove move = SaleActivity.result.getArg();
                if(move != null) {
                    mainHandler.getMoveList().updateMove(move);
                    updateListAdapter();
                    Toast.makeText(this, getString(R.string.popup_sitem_edited), Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    private void insertToArchive(final ShoesItem item) {
        new AlertDialog.Builder(this).setMessage(R.string.alert_archive_q)
                .setPositiveButton(getString(R.string.answ_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mainHandler.getItemsList().inputToArchive(item);
                        updateListAdapter();
                        Toast.makeText(Main2Activity.this, R.string.popup_sitem_archived, Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(R.string.answ_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setTitle(R.string.alert_delete_head)
                .show();
    }

    private void deleteShoesItem(final ShoesItem item) {
        if(mainHandler.isUsed(item)) {
            new AlertDialog.Builder(this).setMessage(R.string.alert_archive)
                    .setPositiveButton(getString(R.string.answ_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mainHandler.getItemsList().inputToArchive(item);
                            updateListAdapter();
                            Toast.makeText(Main2Activity.this, R.string.popup_sitem_archived, Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setNegativeButton(R.string.answ_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .setTitle(R.string.alert_delete_head)
                    .show();
        } else {
            new AlertDialog.Builder(this).setMessage(getString(R.string.alert_delete))
                    .setPositiveButton(getString(R.string.answ_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mainHandler.getItemsList().deleteItem(item);
                            updateListAdapter();
                            Toast.makeText(Main2Activity.this, getString(R.string.popup_sitem_deleted), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setNegativeButton(getString(R.string.answ_cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //finish();
                        }
                    })
                    .setTitle(getString(R.string.alert_delete_head))
                    .show();

        }
    }

    private void deleteReceiptMove(ReceiptMove move) {
        mainHandler.getReceiptMoveList().removeMove(move);
        updateListAdapter();
        Toast.makeText(this, getString(R.string.popup_sitem_deleted), Toast.LENGTH_SHORT).show();
    }

    private void deleteDraft(Draft draft) {
        mainHandler.getDraftList().removeDraft(draft);
        updateListAdapter();
        Toast.makeText(this, getString(R.string.popup_sitem_deleted), Toast.LENGTH_SHORT).show();
    }

    private void deleteSale(SaleMove saleMove) {
        mainHandler.getMoveList().deleteMove(saleMove);
        updateListAdapter();
        Toast.makeText(this, getString(R.string.popup_sitem_deleted), Toast.LENGTH_SHORT).show();
    }

    private void onOpenListViewContextMenu(View view, int position, long id) {
        PopupMenu contextMenu = new PopupMenu(this, view);
        contextMenu.setOnMenuItemClickListener(this);
        contextMenu.setOnDismissListener(this);
        this.listPositionContext = position;
        contextMenu.inflate(R.menu.list_context_menu);
        if(listType == ListType.SHOES)
            contextMenu.inflate(R.menu.shoes_item_menu);
        contextMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
            ShoesItem shoesItem;
            final ReceiptMove receiptMove;
            final Draft draft;
            final SaleMove saleMove;
            switch (item.getItemId()) {
                case R.id.context_open:
                    switch (listType) {
                        case SHOES:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                shoesItem = (ShoesItem) listAdapter.getItem(this.listPositionContext);
                                if (shoesItem != null) {
                                    startShowShoesItemActivity(shoesItem);
                                }
                            }
                            break;
                        case RECEIPTS:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                receiptMove = (ReceiptMove) listAdapter.getItem(this.listPositionContext);
                                if (receiptMove != null) {
                                    startEditReceiptMoveActivity(receiptMove);
                                }
                            }
                            break;
                        case DRAFTS:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                draft = (Draft) listAdapter.getItem(this.listPositionContext);
                                if (draft != null) {
                                    startEditDraftActivity(draft);
                                }
                            }
                            break;
                        case SALES:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                saleMove = (SaleMove) listAdapter.getItem(this.listPositionContext);
                                if (saleMove != null) {
                                    startSaleActivity(saleMove);
                                }
                            }
                            break;
                    }
                    break;
                case R.id.context_edit:
                    switch (listType) {
                        case SHOES:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                shoesItem = (ShoesItem) listAdapter.getItem(this.listPositionContext);
                                if (shoesItem != null) {
                                    startEditShoesItemActivity(shoesItem);
                                }
                            }
                            break;
                        case RECEIPTS:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                receiptMove = (ReceiptMove) listAdapter.getItem(this.listPositionContext);
                                if (receiptMove != null) {
                                    startEditReceiptMoveActivity(receiptMove);
                                }
                            }
                            break;
                        case DRAFTS:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                draft = (Draft) listAdapter.getItem(this.listPositionContext);
                                if (draft != null) {
                                    startEditDraftActivity(draft);
                                }
                            }
                            break;
                        case SALES:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                saleMove = (SaleMove) listAdapter.getItem(this.listPositionContext);
                                if (saleMove != null) {
                                    startSaleActivity(saleMove);
                                }
                            }
                            break;
                    }
                    break;
                case R.id.context_delete:
                    switch (listType) {
                        case SHOES:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                shoesItem = (ShoesItem) listAdapter.getItem(this.listPositionContext);
                                if (shoesItem != null) {
                                    //final ShoesItem finalShoesItem = shoesItem;
                                    deleteShoesItem(shoesItem);
                                }
                            }
                            break;
                        case RECEIPTS:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                receiptMove = (ReceiptMove) listAdapter.getItem(this.listPositionContext);
                                if (receiptMove != null) {
                                    final ReceiptMove receiptMove1 = receiptMove;
                                    new AlertDialog.Builder(this).setMessage(getString(R.string.alert_delete))
                                            .setPositiveButton(getString(R.string.answ_yes), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    deleteReceiptMove(receiptMove1);
                                                }
                                            })
                                            .setNegativeButton(getString(R.string.answ_no), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //finish();
                                                }
                                            })
                                            .setTitle(getString(R.string.alert_delete_head))
                                            .show();
                                }
                            }
                            break;
                        case DRAFTS:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                draft = (Draft) listAdapter.getItem(this.listPositionContext);
                                if (draft != null) {
                                    final Draft draft1 = draft;
                                    new AlertDialog.Builder(this).setMessage(getString(R.string.alert_delete))
                                            .setPositiveButton(getString(R.string.answ_yes), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    deleteDraft(draft1);
                                                }
                                            })
                                            .setNegativeButton(getString(R.string.answ_no), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //finish();
                                                }
                                            })
                                            .setTitle(getString(R.string.alert_delete_head))
                                            .show();
                                }
                            }
                            break;
                        case SALES:
                            if(this.listPositionContext >= 0 && this.listPositionContext < listAdapter.getCount()) {
                                saleMove = (SaleMove) listAdapter.getItem(this.listPositionContext);
                                if (saleMove != null) {
                                    new AlertDialog.Builder(this).setMessage(getString(R.string.alert_delete))
                                            .setPositiveButton(getString(R.string.answ_yes), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    deleteSale(saleMove);
                                                }
                                            })
                                            .setNegativeButton(getString(R.string.answ_no), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //finish();
                                                }
                                            })
                                            .setTitle(getString(R.string.alert_delete_head))
                                            .show();
                                }
                            }
                            break;
                    }
                    break;
                case R.id.shoes_setprice_on_equals:
                    if(listType == ListType.SHOES && this.listPositionContext >= 0
                            && this.listPositionContext < listAdapter.getCount()) {
                        ShoesItem it = (ShoesItem) listAdapter.getItem(this.listPositionContext);
                        for(ShoesItem item1 : mainHandler.getItemsList().getItemsEqualsName(it.getName())) {
                            item1.setPriceRetail(it.getPriceRetail());
                            item1.setPriceWhole(it.getPriceWhole());
                            mainHandler.getItemsList().updateItem(item1);
                        }
                    }
                    break;
                case R.id.shoes_set_whole:
                    if(listType == ListType.SHOES && this.listPositionContext >= 0
                            && this.listPositionContext < listAdapter.getCount()) {
                        ShoesItem it = (ShoesItem) listAdapter.getItem(this.listPositionContext);
                        fieldEditor.show(listView, it, ShoesItemFieldLayoutHandler.ShoesItemField.Whole);
                    }
                    break;
                case R.id.shoes_set_field:
                    if(listType == ListType.SHOES && this.listPositionContext >= 0
                            && this.listPositionContext < listAdapter.getCount()) {
                        if(choseFieldType.isShowing())
                            choseFieldType.dismiss();
                        choseFieldType.showAsDropDown(menuButton);
                        choseFieldType.update();
                        fieldEditor.addItem((ShoesItem) listAdapter.getItem(listPositionContext));
                        fieldEditor.setDefaultAnchor(listView);
                    }
                    break;
                case R.id.shoes_find_uses:
                    if(listType == ListType.SHOES && listAdapter.isEnabled(this.listPositionContext)) {
                        ShoesItem it = (ShoesItem) listAdapter.getItem(this.listPositionContext);
                        ArrayList<EntityMove> founded = new ArrayList<>();
                        founded.addAll(mainHandler.getDraftList().findDraftsByShoesItem(it));
                        founded.addAll(mainHandler.getReceiptMoveList().findMoveByShoesItem(it));
                        listView.setAdapter(new MoveActionAdapter(this, founded));
                    }
                    break;
                //Sorts:
                case R.id.sort_by_name:
                    if(listType == ListType.SHOES) {
                        ShoesItemListAdapter adapter = (ShoesItemListAdapter) listAdapter;
                        adapter.sort(ShoesComparator.ByName);
                    }
                    break;
                case R.id.sort_by_id:
                    if(listType == ListType.SHOES) {
                        ShoesItemListAdapter adapter = (ShoesItemListAdapter) listAdapter;
                        adapter.sort(ShoesComparator.ByID);
                    }
                    break;
                case R.id.sort_by_color:
                    if(listType == ListType.SHOES) {
                        ShoesItemListAdapter adapter = (ShoesItemListAdapter) listAdapter;
                        adapter.sort(ShoesComparator.ByColor);
                    }
                    break;
                case R.id.show_categoies:
                    if(listType == ListType.SHOES) {
                        startCategoriesActivity();
                    }
                    break;
            }
        return true;
    }

    @Override
    public void onDismiss(PopupMenu menu) {
        this.listPositionContext = -1;
    }

    public enum ResultType {
        CREATE_ITEM(1),
        EDIT_ITEM(2),
        DELETE_ITEM(3),
        CREATE_RECEIPT(4),
        EDIT_RECEIPT(5),
        DELETE_RECEIPT(6),
        ITEM(7),
        RECEIPT(8),
        ITEM_ENTRY(9),
        CREATE_ITEM_ENTRY(10),
        EDIT_ITEM_ENTRY(11),
        CREATE_DRAFT(12),
        EDIT_DRAFT(13),
        CREATE_STORAGE(14),
        EDIT_STORAGE(15),
        CREATE_SALE(16),
        EDIT_SALE(17);

        private int id;

        ResultType(int typeId) {
            this.id = typeId;
        }

        public int getId() {
            return id;
        }

        static public ResultType getResultType(int id) {
            switch (id) {
                case 1:
                    return CREATE_ITEM;
                case 2:
                    return EDIT_ITEM;
                case 3:
                    return DELETE_ITEM;
                case 4:
                    return CREATE_RECEIPT;
                case 5:
                    return EDIT_RECEIPT;
                case 6:
                    return DELETE_RECEIPT;
                case 7:
                    return ITEM;
                case 8:
                    return RECEIPT;
                case 9:
                    return ITEM_ENTRY;
                case 10:
                    return CREATE_ITEM_ENTRY;
                case 11:
                    return EDIT_ITEM_ENTRY;
                case 12:
                    return CREATE_DRAFT;
                case 13:
                    return EDIT_DRAFT;
                case 14:
                    return CREATE_STORAGE;
                case 15:
                    return EDIT_STORAGE;
                case 16:
                    return CREATE_SALE;
                case 17:
                    return EDIT_SALE;
                default:
                    return null;
            }
        }
    }
    public enum ListType {
        SHOES,
        DRAFTS,
        WRITEOFFS,
        SALES,
        RECEIPTS,
        STORAGE
    }
}
