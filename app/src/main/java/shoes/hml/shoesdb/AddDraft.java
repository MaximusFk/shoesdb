package shoes.hml.shoesdb;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.Calendar;
import java.util.GregorianCalendar;

import shoes.hml.shoesdb.actions.Draft;
import shoes.hml.shoesdb.adapters.ReceiptItemEntryAdapter;
import shoes.hml.shoesdb.adapters.ShoesItemListAdapter;
import shoes.hml.shoesdb.handlers.MainHandler;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.utils.ActivityArgs1Param;
import shoes.hml.shoesdb.utils.ActivityArgs2Param;
import shoes.hml.shoesdb.views.ItemEntry_w_add;

public class AddDraft extends ActionBarActivity implements DatePickerDialog.OnDateSetListener, PopupMenu.OnMenuItemClickListener {


    MainHandler handler;
    Draft draft;
    Calendar date;

    Button access;
    Button cancel;

    DatePickerDialog datePickerDialog;

    ItemEntryLayoutHandler addItemEntry;

    PopupWindow addItemEntryPopup;
    ShoesItemListAdapter shoesItemListAdapter;
    ReceiptItemEntryAdapter itemEntryAdapter;
    ListView itemList;
    Main2Activity.ResultType resultType;
    private PopupMenu contextMenu;
    private AlertDialog.Builder closeAlert;
    private boolean isEdited = false;
    private ImageButton menu;
    private PopupMenu menuAdditional;

    public static final ActivityArgs2Param<MainHandler, Draft> args = new ActivityArgs2Param<>();
    public static final ActivityArgs1Param<Draft> result = new ActivityArgs1Param<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_draft);
        Toolbar toolbar = (Toolbar) findViewById(R.id.action_bar);
        addItemEntryPopup = new PopupWindow(this);
        itemList = (ListView) findViewById(R.id.draft_items);
        date = new GregorianCalendar();
        handler = args.getArg1();
        draft = args.getArg2();
        if (draft != null) {
            resultType = Main2Activity.ResultType.EDIT_DRAFT;
            draft = (Draft) draft.clone();
            initFields();
        } else {
            resultType = Main2Activity.ResultType.CREATE_DRAFT;
            draft = new Draft("", date.getTime(), date.getTime());
            initDate();
        }
        datePickerDialog = new DatePickerDialog(
                this,
                this,
                date.get(Calendar.YEAR),
                date.get(Calendar.MONTH),
                date.get(Calendar.DAY_OF_MONTH)
        );
        findViewById(R.id.draft_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });
        access = new Button(this);
        access.setText(getString(R.string.answ_ok));
        access.setBackgroundColor(Color.TRANSPARENT);
        access.setTextColor(Color.WHITE);
        cancel = new Button(this);
        cancel.setText(getString(R.string.answ_cancel));
        cancel.setBackgroundColor(Color.TRANSPARENT);
        cancel.setTextColor(Color.WHITE);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        menu = new ImageButton(this);
        menu.setImageResource(R.drawable.ic_menu_button);
        menu.setBackgroundColor(Color.TRANSPARENT);
        addItemEntry = new ItemEntryLayoutHandler(this, addItemEntryPopup);
        shoesItemListAdapter = new ShoesItemListAdapter(this, handler.getItemsList());
        itemEntryAdapter = new ReceiptItemEntryAdapter(this, draft);
        addItemEntry.setAdapter(shoesItemListAdapter);

        itemEntryAdapter.setAddButtonClick(new ItemEntry_w_add.OnAddButtonClick() {
            @Override
            public void onAddClick(ItemEntry entry, int listPosition, ClickType clickType) {
                switch (clickType) {
                    case ADD:
                        addItemEntry.showWithNoEdit(findViewById(R.id.action_bar), entry);
                        break;
                    case SET:
                        addItemEntry.show(findViewById(R.id.action_bar), entry);
                        break;
                }
            }
        });

        findViewById(R.id.draft_add_entry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItemEntry.show(findViewById(R.id.action_bar));
            }
        });

        itemList.setAdapter(itemEntryAdapter);
        itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getAdapter() instanceof ReceiptItemEntryAdapter
                        && ((ReceiptItemEntryAdapter) parent.getAdapter()).isAddButton(position)) {
                    addItemEntry.show(findViewById(R.id.action_bar));
                } else if (parent.getAdapter() instanceof ReceiptItemEntryAdapter) {
                    ItemEntry entry = ((ReceiptItemEntryAdapter) parent.getAdapter()).getItemEntry(position);
                    addItemEntry.show(findViewById(R.id.action_bar), entry);
                }
            }
        });

        addItemEntry.setOnItemChanged(new ItemEntryLayoutHandler.OnItemChanged() {
            @Override
            public void onChanged(ItemEntry entry, boolean edited) {
                if (!edited) {
                    draft.addItem(entry);
                }
                isEdited = true;
                itemEntryAdapter.notifyDataSetChanged();
            }
        });

        itemList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getAdapter() instanceof ReceiptItemEntryAdapter &&
                        !((ReceiptItemEntryAdapter) parent.getAdapter()).isAddButton(position)) {
                    onOpenListViewContextMenu(view, position);
                    return true;
                }
                return false;
            }
        });

        access.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                draft.setLabel(((EditText) findViewById(R.id.draft_label)).getText().toString());
                draft.setCustomDate(date.getTime());
                final Intent rresult = new Intent();
                //result.putExtra(Draft.ACCESS, draft);
                result.setArg(draft);
                setResult(Activity.RESULT_OK, rresult);
                args.clear();
                finish();
            }
        });

        toolbar.addView(access);
        toolbar.addView(cancel);
        toolbar.addView(menu);
        //TODO изменить отступ на подходящий для разных рамеров экрана расчет
        menu.setPadding(55, 0, 0, 0);

        closeAlert = new AlertDialog.Builder(this);
        closeAlert.setTitle(getString(R.string.alert_close_head))
                .setMessage(getString(R.string.alert_close))
                .setPositiveButton(getString(R.string.answ_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.answ_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        menuAdditional = new PopupMenu(this, menu);
        menuAdditional.setOnMenuItemClickListener(this);
        menuAdditional.inflate(R.menu.additional_receipt_menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuAdditional.show();
            }
        });
    }

    @Override
    public void finish() {
        args.clear();
        super.finish();
    }

    @Override
    public void onBackPressed() {
        if(addItemEntryPopup.isShowing()) {
            addItemEntryPopup.dismiss();
        }
        else if(isEdited) {
            //closeAlert.show();
            access.callOnClick();
        } else {
            setResult(Activity.RESULT_CANCELED);
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_MENU && event.getAction() == KeyEvent.ACTION_DOWN)
            return menu.callOnClick();
        else
            return super.onKeyDown(keyCode, event);
    }

    void initFields() {
        if(draft != null) {
            ((EditText) findViewById(R.id.draft_label)).setText(draft.getLabel());
            date.setTime(draft.getCustomDate());
            initDate();
        }
    }
    
    
    void initDate() {
        if(date != null) {
            TextView datetextView = (TextView) findViewById(R.id.draft_date);
            datetextView.setText(new StringBuilder().append(date.get(Calendar.DAY_OF_MONTH)).append('/')
                    .append(date.get(Calendar.MONTH) + 1).append('/')
                    .append(date.get(Calendar.YEAR)).toString());
        }
    }

    private void onOpenListViewContextMenu(View view, int position) {
        contextMenu = new PopupMenu(this, view);
        contextMenu.setOnMenuItemClickListener(this);
        contextMenu.getMenu().add(1, position, 0, getString(R.string.text_delete));
        contextMenu.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        TextView datetextView = (TextView) findViewById(R.id.draft_date);
        datetextView.setText(new StringBuilder().append(dayOfMonth).append('/')
                .append(monthOfYear + 1).append('/')
                .append(year).toString());
        date.set(year, monthOfYear, dayOfMonth);
        isEdited = true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getGroupId()) {
            case 1:
                draft.removeItemEntry(itemEntryAdapter.getItemEntry(item.getItemId()).getItem().getID());
                itemList.setAdapter(itemEntryAdapter);
                isEdited = true;
                break;
            default:
                switch (item.getItemId()) {
                    case R.id.create_shoes_item:
                        Main2Activity.once.startCreateShoesItemActivity();
                        break;
                    case R.id.send_receipt_to:
                        final Intent sender = new Intent();
                        sender.setAction(Intent.ACTION_SEND);
                        sender.putExtra(Intent.EXTRA_TEXT, draft.toSenderString());
                        sender.setType("text/plain");
                        startActivity(Intent.createChooser(sender, getString(R.string.menu_send_receipt)));
                        break;
                }
        }
        return true;
    }
}
