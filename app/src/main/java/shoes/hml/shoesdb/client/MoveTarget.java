package shoes.hml.shoesdb.client;

import shoes.hml.shoesdb.items.ItemEntry;

/**
 * Created by maximusfk on 03.03.16.
 */
public interface MoveTarget {
    long getID();
    <T extends ItemEntry> void setItemContent(T [] itemContent);
}
