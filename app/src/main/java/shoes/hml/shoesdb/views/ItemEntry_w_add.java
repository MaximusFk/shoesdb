package shoes.hml.shoesdb.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import shoes.hml.shoesdb.R;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesColor;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 09.06.16.
 */
public class ItemEntry_w_add extends LinearLayout implements View.OnClickListener {

    ItemEntry entry;
    TextView t_count;
    TextView t_name;
    TextView t_color_main;
    TextView t_color_second;
    Button b_add;
    OnAddButtonClick event;
    int listPosition;
    View child;

    public ItemEntry_w_add(Context parent, ItemEntry entry, AttributeSet attributeSet) {
        super(parent, attributeSet);
        this.entry = entry;
        child = inflate(parent, R.layout.item_entry_view_w_add, this);
        this.bringChildToFront(child);
        this.setClickable(false);
        child.setClickable(false);
        initComponents();
    }

    private void initComponents() {
        t_count = (TextView) child.findViewById(R.id.count);
        t_name = (TextView) child.findViewById(R.id.itemName);
        b_add = (Button) child.findViewById(R.id.add_to_this);
        t_color_main = (TextView) child.findViewById(R.id.color_main_w);
        t_color_second = (TextView) child.findViewById(R.id.color_secondary_w);
        b_add.setOnClickListener(this);
        t_name.setOnClickListener(this);
        t_count.setOnClickListener(this);
        t_color_main.setOnClickListener(this);
        t_color_second.setOnClickListener(this);
        initItem();
    }

    private void initItem() {
        if(entry != null) {
            t_name.setText(new StringBuilder()
                    .append('[')
                    .append(entry.getItem().getMinSize())
                    .append('-')
                    .append(entry.getItem().getMaxSize())
                    .append("] ")
                    .append(entry.getItem().getName())
                    .toString()
            );
            if(!entry.getItem().getLabel().isEmpty())
                t_name.append(new StringBuilder()
                        .append(" (")
                        .append(entry.getItem().getLabel())
                        .append(')')
                        .toString()
                );
            t_count.setText("" + entry.getCount());
            if(entry.getItem().getMainColor() != ShoesColor.None) {
                ShoesColor color = entry.getItem().getMainColor();
                t_color_main.setText(color.getLocalizeName());
                t_color_main.setTextColor(color.getColorCode());
                t_color_main.setShadowLayer(2, 2, 2, Color.BLACK);
            }
            if(entry.getItem().getSecondColor() != ShoesColor.None) {
                ShoesColor color = entry.getItem().getSecondColor();
                t_color_second.setText(color.getLocalizeName());
                t_color_second.setTextColor(color.getColorCode());
                t_color_second.setShadowLayer(2, 2, 2, Color.BLACK);
            }
        }
    }

    public void setCount(long count) {
        entry.setCount(count);
        t_count.setText("" + count);
    }

    public void setListPosition(int listPosition) {
        this.listPosition = listPosition;
    }

    public int getListPosition() {
        return listPosition;
    }

    public void setItem(ShoesItem item) {
        entry.setItem(item);
    }

    @Override
    public void onClick(View view) {
        if(event != null) {
            if(view.getId() == b_add.getId())
                event.onAddClick(entry, listPosition, OnAddButtonClick.ClickType.ADD);
            else
                event.onAddClick(entry, listPosition, OnAddButtonClick.ClickType.SET);
        }
    }

    public void setOnAddButtonClickListener(OnAddButtonClick listener) {
        this.event = listener;
    }

    public interface OnAddButtonClick {
        enum ClickType {
            ADD,
            SET
        }
        void onAddClick(ItemEntry entry, int listPosition, ClickType clickType);
    }
}
