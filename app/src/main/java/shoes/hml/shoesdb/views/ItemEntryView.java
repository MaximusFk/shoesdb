package shoes.hml.shoesdb.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.text.TextPaint;
import android.view.View;

import shoes.hml.shoesdb.Main2Activity;
import shoes.hml.shoesdb.R;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesColor;

/**
 * Created by maximusfk on 06.03.16.
 */
public class ItemEntryView extends View {

    private ItemEntry itemEntry;

    public ItemEntryView(Context context, ItemEntry entry) {
        super(context);
        itemEntry = entry;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        TextPaint textPaint = new TextPaint();
        float textHeight = textPaint.getFontMetrics().bottom,
                textWidth = textPaint.getFontMetrics().descent * 2.6f;

        String color1 = itemEntry.getItem().getMainColor() != ShoesColor.None ? itemEntry.getItem().getMainColor().getLocalizeName() : "";
        String color2 = itemEntry.getItem().getSecondColor() != ShoesColor.None ? "/" + itemEntry.getItem().getSecondColor().getLocalizeName() : "";
        canvas.drawText(itemEntry.getItem().getName(), 10, textHeight * 3, textPaint);
        TextPaint colPaint = new TextPaint();
        colPaint.setColor(Color.argb(255, 75, 0, 130));
        colPaint.setTextSize(18);
        canvas.drawText(color1 + color2, ((itemEntry.getItem().getName().length() + 2) * textWidth), textHeight * 3, colPaint);
        canvas.drawText("" + itemEntry.getCount() + " " + Main2Activity.once.getString(R.string.text_pairs)
                , getWidth() - this.getPaddingRight() - ((int)((textWidth) *
                ("" + itemEntry.getCount()).length()) + Main2Activity.once.getString(R.string.text_pairs).length()), textHeight * 3, colPaint);
    }
}
