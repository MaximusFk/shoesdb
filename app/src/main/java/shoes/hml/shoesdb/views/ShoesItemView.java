package shoes.hml.shoesdb.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import shoes.hml.shoesdb.Main2Activity;
import shoes.hml.shoesdb.items.ShoesColor;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 15.02.16.
 */
public class ShoesItemView extends View {

    ShoesItem item;
    TextPaint textPaint;
    ShoesItemViewVariant viewVariant = ShoesItemViewVariant.BIG;

    public ShoesItemView(Context context, ShoesItem item) {
        super(context);
        init(item);
    }

    public ShoesItemView(Context context, ShoesItem item, AttributeSet attrs) {
        super(context, attrs);
        init(item);
    }

    public ShoesItemView(Context context, ShoesItem item, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(item);
    }

    public ShoesItemViewVariant getViewVariant() {
        return viewVariant;
    }

    public void setViewVariant(ShoesItemViewVariant viewVariant) {
        this.viewVariant = viewVariant;
    }

    private void init(ShoesItem item) {
        this.item = item;
        textPaint = new TextPaint();
       // textPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
       // textPaint.setTextAlign(Paint.Align.LEFT);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(25);
    }

    public ShoesItem getItem() {
        return item;
    }

    private void drawV1(Canvas canvas) {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

        float textHeight = textPaint.getFontMetrics().bottom,
                textWidth = textPaint.getFontMetrics().descent * 2.6f;

        TextPaint model = new TextPaint(textPaint);
        model.setColor(Color.GRAY);
        canvas.drawText(Main2Activity.sName + ": ", 10, textHeight * 3, model);
        canvas.drawText(item.getName(), (Main2Activity.sName.length() * textWidth) + model.getTextSize(), textHeight * 3, textPaint);
        if(item.getMainColor() != ShoesColor.None) {
            TextPaint color1paint = new TextPaint();
            color1paint.setTextSize(18);
            color1paint.setColor(item.getMainColor().getColorCode());
            color1paint.setShadowLayer(2, 2, 2, Color.BLACK);
            canvas.drawText(item.getMainColor().getLocalizeName(), ((item.getName().length() + Main2Activity.sName.length() + 2) * textWidth),
                    textHeight * 3, color1paint);
            if(item.getSecondColor() != ShoesColor.None) {
                TextPaint color2paint = new TextPaint();
                color2paint.setTextSize(18);
                color2paint.setColor(item.getSecondColor().getColorCode());
                color2paint.setShadowLayer(2, 2, 2, Color.BLACK);
                canvas.drawText(item.getSecondColor().getLocalizeName(),
                        ((item.getName().length() + Main2Activity.sName.length() + 1 + item.getMainColor().getLocalizeName().length()) * textWidth),
                        textHeight * 3, color2paint);
            }
        }
        //canvas.drawText(color1 + color2, ((item.getName().length() + Main2Activity.sName.length() + 2) * textWidth), textHeight * 3, colPaint);
        TextPaint paint = new TextPaint();
        paint.setColor(Color.GRAY);
        paint.setTextSize(20);
        String sizeStr = Main2Activity.sSize + ": " + item.getMinSize() + " - " + item.getMaxSize(),
                priceStr = Main2Activity.sPrice + ": " + item.getPriceWhole() + " / " + item.getPriceRetail();
        canvas.drawText(sizeStr, 12, getHeight() - paddingBottom - 6, paint);
        canvas.drawText(priceStr, 12 + ((int)(textWidth * sizeStr.length())), getHeight() - paddingBottom - 6, paint);

        if(item.getLabel() != null && !item.getLabel().isEmpty()) {
            canvas.drawText(item.getLabel(), (getWidth() + paddingRight) - ((int)((paint.getTextSize()/1.6f) * item.getLabel().length())), textHeight * 3, paint);
        }
    }

    private void drawV2(Canvas canvas) {
        float textHeight = textPaint.getFontMetrics().bottom,
                textWidth = textPaint.getFontMetrics().descent * 2.6f;
        TextPaint itemNamePaint = new TextPaint();
        itemNamePaint.setColor(Color.BLACK);
        itemNamePaint.setTextSize(25);
        canvas.drawText(item.getName(), 10, textHeight + itemNamePaint.getTextSize(), itemNamePaint);
        if(item.getMainColor() != ShoesColor.None) {
            TextPaint color1paint = new TextPaint();
            color1paint.setTextSize(18);
            color1paint.setColor(item.getMainColor().getColorCode());
            color1paint.setShadowLayer(2, 2, 2, Color.BLACK);
            canvas.drawText(item.getMainColor().getLocalizeName(), this.getWidth() / 2.5f,
                    textHeight * 3, color1paint);
            if(item.getSecondColor() != ShoesColor.None) {
                TextPaint color2paint = new TextPaint();
                color2paint.setTextSize(18);
                color2paint.setColor(item.getSecondColor().getColorCode());
                color2paint.setShadowLayer(2, 2, 2, Color.BLACK);
                canvas.drawText(item.getSecondColor().getLocalizeName(),
                        ((this.getWidth() / 2.5f) + (item.getMainColor().getLocalizeName().length() * textWidth)),
                        textHeight * 3, color2paint);
            }
        }
        int contentWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        TextPaint size_labelPaint = new TextPaint();
        size_labelPaint.setTextSize(11);
        size_labelPaint.setColor(Color.GRAY);
        canvas.drawText(item.getLabel(),
                contentWidth - (item.getLabel().length() * (size_labelPaint.getTextSize()/1.6f)),
                size_labelPaint.getFontMetrics().bottom + size_labelPaint.getTextSize(),
                size_labelPaint);
    }

    @Override
    public void onDraw(Canvas canvas) {
        switch (viewVariant) {
            case BIG:
                drawV1(canvas);
                break;
            case LINE:
                drawV2(canvas);
                break;
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    public enum ShoesItemViewVariant {
        BIG,
        COMPACT,
        LINE;
    }
}
