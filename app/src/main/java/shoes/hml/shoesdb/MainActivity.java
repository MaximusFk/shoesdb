package shoes.hml.shoesdb;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import shoes.hml.shoesdb.adapters.ShoesItemListAdapter;
import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.handlers.MainHandler;
import shoes.hml.shoesdb.handlers.ShoesItemsList;
import shoes.hml.shoesdb.items.ShoesItem;

public class MainActivity extends ActionBarActivity {

    ShoesItemListAdapter mAdapter;
    ListView view;
    ShoesItemsList itemsList;
    ShoesDBReaderHelper shoesDBReaderHelper;
    SQLiteDatabase database;

    private MainHandler handler;

    static MainActivity once;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //handler = new MainHandler(this, "shoes_test.db");
        once = this;
        view = (ListView) findViewById(R.id.itemsList);
        Toolbar ac = (Toolbar) findViewById(R.id.action_bar);
        Button button = new Button(this), openActivity2 = new Button(this);
        button.setText("Add Item");
        openActivity2.setText("Open");
        shoesDBReaderHelper = new ShoesDBReaderHelper(this, "shoes_test.db", handler);
        database = shoesDBReaderHelper.getWritableDatabase();
        //final Intent intent = new Intent(this, AddShoesItem.class);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivityForResult(intent, ResultType.CREATE_ITEM.getId());
                startCreateActivity();
            }
        });
        openActivity2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMain2Activity();
            }
        });
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getAdapter() instanceof ShoesItemListAdapter) {
                    ShoesItemListAdapter adapter = (ShoesItemListAdapter) parent.getAdapter();
                    ShoesItem item = adapter.getShoesItem(position);
                    startShowActivity(item);
                }
            }
        });
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setTextColor(Color.WHITE);
        openActivity2.setBackgroundColor(Color.TRANSPARENT);
        openActivity2.setTextColor(Color.WHITE);
        ac.addView(button);
        ac.addView(openActivity2);
        view.setPadding(5, 2, 5, 2);
        itemsList = new ShoesItemsList("Spring/Summer 2016", shoesDBReaderHelper);
        mAdapter = new ShoesItemListAdapter(this, itemsList);
        view.setAdapter(mAdapter);

    }

    private void openMain2Activity() {
        startActivity(new Intent(this, Main2Activity.class));
    }

    private void showShoesItemsList(ShoesItemsList list) {
        view.setAdapter(new ShoesItemListAdapter(this, list));
    }

    private void startShowActivity(ShoesItem item) {
        if(item == null)
            return;
        Intent intent = new Intent(this, ShoesItemShow.class);
        intent.putExtra(ShoesItem.ACCESS, item);
        startActivity(intent);
    }

    public void startCreateActivity() {
        startActivityForResult(new Intent(this, AddShoesItem.class), ResultType.CREATE_ITEM.getId());
    }

    public void startEditActivity(ShoesItem item) {
        if(item == null)
            return;
        Intent intent = new Intent(this, AddShoesItem.class);
        intent.putExtra(ShoesItem.ACCESS, item);
        startActivityForResult(intent, ResultType.EDIT_ITEM.getId());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        ResultType type = ResultType.getResultType(resultCode);
        if(type == null)
            return;
        ShoesItem item;
        switch (type) {
            case CREATE_ITEM:
                item = (ShoesItem) result.getSerializableExtra(ShoesItem.ACCESS);
                if(item != null) {
                    itemsList.addShoesItem(item);
                    itemsList.sortItemsArray();
                    view.setAdapter(mAdapter);
                    Toast.makeText(this, "New item are created", Toast.LENGTH_SHORT).show();
                }
                break;
            case EDIT_ITEM:
                item = (ShoesItem) result.getSerializableExtra(ShoesItem.ACCESS);
                if(item != null) {
                    itemsList.updateItem(item);
                    itemsList.sortItemsArray();
                    view.setAdapter(mAdapter);
                    Toast.makeText(this, "Item are edited", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public enum ResultType {
        CREATE_ITEM(1),
        EDIT_ITEM(2);

        private int id;
        ResultType(int typeId) {
            this.id = typeId;
        }

        public int getId() {
            return id;
        }

        static public ResultType getResultType(int id) {
            switch (id) {
                case 1:
                    return CREATE_ITEM;
                case 2:
                    return EDIT_ITEM;
                default:return null;
            }
        }
    }
}
