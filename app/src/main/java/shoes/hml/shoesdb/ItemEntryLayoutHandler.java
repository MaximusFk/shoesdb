package shoes.hml.shoesdb;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import shoes.hml.shoesdb.adapters.ShoesItemListAdapter;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 25.03.16.
 */
public class ItemEntryLayoutHandler implements View.OnClickListener {

    Context parent;
    ItemEntry entry;
    LinearLayout layout;
    PopupWindow popupWindow;
    OnItemChanged itemChanged;

    public ItemEntryLayoutHandler(Context context, PopupWindow popupWindow) {
        parent = context;
        this.popupWindow = popupWindow;
        layout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.add_item_entry, null);
        layout.findViewById(R.id.add_item_ok).setOnClickListener(this);
        layout.findViewById(R.id.add_item_plus_1).setOnClickListener(this);
        layout.findViewById(R.id.add_item_plus_2).setOnClickListener(this);
        layout.findViewById(R.id.add_item_plus_5).setOnClickListener(this);
        layout.findViewById(R.id.add_item_plus_10).setOnClickListener(this);
        layout.findViewById(R.id.add_item_mul_2).setOnClickListener(this);
        layout.findViewById(R.id.editText3).setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER)
                    layout.findViewById(R.id.add_item_ok).callOnClick();
                return false;
            }
        });
        this.popupWindow.setContentView(layout);
    }

    public ItemEntryLayoutHandler(Context context, PopupWindow popupWindow, ItemEntry entry) {
        this(context, popupWindow);
        this.entry = entry;
    }

    public LinearLayout getLayout() {
        return layout;
    }

    public void setOnItemChanged(OnItemChanged itemChanged) {
        this.itemChanged = itemChanged;
    }

    public void show(View anchor) {
        if(popupWindow != null) {
            if(popupWindow.isShowing())
                popupWindow.dismiss();
            ((EditText)layout.findViewById(R.id.editText3)).setText("");
            popupWindow.setWindowLayoutMode(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            popupWindow.setWidth(layout.getWidth());
            popupWindow.setHeight(layout.getHeight());
            popupWindow.showAsDropDown(anchor, 0, 0);
            popupWindow.setFocusable(true);
            popupWindow.setClippingEnabled(true);
            popupWindow.update();
        }
    }

    public void setAdapter(SpinnerAdapter adapter) {
        ((Spinner) layout.findViewById(R.id.spinner)).setAdapter(adapter);
    }

    public SpinnerAdapter getAdapter() {
        return ((Spinner)layout.findViewById(R.id.spinner)).getAdapter();
    }

    public void show(View anchor, ItemEntry entry) {
        show(anchor);
        Spinner spinner = (Spinner) layout.findViewById(R.id.spinner);
        spinner.setSelection(((ShoesItemListAdapter) spinner.getAdapter()).getPosition(entry.getItem()));
        setCount(entry.getCount());
        this.entry = entry;
    }

    public void showWithNoEdit(View anchor, ItemEntry entry) {
        show(anchor);
        Spinner spinner = (Spinner) layout.findViewById(R.id.spinner);
        spinner.setSelection(((ShoesItemListAdapter) spinner.getAdapter()).getPosition(entry.getItem()));
    }

    private long getCount() {
        EditText text = (EditText) layout.findViewById(R.id.editText3);
        long count = AddShoesItem.getLong(text.getText().toString());
        return count;
    }

    private void setCount(long count) {
        ((EditText) layout.findViewById(R.id.editText3)).setText(new StringBuilder().append(count));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_item_ok:
                boolean edited;
                Spinner spinner = (Spinner) layout.findViewById(R.id.spinner);
                long count = getCount();
                ShoesItem item = (ShoesItem) spinner.getSelectedItem();
                if(this.entry != null) {
                    this.entry.setItem(item);
                    this.entry.setCount(count);
                    edited = true;
                } else {
                    this.entry = new ItemEntry(item, count);
                    edited = false;
                }
                if (itemChanged != null) {
                    itemChanged.onChanged(entry, edited);
                }
                this.entry = null;
                popupWindow.dismiss();
                break;
            case R.id.add_item_plus_1:
                setCount(getCount() + 1);
                break;
            case R.id.add_item_plus_2:
                setCount(getCount() + 2);
                break;
            case R.id.add_item_plus_5:
                setCount(getCount() + 5);
                break;
            case R.id.add_item_plus_10:
                setCount(getCount() + 10);
                break;
            case R.id.add_item_clear:
                ((EditText)layout.findViewById(R.id.editText3)).setText("");
                break;
        }
    }

    public interface OnItemChanged {
        void onChanged(ItemEntry entry, boolean edited);
    }
}
