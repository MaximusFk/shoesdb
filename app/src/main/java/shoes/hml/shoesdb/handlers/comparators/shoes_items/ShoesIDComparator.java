package shoes.hml.shoesdb.handlers.comparators.shoes_items;

import java.util.Comparator;

import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 02.10.16.
 */

public class ShoesIDComparator implements Comparator<ShoesItem> {
    @Override
    public int compare(ShoesItem item, ShoesItem t1) {
        return item.getID() == t1.getID() ? 0 :
                item.getID() < t1.getID() ? -1 : 1;
    }
}
