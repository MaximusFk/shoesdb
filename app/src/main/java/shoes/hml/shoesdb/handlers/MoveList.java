package shoes.hml.shoesdb.handlers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import shoes.hml.shoesdb.actions.EntityMove;
import shoes.hml.shoesdb.actions.ItemEntryMove;
import shoes.hml.shoesdb.actions.ReceiptMove;
import shoes.hml.shoesdb.actions.SaleMove;
import shoes.hml.shoesdb.actions.WriteOff;
import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 25.02.16.
 */
public class MoveList implements Serializable {
    //private ArrayList<EntityMove> moves;
    private TreeMap<Long, EntityMove> moves;
    transient private ShoesDBReaderHelper database;

    public MoveList(ShoesDBReaderHelper database) {
        this.database = database;
        moves = new TreeMap<>();
        this.addMovesArray(database.getSaleMoves());
    }

    public <T extends EntityMove> void addNewMove(T move) {
        if(!exist(move.getID())) {
            moves.put(move.getID(), move);
            database.addEntry(move);
            if (move instanceof ItemEntryMove)
                addNewItemEntryContent((ItemEntryMove) move);
        } else {
            updateMove(move);
        }
    }

    public <T extends EntityMove> void deleteMove(T move) {
        if(exist(move.getID())) {
            if (move instanceof ItemEntryMove)
                deleteItemEntryContent((ItemEntryMove) move);
            database.deleteEntry(move);
            moves.remove(move.getID());
        }
    }

    private  <T extends EntityMove> void addMove(T move) {
        if(!exist(move.getID()))
            moves.put(move.getID(), move);
    }

    private  <T extends EntityMove> void addMovesArray(ArrayList<T> moves) {
        for(T m : moves) {
            addMove(m);
        }
    }

    private <T extends EntityMove> void addMovesArray(List<T> moves) {
        for(T m : moves)
            addMove(m);
    }

    public <T extends EntityMove> void addNewMovesArray(ArrayList<T> moves) {
        for(T m : moves) {
            addNewMove(m);
        }
    }

    public <T extends ItemEntryMove> void addNewItemEntryContent(T move) {
        for(ItemEntry entry : move.getItems()) {
            database.addEntry(entry);
        }
    }

    public <T extends EntityMove> void updateMove(T move) {
        if(move instanceof ItemEntryMove) {
            updateItemEntryContent((ItemEntryMove) move);
        }
        database.editEntry(move);
    }

    private <T extends ItemEntryMove> void updateItemEntryContent(T move) {
        EntityMove entityMove = getEntityMove(move.getID());
        if(entityMove instanceof ItemEntryMove) {
            ItemEntryMove current = (ItemEntryMove) entityMove;
            for (ItemEntry entry : move.getItems()) {
                if (!current.existItemEntry(entry.getEntryID())) {
                    current.addItem(entry);
                    database.addEntry(entry);
                } else {
                    current.findItemEntry(entry.getEntryID()).updateDataFrom(entry);
                    database.editEntry(entry);
                }
            }
            for (ItemEntry entry : current.getItems()) {
                if (!move.existItemEntry(entry.getItem())) {
                    move.removeItemEntry(entry.getEntryID());
                    database.deleteEntry(entry);
                }
            }
        }
    }

    private  <T extends ItemEntryMove> void deleteItemEntryContent(T move) {
        for(ItemEntry entry : move.getItems()) {
            database.deleteEntry(entry);
        }
    }

    public EntityMove getEntityMove(long ID) {
        return moves.get(ID);
    }

    public boolean exist(long ID) {
        return moves.containsKey(ID);
    }
    public EntityMove[] getArray() {
        return (EntityMove[]) moves.values().toArray();
    }

    public ArrayList<EntityMove> getMoves() {
        return new ArrayList<>(this.moves.values());
    }

    public ArrayList<EntityMove> getMoves(Class<? extends EntityMove> filter) {
        ArrayList<EntityMove> moves = new ArrayList<>();
        for(EntityMove move : this.moves.values()) {
            if(filter.isInstance(move))
                moves.add(move);
        }
        return moves;
    }

    public ArrayList<EntityMove> getMoves(int moveTypeID) {
        ArrayList<EntityMove> moves = new ArrayList<>();
        for(EntityMove move : this.moves.values())
            if(move.getMoveTypeID() == moveTypeID)
                moves.add(move);
        return moves;
    }


    public ArrayList<ReceiptMove> getReceiptMoves() {
        ArrayList<ReceiptMove> moves = new ArrayList<>();
        for(EntityMove move : this.moves.values())
            if(move.getMoveTypeID() == ReceiptMove.moveType_Receipt)
                moves.add((ReceiptMove) move);
        return moves;
    }

    public ArrayList<WriteOff> getWriteOffMoves() {
        ArrayList<WriteOff> moves = new ArrayList<>();
        for(EntityMove move : this.moves.values())
            if(move.getMoveTypeID() == WriteOff.move_type)
                moves.add((WriteOff) move);
        return moves;
    }

    public boolean existItem(final ShoesItem item) {
        for(EntityMove move : getMoves(ItemEntryMove.class)) {
            ItemEntryMove itemEntryMove = (ItemEntryMove) move;
            if(itemEntryMove.existItemEntry(item.getID()))
                return true;
        }
        return false;
    }
}
