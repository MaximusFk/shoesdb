package shoes.hml.shoesdb.handlers.comparators.shoes_items;

import java.util.Comparator;

import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 18.10.16.
 */

public class ShoesLabelComparator implements Comparator<ShoesItem> {
    @Override
    public int compare(ShoesItem item, ShoesItem t1) {
        return item.getLabel().compareTo(t1.getLabel());
    }
}
