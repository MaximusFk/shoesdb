package shoes.hml.shoesdb.handlers;

import android.content.ContentValues;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;

import shoes.hml.shoesdb.client.MoveTarget;
import shoes.hml.shoesdb.database.ContentValuesSerializable;
import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 04.02.16.
 */
public class Storage implements ContentValuesSerializable, MoveTarget, Serializable {

    public final static String ACCESS = "Storage";

    public final static String TABLE_NAME = "Storages";
    public final static String STORAGE_ID = "storage_id";
    public final static String STORAGE_NAME = "storage_name";
    public final static String LAST_UPDATE = "last_update";
    //public final static String ENTITIES_ID = "entities";

    long ID = (new java.util.Random()).nextLong();
    String storageName;
    ArrayList<StorageEntity> items;
    transient ShoesDBReaderHelper database;
    transient ShoesItemsList shoesList = null;

    public Storage(String storageName) {
        this.storageName = storageName;
        items = new ArrayList<>();
    }

    public Storage(String storageName, long ID) {
        this(storageName);
        this.ID = ID;
    }

    public Storage(String storageName, long ID, ShoesDBReaderHelper database) {
        this.storageName = storageName;
        this.ID = ID;
        this.database = database;
        this.items = this.database.getStorageEntities(this);
    }

    public void addEntity(ShoesItem item) {
        if(getEntity(item.getID()) == null) {
            long[] size = new long[(item.getMaxSize() - item.getMinSize())];
            StorageEntity storageEntity = new StorageEntity(this, item, 0, size);
            this.items.add(storageEntity);
            database.addEntity(storageEntity);
        }
    }

    public StorageEntity addEntity(ShoesItem item, long count) {
        if(!existItem(item)) {
            long[] size = new long[(item.getMaxSize() - item.getMinSize())];
            StorageEntity storageEntity = new StorageEntity(this, item, count, size);
            this.items.add(storageEntity);
            database.addEntity(storageEntity);
            return storageEntity;
        }
        return null;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getStorageName() {
        return storageName;
    }

    public StorageEntity getEntity(final ShoesItem item) {
        for(StorageEntity ent : items) {
            if(ent.getItem().equals(item))
                return ent;
        }
        return null;
    }

    public StorageEntity getEntity(long itemId) {
        for(StorageEntity ent : items) {
            if(ent.getItem().getID() == itemId)
                return ent;
        }
        return null;
    }

    public void insertDataFrom(Storage storage) {
        if(storage.getID() == ID)
            this.setStorageName(storage.getStorageName());
    }

    public boolean updateEntity(StorageEntity another) {
        StorageEntity entity;
        if((entity = getEntity(another.getItem().getID())) != null) {
            entity.setCount(another.getProductsCount());
            return true;
        }
        return false;
    }

    public ArrayList<ItemEntry> getItemEntries() {
        ArrayList<ItemEntry> entries = new ArrayList<>();
        for(StorageEntity entity : items)
            entries.add(entity);
        return entries;
    }

    public boolean existItem(final ShoesItem item) {
        for(StorageEntity entity : items) {
            if(entity.getItem().equals(item))
                return true;
        }
        return false;
    }

    public ShoesItemsList getShoesList() {
        return shoesList;
    }

    public void setShoesList(ShoesItemsList itemsList) {
        this.shoesList = itemsList;
    }

    public int size() {
        return items.size();
    }

    @Override
    public long getID() {
        return ID;
    }

    @Override
    public <T extends ItemEntry> void setItemContent(T[] itemContent) {
        for(T item : itemContent) {
            if(item instanceof StorageEntity) {
                this.items.add((StorageEntity) item);
            }
        }
    }

    @Override
    public ContentValues getContent() {
        ContentValues values = new ContentValues();
        values.put(STORAGE_ID, ID);
        values.put(STORAGE_NAME, storageName);
        //ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE);
        //for(StorageEntity id : this.items)
        //    buffer.putLong(id.getItem().getID());
        //values.put(ENTITIES_ID, buffer.array());
        return values;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPriorityIdKey() {
        return STORAGE_ID;
    }

    @Override
    public String getPriorityIdKeyWithValue() {
        return getPriorityIdKey() + '=' + ID;
    }

    public static class StorageEntity extends ItemEntry implements ContentValuesSerializable {

        public final static String TABLE_NAME = "Storage_Entities";
        public final static String ITEMS_COUNT = "sizes_count";
        //public final static String ALL_COUNT = "all_count";
        //public final static String ITEM_ID = "item_id";
        //public final static String PARENT_ID = "patent_id";

        Storage parent;
        HashMap<Integer, Long> sizesCount;

        public StorageEntity(Storage parent, ShoesItem item, long count, long[] counts) {
            super(item, count);
            this.parent = parent;
            if(counts != null && counts.length > 0) {
                sizesCount = new HashMap<>();
                int s = item.getMinSize();
                for(long size : counts) {
                    sizesCount.put(s++, size);
                    if(size > item.getMaxSize())
                        break;
                }
            }
        }

        public StorageEntity(ShoesItem item, long count, long[] counts) {
            super(item, count);
            if(counts != null && counts.length > 0) {
                sizesCount = new HashMap<>();
                int s = item.getMinSize();
                for(long size : counts) {
                    sizesCount.put(s++, size);
                    if(size > item.getMaxSize())
                        break;
                }
            }
        }

        private void calculateCount() {
            long c = 0;
            for(Long val : sizesCount.values())
                c += val;
            count = c;
        }

        public long getProductsCount() {
            long count = 0;
            for(java.util.Map.Entry<Integer, Long> ent : sizesCount.entrySet())
                count += ent.getValue();
            return count + (this.count > count ? this.count - count : count - this.count);
        }

        public long getCountBySize(int size) {
            if(sizesCount.containsKey(size))
                return sizesCount.get(size);
            else
                return 0;
        }

//        public ShoesItem getItem() {
//            return item;
//        }

        public Storage getParent() {
            return parent;
        }

        public boolean setCount(int size, long count) {
            if(sizesCount.containsKey(size)) {
                this.count += sizesCount.get(size) > count ? sizesCount.get(size) - count : count - sizesCount.get(size);
                sizesCount.put(size, count);
                return true;
            }
            return false;
        }

//        public void setCount(long count) {
//            this.count = count;
//        }

        public long addCount(int size, long count) {
            if(sizesCount.containsKey(size)) {
                sizesCount.put(size, count + sizesCount.get(size));
                addCount(count);
                return sizesCount.get(size);
            }
            return 0;
        }

//        public long addCount(long count) {
//            this.count += count;
//            return this.count;
//        }

        @Override
        public ContentValues getContent() {
            ContentValues values = super.getContent();
            //if(parent != null)
            //    values.put(PARENT_ID, parent.getID());
            //values.put(ITEM_ID, item.getID());
            //values.put(ALL_COUNT, count);
            ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE);
            for(long s : sizesCount.values())
                buffer.putLong(s);
            values.put(ITEMS_COUNT, buffer.array());
            return values;
        }

        @Override
        public String getTableName() {
            return TABLE_NAME;
        }

        @Override
        public String getPriorityIdKey() {
            return PARENT_ID;
        }

        @Override
        public String getPriorityIdKeyWithValue() {
            return getPriorityIdKey() + '=' + parent.getID();
        }
    }
}
