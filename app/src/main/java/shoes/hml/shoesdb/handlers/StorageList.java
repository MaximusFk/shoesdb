package shoes.hml.shoesdb.handlers;

import java.io.Serializable;
import java.util.ArrayList;

import shoes.hml.shoesdb.database.ShoesDBReaderHelper;

/**
 * Created by maximusfk on 24.02.16.
 */
public class StorageList implements Serializable {

    private ArrayList<Storage> storages;
    transient private ShoesDBReaderHelper database;

    public StorageList(ShoesDBReaderHelper database) {
        this.database = database;
        this.storages = database.getStorages();
        this.storages.add(0, database.getMainHandler().getMainStorage());
    }

    public void createStorage(String name) {
        addStorage(new Storage(name));
    }

    public void addStorage(Storage storage) {
        storages.add(storage);
        database.addStorage(storage);
    }

    public void updateStorage(Storage storage) {
        Storage found = getStorageByID(storage.getID());
        if (found != null) {
            found.insertDataFrom(storage);
            database.editEntry(found);
        }
    }

    public Storage getStorageByName(String name) {
        for(Storage storage : storages)
            if(storage.getStorageName().equals(name))
                return storage;
        return null;
    }

    public Storage getStorageByID(long _id) {
        for(Storage storage : storages)
            if(storage.getID() == _id)
                return storage;
        return null;
    }

    public boolean existStorage(long _id) {
        for (Storage storage : storages)
            if(storage.getID() == _id)
                return true;
        return false;
    }

    public ArrayList<Storage> getStorages() {
        return storages;
    }

    Storage[] toArray() {
        return (Storage[]) storages.toArray();
    }
}
