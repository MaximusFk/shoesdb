package shoes.hml.shoesdb.handlers;

import android.content.Context;

import java.io.Serializable;

import shoes.hml.shoesdb.AppSettings;
import shoes.hml.shoesdb.client.MoveTarget;
import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 24.02.16.
 */
public class MainHandler implements Serializable {

    final static public String ACCESS = "mainHandler";

    StorageList storageList;
    MoveList moveList;
    ReceiptList receiptMoveList;
    ShoesItemsList itemsList;
    DraftList draftList;
    Storage mainStorage;
    CategoryList categoryList;
    transient ShoesDBReaderHelper database;
    transient Context parentContext;

    public MainHandler(Context context, String dbName) {
        parentContext = context;
        database = new ShoesDBReaderHelper(context, dbName, this);
        categoryList = new CategoryList(database);
        itemsList = new ShoesItemsList("Items", database);
        mainStorage = database.getMainStorage();
        if(mainStorage == null) {
            mainStorage = new Storage("StorageMain", -1, database);
            database.addEntry(mainStorage);
        }
        //storageList = new StorageList(database);

        moveList = new MoveList(database);
        receiptMoveList = new ReceiptList(database);
        draftList = new DraftList(database);
    }

    public Storage getMainStorage() {
        return mainStorage;
    }

    public ShoesItemsList getItemsList() {
        return itemsList;
    }

    public boolean isUsed(final ShoesItem item) {
        boolean exist = false;
        exist = exist || moveList.existItem(item);
        exist = exist || receiptMoveList.existItem(item);
        exist = exist || draftList.existItem(item);
        return exist;
    }

    public MoveList getMoveList() {
        return moveList;
    }

    public ReceiptList getReceiptMoveList() {
        return receiptMoveList;
    }

    public DraftList getDraftList() {
        return draftList;
    }

    public StorageList getStorageList() {
        return storageList;
    }

    public CategoryList getCategoryList() {
        return categoryList;
    }

    public ShoesDBReaderHelper getDatabase() {
        return database;
    }

    public MoveTarget findTargetByID(long ID) {
        if(mainStorage.getID() == ID)
            return mainStorage;
//        for(Storage storage : storageList.toArray()) {
//            if(storage.getID() == ID)
//                return storage;
//        }
        return null;
    }
}
