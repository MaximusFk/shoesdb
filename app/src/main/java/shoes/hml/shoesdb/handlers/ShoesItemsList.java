package shoes.hml.shoesdb.handlers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeMap;

import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 04.02.16.
 */
public class ShoesItemsList implements Serializable {

    public static final String ACCESS = "shoesItemsList";

    private String listName;
    private TreeMap<Long, ShoesItem> items;
    private TreeMap<Long, ShoesItem> archived;
    private transient ShoesDBReaderHelper shoesDBReaderHelper;

    public ShoesItemsList(String name, ShoesDBReaderHelper db) {
        this.listName = name;
        this.items = db.getItems();
        this.archived = db.getArchivedItems();
        this.shoesDBReaderHelper = db;
    }

    public boolean addShoesItem(ShoesItem item) {
        if(items.containsKey(item.getID()))
            return false;

        items.put(item.getID(), item);
        shoesDBReaderHelper.addItem(item);
        shoesDBReaderHelper.close();
        return true;
    }

    public boolean updateItem(ShoesItem item) {
        ShoesItem item1 = items.get(item.getID());
        if(item1 != null) {
            shoesDBReaderHelper.editEntry(item);
            item1.insertDataFrom(item);
            return true;
        }
        return false;
    }

    public boolean deleteItem(ShoesItem item) {
        if(exist(item)) {
            shoesDBReaderHelper.deleteItem(item);
            items.remove(item.getID());
            return true;
        }
        return false;
    }

    public ShoesItem getItemByID(long _id) {
        ShoesItem item = items.get(_id);
        return item != null ? item : archived.get(_id);
    }

    public boolean inputToArchive(final ShoesItem item) {
        if(!existInArchive(item) && exist(item)) {
            archived.put(item.getID(), item);
            shoesDBReaderHelper.addArchiveItem(item);
            return deleteItem(item);
        }
        return false;
    }

    public ArrayList<ShoesItem> getItemsEqualsName(String name){
        ArrayList<ShoesItem> list = new ArrayList<>();
        for(ShoesItem item : items.values())
            if(item.getName().equals(name))
                list.add(item);
        return list;
    }

    public boolean exist(final ShoesItem item) {
        return items.containsKey(item.getID());
    }

    public boolean existInArchive(final ShoesItem item) {
        return archived.containsKey(item.getID());
    }

    public ArrayList<ShoesItem> getItems() {
        return new ArrayList<>(items.values());
    }
}
