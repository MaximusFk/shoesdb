package shoes.hml.shoesdb.handlers.comparators.item_entries;

import java.util.Comparator;

import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 18.10.16.
 */

public class ItemShoesComparator implements Comparator<ItemEntry> {

    Comparator<ShoesItem> comparator;

    public ItemShoesComparator(Comparator<ShoesItem> comparator) {
        this.comparator = comparator;
    }

    @Override
    public int compare(ItemEntry entry, ItemEntry t1) {
        return comparator.compare(entry.getItem(), t1.getItem());
    }
}
