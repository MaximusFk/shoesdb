package shoes.hml.shoesdb.handlers.comparators.shoes_items;

import java.util.Comparator;

import shoes.hml.shoesdb.handlers.comparators.category.CategoryNameComparator;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 03.10.16.
 */

public class ShoesCategoryComparator implements Comparator<ShoesItem> {
    @Override
    public int compare(ShoesItem item, ShoesItem t1) {
        return new CategoryNameComparator().compare(item.getMainCategory(), t1.getMainCategory());
    }
}
