package shoes.hml.shoesdb.handlers.comparators.shoes_items;

import java.util.Comparator;

import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 02.10.16.
 */

public class ShoesColorComparator implements Comparator<ShoesItem> {
    @Override
    public int compare(ShoesItem item, ShoesItem t1) {
        if(item.getMainColor().getColorId() == t1.getMainColor().getColorId())
            return item.getSecondColor().getColorId() == t1.getSecondColor().getColorId() ? 0 :
                    item.getSecondColor().getColorId() < t1.getSecondColor().getColorId() ? -1 : 1;
        else
            return item.getMainColor().getColorId() < t1.getMainColor().getColorId() ? -1 : 1;
    }
}
