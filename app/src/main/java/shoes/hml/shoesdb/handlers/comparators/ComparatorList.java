package shoes.hml.shoesdb.handlers.comparators;

/**
 * Created by maximusfk on 03.10.16.
 */

import java.util.Comparator;

public class ComparatorList <T> implements Comparator<T> {

    private Comparator<? extends T> [] comparators;

    public ComparatorList(Comparator<? extends T> ... comparators) {
        this.comparators = comparators;
    }

    @Override
    public int compare(T t, T t1) {
        int iret = 0;
        for(Comparator comparator : comparators) {
            iret = comparator.compare(t, t1);
            if(iret != 0)
                break;
        }
        return iret;
    }
}
