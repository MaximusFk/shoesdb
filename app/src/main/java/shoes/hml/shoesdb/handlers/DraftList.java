package shoes.hml.shoesdb.handlers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import shoes.hml.shoesdb.actions.Draft;
import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 21.03.16.
 */
public class DraftList implements Serializable {
    ArrayList<Draft> items;
    transient ShoesDBReaderHelper shoesDBReaderHelper;
    MainHandler mainHandler;

    public DraftList(ShoesDBReaderHelper database) {
        shoesDBReaderHelper = database;
        items = shoesDBReaderHelper.getDrafts();
        sortItemsArray();
    }

    public void addDraft(Draft draft) {
        items.add(draft);
        for(ItemEntry entry : draft.getItems()) {
            shoesDBReaderHelper.addEntry(entry);
        }
        shoesDBReaderHelper.addEntry(draft);
        sortItemsArray();
    }

    public void updateDraft(Draft draft) {
        if(exist(draft)) {
            Draft current = findDraft(draft.getID());
            for (ItemEntry entry : draft.getItems()) {
                if(!current.existItemEntry(entry.getEntryID())) {
                    current.addItem(entry);
                    shoesDBReaderHelper.addEntry(entry);
                } else {
                    current.findItemEntry(entry.getEntryID()).updateDataFrom(entry);
                    shoesDBReaderHelper.editEntry(entry);
                }
            }
            for(ItemEntry entry : current.getItems()) {
                if(!draft.existItemEntry(entry.getItem())) {
                    removeItemEntry(draft, entry);
                    shoesDBReaderHelper.deleteEntry(entry);
                }
            }
            current.insertDataFrom(draft);
            shoesDBReaderHelper.editEntry(draft);
            sortItemsArray();
        }
    }

    public void removeDraft(Draft draft) {
        items.remove(draft);
        for(ItemEntry entry : draft.getItems()) {
            shoesDBReaderHelper.deleteEntry(entry);
        }
        shoesDBReaderHelper.deleteEntry(draft);
        sortItemsArray();
    }

    public void removeItemEntry(Draft draft, ItemEntry entry) {
        draft.removeItemEntry(entry.getEntryID());
    }

    public boolean exist(Draft draft) {
        for(Draft ob : items) {
            if(ob.equals(draft))
                return true;
        }
        return false;
    }

    public Draft findDraft(long id) {
        for(Draft draft : items) {
            if(draft.getID() == id)
                return draft;
        }
        return null;
    }

    public boolean existItem(final ShoesItem item) {
        for(Draft draft : items) {
            if(draft.existItemEntry(item))
                return true;
        }
        return false;
    }

    public ArrayList<Draft> findDraftsByShoesItem(ShoesItem item) {
        ArrayList<Draft> drafts = new ArrayList<>();
        for(Draft draft : items) {
            if(draft.existItemEntry(item))
                drafts.add(draft);
        }
        return drafts;
    }

    public void sortItemsArray() {
        Object [] array = items.toArray();
        Arrays.sort(array);
        if(items == null)
            items = new ArrayList<>();
        else items.clear();
        for(Object ob : array)
            items.add((Draft) ob);
    }

    public ArrayList<Draft> getItems() {
        return items;
    }
}
