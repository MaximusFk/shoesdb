package shoes.hml.shoesdb.handlers.comparators.shoes_items;

import java.util.Comparator;

import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 11.10.16.
 */

public class ShoesWholeComparator implements Comparator<ShoesItem> {
    @Override
    public int compare(ShoesItem item, ShoesItem t1) {
        return item.getPriceWhole() == t1.getPriceWhole() ? 0 :
                item.getPriceWhole() < t1.getPriceWhole() ? -1 : 1;
    }
}
