package shoes.hml.shoesdb.handlers.comparators;

import java.util.Comparator;

/**
 * Created by maximusfk on 11.10.16.
 */

public class ComparatorInvert <T> implements Comparator<T> {

    Comparator<T> comparator;

    public ComparatorInvert(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    @Override
    public int compare(T o, T t1) {
        return -(comparator.compare(o, t1));
    }
}
