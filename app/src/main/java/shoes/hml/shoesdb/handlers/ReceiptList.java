package shoes.hml.shoesdb.handlers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import shoes.hml.shoesdb.actions.ReceiptMove;
import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 07.03.16.
 */
public class ReceiptList implements Serializable {
    private ArrayList<ReceiptMove> moves;
    transient private ShoesDBReaderHelper shoesDBReaderHelper;

    public ReceiptList(ShoesDBReaderHelper dbReaderHelper) {
        shoesDBReaderHelper = dbReaderHelper;
        this.moves = dbReaderHelper.getReceiptMoves();
        sortItemsArray();
    }

    public void addMove(ReceiptMove move) {
        if(!exist(move)){
            moves.add(move);
            shoesDBReaderHelper.addEntry(move);
            for(ItemEntry entry : move.getItems())
                shoesDBReaderHelper.addEntry(entry);
            for(ReceiptMove.ProviderEntry entry : move.getProviderEntries())
                shoesDBReaderHelper.addEntry(entry);
            sortItemsArray();
        }
    }

    public void removeMove(ReceiptMove move) {
        if(exist(move)) {
            for(ItemEntry itemEntry : move.getItems())
                shoesDBReaderHelper.deleteEntry(itemEntry);
            for(ReceiptMove.ProviderEntry providerEntry : move.getProviderEntries())
                shoesDBReaderHelper.deleteEntry(providerEntry);
            moves.remove(move);
            shoesDBReaderHelper.deleteEntry(move);
            sortItemsArray();
        }
    }

    public void updateMove(ReceiptMove move) {
        if(exist(move)) {
            ReceiptMove current = moves.get(indexOf(move));
            for(ItemEntry entry : current.getItems()) {
                if(!move.existItemEntry(entry.getEntryID())) {
                    removeItemEntry(current, entry);
                }
            }
            for(ItemEntry entry : move.getItems()) {
                if(!current.existItemEntry(entry.getEntryID())) {
                    current.addItem(entry);
                    shoesDBReaderHelper.addEntry(entry);
                }
                else {
                    current.findItemEntry(entry.getEntryID()).updateDataFrom(entry);
                    shoesDBReaderHelper.editEntry(entry);
                }
            }
            for(ReceiptMove.ProviderEntry entry : current.getProviderEntries()) {
                if(!move.existProviderEntry(entry)) {
                    removeProviderEntry(current, entry);
                }
            }
            for(ReceiptMove.ProviderEntry entry : move.getProviderEntries()) {
                if(!current.existProviderEntry(entry)) {
                    current.addProvider(entry);
                    shoesDBReaderHelper.addEntry(entry);
                } else {
                    current.findProviderEntry(entry).insertDataFrom(entry);
                    shoesDBReaderHelper.editEntry(entry);
                }
            }
            current.insertDataFrom(move);
            shoesDBReaderHelper.editEntry(current);
            sortItemsArray();
        }
    }

    public void removeItemEntry(ReceiptMove move, ItemEntry entry) {
        if(move.existItemEntry(entry.getEntryID())) {
            move.removeItemEntry(entry.getEntryID());
            shoesDBReaderHelper.deleteEntry(entry);
        }
    }

    public void removeProviderEntry(ReceiptMove move, ReceiptMove.ProviderEntry entry) {
        if(move.existProviderEntry(entry)) {
            move.removeProviderEntry(entry);
            shoesDBReaderHelper.deleteEntry(entry);
        }
    }

    public boolean exist(ReceiptMove move) {
        for(ReceiptMove move1 : moves)
            if(move.equals(move1))
                return true;
        return false;
    }

    public int indexOf(ReceiptMove move) {
        for(int i = 0; i < moves.size(); i++) {
            if(moves.get(i).equals(move))
                return i;
        }
        return -1;
    }

    public ArrayList<ReceiptMove> findMoveByShoesItem(ShoesItem item) {
        ArrayList<ReceiptMove> moves = new ArrayList<>();
        for(ReceiptMove move : this.moves) {
            if(move.existItemEntry(item))
                moves.add(move);
        }
        return moves;
    }

    public boolean existItem(final ShoesItem item) {
        for(ReceiptMove move : moves) {
            if(move.existItemEntry(item))
                return true;
        }
        return false;
    }

    public void sortItemsArray() {
        Object [] array = moves.toArray();
        Arrays.sort(array);
        if(moves == null)
            moves = new ArrayList<>();
        else moves.clear();
        for(Object ob : array)
            moves.add((ReceiptMove) ob);
    }

    public ArrayList<ReceiptMove> getMoves() {
        return moves;
    }
}
