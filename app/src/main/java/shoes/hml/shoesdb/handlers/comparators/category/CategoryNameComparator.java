package shoes.hml.shoesdb.handlers.comparators.category;

import java.util.Comparator;

import shoes.hml.shoesdb.items.ShoesCategory;

/**
 * Created by maximusfk on 03.10.16.
 */

public class CategoryNameComparator implements Comparator<ShoesCategory> {
    @Override
    public int compare(ShoesCategory shoesCategory, ShoesCategory t1) {
        return shoesCategory.getLabel().compareTo(t1.getLabel());
    }
}
