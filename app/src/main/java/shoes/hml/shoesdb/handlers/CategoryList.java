package shoes.hml.shoesdb.handlers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import shoes.hml.shoesdb.database.ShoesDBReaderHelper;
import shoes.hml.shoesdb.items.ShoesCategory;

/**
 * Created by maximusfk on 03.10.16.
 */

public class CategoryList {

    public static final String ACCESS = "CATEGORIES_LIST";

    private HashMap<Short, ShoesCategory> categories;

    transient private ShoesDBReaderHelper dbReaderHelper;

    public CategoryList(ShoesDBReaderHelper db) {
        dbReaderHelper = db;
        categories = db.getCategoies();
        categories.put(ShoesCategory.Default.getID(), ShoesCategory.Default);
    }

    public boolean addCategory(String label) {
        if(!categories.containsValue(label)) {
            short nID = getFreeID();
            ShoesCategory category = new ShoesCategory(label, nID);
            categories.put(nID, category);
            dbReaderHelper.addEntry(category);
            return true;
        }
        return false;
    }

    public boolean removeCategory(short ID) {
        if(categories.containsKey(ID) && ID != ShoesCategory.Default.getID()) {
            ShoesCategory category = categories.get(ID);
            dbReaderHelper.deleteEntry(category);
            categories.remove(ID);
            return true;
        }
        return false;
    }

    public ShoesCategory getCategory(short ID) {
        return categories.containsKey(ID) ? categories.get(ID) : ShoesCategory.Default;
    }

    private short getFreeID() {
        short ID = 1;
        while (categories.containsKey(ID)) {
            ++ID;
        }
        return ID;
    }

    public ArrayList<ShoesCategory> getArray() {
        ArrayList<ShoesCategory> categories = new ArrayList<>(this.categories.values());
        Collections.sort(categories);
        return categories;
    }
}
