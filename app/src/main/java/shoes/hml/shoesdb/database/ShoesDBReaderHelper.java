package shoes.hml.shoesdb.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import shoes.hml.shoesdb.actions.Draft;
import shoes.hml.shoesdb.actions.ReceiptMove;
import shoes.hml.shoesdb.actions.SaleMove;
import shoes.hml.shoesdb.handlers.CategoryList;
import shoes.hml.shoesdb.handlers.MainHandler;
import shoes.hml.shoesdb.handlers.Storage;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.items.ShoesCategory;
import shoes.hml.shoesdb.items.ShoesColor;
import shoes.hml.shoesdb.items.ShoesItem;

/**
 * Created by maximusfk on 06.02.16.
 */
public class ShoesDBReaderHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 9;

    private static final String CREATE_TABLE = "CREATE TABLE ";
    private static final String COMA = " , ";

    private final MainHandler mainHandler;

    public ShoesDBReaderHelper(Context parent, String dbName, MainHandler handler) {
        super(parent, dbName, null, DATABASE_VERSION);
        this.mainHandler = handler;
    }

    public MainHandler getMainHandler() {
        return mainHandler;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createShoesItemTable(db);
        createStorageTable(db);
        createStorageEntityTable(db);
        createItemEntryTable(db);
        createReceiptMoveTable(db);
        createProviderEntryTable(db);
        createDraftTable(db);
        createSaleMoveTable(db);
        createOldShoesItemTable(db);
        createCategoriesTable(db);
    }

    private void createShoesItemTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE + ShoesItem.TABLE_NAME + //Table for shoes items
                        "(" +
                        ShoesItem.ITEM_ID + " INTEGER PRIMARY KEY" + COMA +
                        ShoesItem.ITEM_NAME + " TEXT" + COMA +
                        ShoesItem.ITEM_MIN_SIZE + " INTEGER" + COMA +
                        ShoesItem.ITEM_MAX_SIZE + " INTEGER" + COMA +
                        ShoesItem.ITEM_PRICE_WHOLE + " REAL" + COMA +
                        ShoesItem.ITEM_PRICE_RETAIL + " REAL" + COMA +
                        ShoesItem.ITEM_PRICE_COST + " REAL" + COMA +
                        ShoesItem.ITEM_COLOR_MAIN + " INTEGER" + COMA +
                        ShoesItem.ITEM_COLOR_SECOND + " INTEGER" + COMA +
                        ShoesItem.ITEM_LABEL + " TEXT" + COMA +
                        ShoesItem.ITEM_MANUFACTURER + " TEXT" + COMA +
                        ShoesItem.ITEM_CATEGORY + " INTEGER(2)"
                        + ")"

        );
    }

    private void createOldShoesItemTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE + ShoesItem.TABLE_NAME + "_old" +//Table for deleted or old or archived shoes items
                "(" +
                ShoesItem.ITEM_ID + " INTEGER PRIMARY KEY" + COMA +
                ShoesItem.ITEM_NAME + " TEXT" + COMA +
                ShoesItem.ITEM_MIN_SIZE + " INTEGER" + COMA +
                ShoesItem.ITEM_MAX_SIZE + " INTEGER" + COMA +
                ShoesItem.ITEM_PRICE_WHOLE + " REAL" + COMA +
                ShoesItem.ITEM_PRICE_RETAIL + " REAL" + COMA +
                ShoesItem.ITEM_PRICE_COST + " REAL" + COMA +
                ShoesItem.ITEM_COLOR_MAIN + " INTEGER" + COMA +
                ShoesItem.ITEM_COLOR_SECOND + " INTEGER" + COMA +
                ShoesItem.ITEM_LABEL + " TEXT" + COMA +
                ShoesItem.ITEM_MANUFACTURER + " TEXT" + COMA +
                ShoesItem.ITEM_CATEGORY + " INTEGER(2)"
                + ")"

        );
    }

    private void createStorageTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE + Storage.TABLE_NAME +
                        "(" +
                        Storage.STORAGE_ID + " INTEGER(8) PRIMARY KEY , " +
                        Storage.STORAGE_NAME + " TEXT"// +
                        //Storage.ENTITIES_ID + " BLOB"
                        + ")"
        );
    }

    private void createStorageEntityTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE + Storage.StorageEntity.TABLE_NAME +
                        "(" +
                        Storage.StorageEntity.ENTRY_ID + " INTEGER(8) PRIMARY KEY , " +
                        Storage.StorageEntity.PARENT_ID + " INTEGER(8) , " +
                        Storage.StorageEntity.ITEM_ID + " INTEGER(8) , " +
                        ItemEntry.ITEMS_COUNT + " INTEGER(8) , " +
                        Storage.StorageEntity.ITEMS_COUNT + " BLOB"
                        + ")"
        );
    }

    private void createReceiptMoveTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE + ReceiptMove.TABLE_NAME +
                        "(" +
                        ReceiptMove.MOVE_ID + " INTEGER(8) PRIMARY KEY , " +
                        ReceiptMove.CREATE_DATE + " INTEGER(8) , " +
                        ReceiptMove.CUSTOM_DATE + " INTEGER(8) , " +
                        ReceiptMove.FROM_TARGET_ID + " INTEGER(8) , " +
                        ReceiptMove.TO_TARGET_ID + " INTEGER(8)"
                        + ")"

        );
    }

    private void createItemEntryTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE + ItemEntry.TABLE_NAME +
                        "(" +
                        ItemEntry.ENTRY_ID + " INTEGER(8) PRIMARY KEY , " +
                        ItemEntry.ITEM_ID + " INTEGER(8) , " +
                        ItemEntry.ITEMS_COUNT + " INTEGER(8) , " +
                        ItemEntry.PARENT_ID + " INTEGER(8)"
                        + ")"
        );
    }

    private void createProviderEntryTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE + ReceiptMove.ProviderEntry.TABLE_NAME +
                "(" +
                        ReceiptMove.ProviderEntry.ENTRY_ID + " INTEGER(8) PRIMARY KEY , " +
                        ReceiptMove.ProviderEntry.PROVIDER_NAME + " TEXT , " +
                        ReceiptMove.ProviderEntry.ITEMS_COUNT + " INTEGER(8) , " +
                        ReceiptMove.ProviderEntry.PARENT_ID + " INTEGER(8)"
                + ")"
        );
    }

    private void createDraftTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE + Draft.TABLE_NAME +
                        "(" +
                        Draft.DRAFT_ID + " INTEGER(8) PRIMARY KEY , " +
                        Draft.LABEL + " TEXT , " +
                        Draft.CUSTOM_DATE + " INTEGER(8) , " +
                        Draft.MAKE_DATE + " INTEGER(8)"
                        + ")"
        );
    }

    private void createSaleMoveTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE + SaleMove.TABLE_NAME +
                "(" +
                SaleMove.MOVE_ID + " INTEGER(8) PRIMARY KEY , " +
                SaleMove.DEBIT + " REAL , " +
                SaleMove.DEBITOFF + " REAL , " +
                SaleMove.CREATE_DATE + " INTEGER(8) , " +
                SaleMove.CUSTOM_DATE + " INTEGER(8) , " +
                SaleMove.FROM_TARGET_ID + " INTEGER(8) , " +
                SaleMove.TO_TARGET_ID + " INTEGER(8) , " +
                SaleMove.SALE_TYPE_ID + " INTEGER , " +
                SaleMove.HOLD + " INTEGER" +
                ")"
        );
    }

    private void createCategoriesTable(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE + ShoesCategory.TABLE_NAME +
                "(" +
                ShoesCategory.CID + " INTEGER(2) PRIMARY KEY , " +
                ShoesCategory.LABEL + " TEXT , " +
                ShoesCategory.VISIBLE + " INTEGER" +
                ")"
        );
    }

    public void addItem(final ShoesItem item) {
        this.getWritableDatabase().insert(ShoesItem.TABLE_NAME, null, item.getContent());
    }

    public void addArchiveItem(final ShoesItem item) {
        this.getWritableDatabase().insert(ShoesItem.TABLE_NAME + "_old", null, item.getContent());
    }

    public void addEntity(final Storage.StorageEntity entity) {
        this.getWritableDatabase().insert(Storage.StorageEntity.TABLE_NAME, null, entity.getContent());
    }

    public void addStorage(final Storage storage) {
        this.getWritableDatabase().insert(Storage.TABLE_NAME, null, storage.getContent());
    }

    public <Type extends ContentValuesSerializable> void addEntry(Type entry) {
        assert (entry != null);
        this.getWritableDatabase().insert(entry.getTableName(), null, entry.getContent());
    }

    public void editItem(final ShoesItem item) {
        this.getWritableDatabase().update(ShoesItem.TABLE_NAME, item.getContentWithNoID(),
                ShoesItem.ITEM_ID + "=" + item.getID(), null);
    }

    public void editArchiveItem(final ShoesItem item) {
        this.getWritableDatabase().update(ShoesItem.TABLE_NAME + "_old", item.getContentWithNoID(),
                ShoesItem.ITEM_ID + "=" + item.getID(), null);
    }

    public <Type extends ContentValuesSerializable> void editEntry(final Type entry) {
        assert (entry != null);
        this.getWritableDatabase().update(entry.getTableName(), entry.getContent(), entry.getPriorityIdKeyWithValue(), null);
    }

    public void deleteItem(final ShoesItem item) {
        this.getWritableDatabase().delete(ShoesItem.TABLE_NAME,
                ShoesItem.ITEM_ID + "=" + item.getID(), null);
    }

    public void deleteArchiveItem(final ShoesItem item) {
        this.getWritableDatabase().delete(ShoesItem.TABLE_NAME + "_old",
                ShoesItem.ITEM_ID + "=" + item.getID(), null);
    }

    public <Type extends ContentValuesSerializable> void deleteEntry(final Type move) {
        this.getWritableDatabase().delete(move.getTableName(), move.getPriorityIdKeyWithValue(), null);
    }

    public HashMap<Short, ShoesCategory> getCategoies() {
        Cursor cursor = this.getReadableDatabase().query(ShoesCategory.TABLE_NAME, null, null, null, null, null, null);
        if(!cursor.moveToFirst())
            return new HashMap<>();
        HashMap<Short, ShoesCategory> categories = new HashMap<>();
        do {
            ShoesCategory category = new ShoesCategory(
                    cursor.getString(cursor.getColumnIndex(ShoesCategory.LABEL)),
                    (cursor.getInt(cursor.getColumnIndex(ShoesCategory.VISIBLE)) == 0),
                    cursor.getShort(cursor.getColumnIndex(ShoesCategory.CID))
            );
            categories.put(category.getID(), category);
        } while (cursor.moveToNext());
        return categories;
    }

    public ArrayList<Storage.StorageEntity> getStorageEntities(Storage caller) {
        Cursor cursor = this.getReadableDatabase().query(Storage.StorageEntity.TABLE_NAME, null, null, null, null, null, null);
        if(!cursor.moveToFirst() || caller == null)
            return new ArrayList<>();
        ArrayList<Storage.StorageEntity> entities = new ArrayList<>();
        do {
            long ID = cursor.getLong(cursor.getColumnIndexOrThrow(Storage.StorageEntity.PARENT_ID));
            if(ID != caller.getID())
                continue;
            ShoesItem item = mainHandler.getItemsList().getItemByID(cursor.getLong(cursor.getColumnIndexOrThrow(Storage.StorageEntity.ITEM_ID)));
            Storage.StorageEntity entity = new Storage.StorageEntity(caller, item, cursor.getLong(cursor.getColumnIndexOrThrow(ItemEntry.ITEMS_COUNT)), null);
            entity.setEntryID(cursor.getLong(cursor.getColumnIndexOrThrow(Storage.StorageEntity.ENTRY_ID)));
            entities.add(entity);
        } while (cursor.moveToNext());
        return entities;
    }

    public Storage getMainStorage() {
        Cursor cursor = this.getReadableDatabase().query(Storage.TABLE_NAME, null,
                Storage.STORAGE_NAME + " LIKE ?", new String[]{"StorageMain"},
                null, null, null);
        if(!cursor.moveToFirst())
            return null;
        return new Storage(cursor.getString(cursor.getColumnIndexOrThrow(Storage.STORAGE_NAME)),
                cursor.getLong(cursor.getColumnIndexOrThrow(Storage.STORAGE_ID)),
                this
        );
    }

    public ArrayList<Storage> getStorages() {
        Cursor cursor = this.getReadableDatabase().query(Storage.TABLE_NAME, null, null, null, null, null, null);
        if(!cursor.moveToFirst())
            return new ArrayList<>();
        ArrayList<Storage> storages = new ArrayList<>();
        do {
            Storage storage = new Storage(cursor.getString(cursor.getColumnIndexOrThrow(Storage.STORAGE_NAME)),
                    cursor.getLong(cursor.getColumnIndexOrThrow(Storage.STORAGE_ID)),
                    this
                    );
            storages.add(storage);
        } while (cursor.moveToNext());
        return storages;
    }

    public ArrayList<ReceiptMove> getReceiptMoves() {
        Cursor cursor = this.getReadableDatabase().query(ReceiptMove.TABLE_NAME, null, null, null, null, null, null);
        if(!cursor.moveToFirst())
            return new ArrayList<>();
        ArrayList<ReceiptMove> moves = new ArrayList<>();
        do {
            ReceiptMove move = new ReceiptMove(cursor.getLong(cursor.getColumnIndexOrThrow(ReceiptMove.MOVE_ID)),
                        (new Date(cursor.getLong(cursor.getColumnIndexOrThrow(ReceiptMove.CREATE_DATE)))),
                        (new Date(cursor.getLong(cursor.getColumnIndexOrThrow(ReceiptMove.CUSTOM_DATE)))),
                        this
                    );
            if(!cursor.isNull(cursor.getColumnIndexOrThrow(ReceiptMove.TO_TARGET_ID)))
                move.setTo(mainHandler.findTargetByID(cursor.getLong(cursor.getColumnIndexOrThrow(ReceiptMove.TO_TARGET_ID))));
            if(!cursor.isNull(cursor.getColumnIndexOrThrow(ReceiptMove.FROM_TARGET_ID)))
                move.setFrom(mainHandler.findTargetByID(cursor.getLong(cursor.getColumnIndexOrThrow(ReceiptMove.FROM_TARGET_ID))));
            move.setProviderEntries(getProviderEntries(move.getID()));
            moves.add(move);
        } while (cursor.moveToNext());
        return moves;
    }

    public ArrayList<Draft> getDrafts() {
        Cursor cursor = this.getReadableDatabase().query(Draft.TABLE_NAME, null, null, null, null, null, null);
        if(!cursor.moveToFirst())
            return new ArrayList<>();
        ArrayList<Draft> drafts = new ArrayList<>();
        do {
            Draft draft = new Draft(
                    cursor.getString(cursor.getColumnIndexOrThrow(Draft.LABEL)),
                    (new Date(cursor.getLong(cursor.getColumnIndexOrThrow(Draft.CUSTOM_DATE)))),
                    (new Date(cursor.getLong(cursor.getColumnIndexOrThrow(Draft.MAKE_DATE)))),
                    cursor.getLong(cursor.getColumnIndexOrThrow(Draft.DRAFT_ID)),
                    this
            );
            drafts.add(draft);
        } while (cursor.moveToNext());
        return drafts;
    }

    public List<SaleMove> getSaleMoves() {
        Cursor cursor = this.getReadableDatabase().query(SaleMove.TABLE_NAME, null, null, null, null, null, null);
        if(!cursor.moveToFirst())
            return new LinkedList<>();
        ArrayList<SaleMove> saleMoves = new ArrayList<>();
        do {
            SaleMove saleMove = new SaleMove(
                    cursor.getLong(cursor.getColumnIndex(SaleMove.MOVE_ID)),
                    new Date(cursor.getLong(cursor.getColumnIndex(SaleMove.CREATE_DATE))),
                    new Date(cursor.getLong(cursor.getColumnIndex(SaleMove.CUSTOM_DATE))),
                    cursor.getFloat(cursor.getColumnIndex(SaleMove.DEBIT)),
                    this
            );
            saleMove.setTarget(mainHandler.findTargetByID(cursor.getLong(cursor.getColumnIndex(SaleMove.TO_TARGET_ID))));
            saleMove.setStorage((Storage) mainHandler.findTargetByID(cursor.getLong(cursor.getColumnIndex(SaleMove.FROM_TARGET_ID))));
            saleMove.setSaleType(SaleMove.SaleType.getTypeByID((byte) cursor.getInt(cursor.getColumnIndex(SaleMove.SALE_TYPE_ID))));
            saleMove.setDebitOff(cursor.getFloat(cursor.getColumnIndex(SaleMove.DEBITOFF)));
            saleMoves.add(saleMove);
        } while (cursor.moveToNext());
        return saleMoves;
    }

    public TreeMap<Long, ItemEntry> getItemEntries(long parentID) {
        Cursor cursor = this.getReadableDatabase().query(ItemEntry.TABLE_NAME, null,
                ItemEntry.PARENT_ID + " LIKE ?", (new String[]{new StringBuffer().append(parentID).toString()}), null, null, null);
        if(!cursor.moveToFirst())
            return new TreeMap<>();
        TreeMap<Long, ItemEntry> entries = new TreeMap<>();
        do {
            long ID = 0L;
//            if(!cursor.isNull(cursor.getColumnIndexOrThrow(ItemEntry.PARENT_ID))) {
                ID = cursor.getLong(cursor.getColumnIndexOrThrow(ItemEntry.PARENT_ID));
//                if(ID != 0L && ID != parentID)
//                    continue;
//            }
            ShoesItem item = mainHandler.getItemsList().getItemByID(cursor.getLong(cursor.getColumnIndexOrThrow(ItemEntry.ITEM_ID)));
            ItemEntry itemEntry = new ItemEntry(item,
                    cursor.getLong(cursor.getColumnIndexOrThrow(ItemEntry.ITEMS_COUNT)),
                    cursor.getLong(cursor.getColumnIndexOrThrow(ItemEntry.ENTRY_ID))
            );
            itemEntry.setParentID(ID);
            entries.put(itemEntry.getEntryID(), itemEntry);
        } while (cursor.moveToNext());
        return entries;
    }

    public ArrayList<ReceiptMove.ProviderEntry> getProviderEntries(long parentID) {
        Cursor cursor = this.getReadableDatabase().query(ReceiptMove.ProviderEntry.TABLE_NAME,
                null, ReceiptMove.ProviderEntry.PARENT_ID + " LIKE ?", (new String[]{new StringBuffer().append(parentID).toString()}), null, null, null);
        if(!cursor.moveToFirst())
            return new ArrayList<>();
        ArrayList<ReceiptMove.ProviderEntry> providerEntries = new ArrayList<>();
        do {
            ReceiptMove.ProviderEntry newEntry = new ReceiptMove.ProviderEntry(
                    cursor.getString(cursor.getColumnIndexOrThrow(ReceiptMove.ProviderEntry.PROVIDER_NAME)),
                    cursor.getLong(cursor.getColumnIndexOrThrow(ReceiptMove.ProviderEntry.ITEMS_COUNT)),
                    cursor.getLong(cursor.getColumnIndexOrThrow(ReceiptMove.ProviderEntry.PARENT_ID)),
                    cursor.getLong(cursor.getColumnIndexOrThrow(ReceiptMove.ProviderEntry.ENTRY_ID))
            );
            providerEntries.add(newEntry);
        } while (cursor.moveToNext());
        return providerEntries;
    }

    public TreeMap<Long, ShoesItem> getItems() {
        Cursor cursor = this.getReadableDatabase().query(ShoesItem.TABLE_NAME, null, null, null, null, null, null);
        if(!cursor.moveToFirst())
            return new TreeMap<>();
        TreeMap <Long, ShoesItem> items = new TreeMap<>();
        do {
            ShoesItem newItem = new ShoesItem(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_NAME)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MIN_SIZE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MAX_SIZE)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_RETAIL)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_WHOLE)),
                    ShoesColor.getColor(cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_COLOR_MAIN))),
                    ShoesColor.getColor(cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_COLOR_SECOND))),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_COST))
            );
            newItem.setID(cursor.getLong(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_ID)));
            newItem.setLabel(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_LABEL)));
            newItem.setManufacturer(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MANUFACTURER)));
            newItem.setMainCategory(mainHandler.getCategoryList().getCategory(cursor.getShort(cursor.getColumnIndex(ShoesItem.ITEM_CATEGORY))));
            items.put(newItem.getID(), newItem);
        } while(cursor.moveToNext());
        return items;
    }

    public TreeMap<Long, ShoesItem> getArchivedItems() {
        Cursor cursor = this.getReadableDatabase().query(ShoesItem.TABLE_NAME + "_old", null, null, null, null, null, null);
        if(!cursor.moveToFirst())
            return new TreeMap<>();
        TreeMap<Long, ShoesItem> items = new TreeMap<>();
        do {
            ShoesItem newItem = new ShoesItem(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_NAME)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MIN_SIZE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MAX_SIZE)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_RETAIL)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_WHOLE)),
                    ShoesColor.getColor(cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_COLOR_MAIN))),
                    ShoesColor.getColor(cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_COLOR_SECOND))),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_COST))
            );
            newItem.setID(cursor.getLong(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_ID)));
            newItem.setLabel(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_LABEL)));
            newItem.setManufacturer(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MANUFACTURER)));
            items.put(newItem.getID(), newItem);
        } while(cursor.moveToNext());
        return items;
    }

    private void convertProviderEntriesTable(SQLiteDatabase db) {
        ArrayList<ReceiptMove.ProviderEntry> entries = new ArrayList<>();
        Cursor cursor = db.query(ReceiptMove.ProviderEntry.TABLE_NAME, null, null, null, null, null, null);
        if(!cursor.moveToFirst())
            return;
        do {
            ReceiptMove.ProviderEntry entry = new ReceiptMove.ProviderEntry(
                    cursor.getString(cursor.getColumnIndexOrThrow(ReceiptMove.ProviderEntry.PROVIDER_NAME)),
                    cursor.getLong(cursor.getColumnIndexOrThrow(ReceiptMove.ProviderEntry.ITEMS_COUNT)),
                    cursor.getLong(cursor.getColumnIndexOrThrow(ReceiptMove.ProviderEntry.PARENT_ID))
            );
            entries.add(entry);
        } while (cursor.moveToNext());
        db.execSQL("DROP TABLE IF EXISTS " + ReceiptMove.ProviderEntry.TABLE_NAME);
        createProviderEntryTable(db);
        for(ReceiptMove.ProviderEntry entry : entries)
            db.insert(ReceiptMove.ProviderEntry.TABLE_NAME, null, entry.getContent());
    }

    private void convertShoesItemsTable(SQLiteDatabase db) {
        ArrayList<ShoesItem> items = new ArrayList<>();
        Cursor cursor = db.query(ShoesItem.TABLE_NAME, null, null, null, null, null, null);
        if(!cursor.moveToFirst())
            return;
        do {
            ShoesItem newItem = new ShoesItem(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_NAME)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MIN_SIZE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MAX_SIZE)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_RETAIL)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_WHOLE)),
                    ShoesColor.getColor(cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_COLOR_MAIN))),
                    ShoesColor.getColor(cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_COLOR_SECOND))),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_COST))
            );
            newItem.setID(cursor.getLong(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_ID)));
            newItem.setLabel(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_LABEL)));
            newItem.setManufacturer(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MANUFACTURER)));
            items.add(newItem);
        } while(cursor.moveToNext());
        db.execSQL("DROP TABLE IF EXISTS " + ShoesItem.TABLE_NAME);
        createShoesItemTable(db);
        for(ShoesItem item : items)
            db.insert(ShoesItem.TABLE_NAME, null, item.getContent());
    }

    private void convertShoesArchivedItemsTable(SQLiteDatabase db) {
        ArrayList<ShoesItem> items = new ArrayList<>();

        Cursor cursor = db.query(ShoesItem.TABLE_NAME + "_old", null, null, null, null, null, null);
        if(!cursor.moveToFirst())
            return;
        do {
            ShoesItem newItem = new ShoesItem(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_NAME)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MIN_SIZE)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MAX_SIZE)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_RETAIL)),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_WHOLE)),
                    ShoesColor.getColor(cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_COLOR_MAIN))),
                    ShoesColor.getColor(cursor.getInt(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_COLOR_SECOND))),
                    cursor.getFloat(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_PRICE_COST))
            );
            newItem.setID(cursor.getLong(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_ID)));
            newItem.setLabel(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_LABEL)));
            newItem.setManufacturer(cursor.getString(cursor.getColumnIndexOrThrow(ShoesItem.ITEM_MANUFACTURER)));
            items.add(newItem);
        } while(cursor.moveToNext());
        db.execSQL("DROP TABLE IF EXISTS " + ShoesItem.TABLE_NAME + "_old");
        createOldShoesItemTable(db);
        for(ShoesItem item : items)
            db.insert(ShoesItem.TABLE_NAME + "_old", null, item.getContent());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion < newVersion) {
            if(newVersion >= 2 && oldVersion < 2) {
                createStorageTable(db);
                createStorageEntityTable(db);
                createItemEntryTable(db);
                createReceiptMoveTable(db);
            }
            if(newVersion >= 3 && oldVersion < 3) {
                createProviderEntryTable(db);
            }
            if(newVersion >= 4 && oldVersion < 4) {
                convertProviderEntriesTable(db);
            }
            if(newVersion >= 5 && oldVersion < 5) {
                createDraftTable(db);
            }
            if(newVersion >= 6 && oldVersion < 6) {
                db.execSQL("DROP TABLE IF EXISTS " + Storage.StorageEntity.TABLE_NAME);
                createStorageEntityTable(db);
            }
            if(newVersion >= 7 && oldVersion < 7) {
                createOldShoesItemTable(db);
            }
            if(newVersion >= 8 && oldVersion < 8) {
                createSaleMoveTable(db);
            }
            if(newVersion >= 9 && oldVersion < 9) {
                createCategoriesTable(db);
                convertShoesItemsTable(db);
                convertShoesArchivedItemsTable(db);
            }
        }
    }
}
