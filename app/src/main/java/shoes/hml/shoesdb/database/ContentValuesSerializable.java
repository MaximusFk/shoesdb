package shoes.hml.shoesdb.database;

import android.content.ContentValues;

/**
 * Created by maximusfk on 25.02.16.
 */
public interface ContentValuesSerializable {
    ContentValues getContent();
    String getTableName();
    String getPriorityIdKey();
    String getPriorityIdKeyWithValue();
}
