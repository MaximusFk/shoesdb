package shoes.hml.shoesdb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import shoes.hml.shoesdb.handlers.MainHandler;

public class HeadMenu extends AppCompatActivity implements View.OnClickListener {

    ImageButton receipt;
    ImageButton draft;
    ImageButton sale;
    ImageButton writeoff;
    Button sshoes;
    Button storage;

    private static MainHandler mainHandler = null; public MainHandler getMainHandler() {return mainHandler;}
    private static AppSettings settings; public AppSettings getSettings() {return settings;}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_head_menu);
        if(mainHandler == null) {
            settings = new AppSettings(this);
            mainHandler = new MainHandler(this, "shoes_test.db");
        }
        receipt = (ImageButton) findViewById(R.id.main_receipts);
        draft = (ImageButton) findViewById(R.id.main_drafts);
        sale = (ImageButton) findViewById(R.id.main_sales);
        writeoff = (ImageButton) findViewById(R.id.main_writeoff);
        sshoes = (Button) findViewById(R.id.main_items);
        storage = (Button) findViewById(R.id.main_storage);

        receipt.setOnClickListener(this);
        draft.setOnClickListener(this);
        sale.setOnClickListener(this);
        writeoff.setOnClickListener(this);
        sshoes.setOnClickListener(this);
        storage.setOnClickListener(this);
        //sizeInit();
    }

    private void startListActivity(Main2Activity.ListType type) {
        Main2Activity.args.setArg1(type);
        Main2Activity.args.setArg2(mainHandler);
        final Intent i = new Intent(this, Main2Activity.class);
        startActivity(i);
    }

    @Override
    public void onClick(View view) {
        Main2Activity.ListType type;
        switch (view.getId()) {
            case R.id.main_receipts:
                type = Main2Activity.ListType.RECEIPTS;
                break;
            case R.id.main_drafts:
                type = Main2Activity.ListType.DRAFTS;
                break;
            case R.id.main_sales:
                type = Main2Activity.ListType.SALES;
                break;
            case R.id.main_items:
                type = Main2Activity.ListType.SHOES;
                break;
            case R.id.main_storage:
                type = Main2Activity.ListType.STORAGE;
                break;
            case R.id.main_writeoff:
                type = Main2Activity.ListType.WRITEOFFS;
                break;
            default:
                onClick2(view);
                return;
        }
        startListActivity(type);
    }

    private void onClick2(View view) {
        switch (view.getId()) {
            case R.id.main_receipts_add:
            case R.id.main_sales_add:
            case R.id.main_writeoff_add:
        }
    }
}
