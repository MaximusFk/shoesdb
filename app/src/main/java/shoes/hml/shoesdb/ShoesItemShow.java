package shoes.hml.shoesdb;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import shoes.hml.shoesdb.items.ShoesItem;

public class ShoesItemShow extends ActionBarActivity {

    ShoesItem item;
    Main2Activity mainActivity;

    ImageButton editButton;
    ImageButton deleteButton;

    AlertDialog.Builder deleteAlertBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoes_item_show);
        final TextView name = (TextView) findViewById(R.id.itemName),
                color = (TextView) findViewById(R.id.ItemColor),
                size = (TextView) findViewById(R.id.ItemSize),
                retail = (TextView) findViewById(R.id.retailPrice),
                whole = (TextView) findViewById(R.id.wholePrice),
                label = (TextView) findViewById(R.id.label),
                manuf = (TextView) findViewById(R.id.manufacturer),
                ID = (TextView) findViewById(R.id.itemID);
        item = (ShoesItem) this.getIntent().getSerializableExtra(ShoesItem.ACCESS);
        mainActivity = Main2Activity.once;
        if(item == null) {
            finish();
            return;
        }
        deleteAlertBuilder = new AlertDialog.Builder(this);
        deleteAlertBuilder.setMessage(getString(R.string.alert_delete))
                .setPositiveButton(getString(R.string.answ_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onDeleteClick();
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.answ_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finish();
                    }
                })
                .setTitle(getString(R.string.alert_delete_head));
        editButton = new ImageButton(this);
        //editButton.setBackgroundColor(Color.TRANSPARENT);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEditClick();
                finish();
            }
        });
        editButton.setImageResource(R.drawable.ic_edit_document_button);
        editButton.setBackgroundColor(Color.TRANSPARENT);
        deleteButton = new ImageButton(this);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAlertBuilder.show();
            }
        });
        deleteButton.setImageResource(R.drawable.ic_delete_document_button);
        deleteButton.setBackgroundColor(Color.TRANSPARENT);
        Toolbar bar = (Toolbar) findViewById(R.id.action_bar);
        bar.addView(editButton);
        bar.addView(deleteButton);
        name.setText(item.getName());
        color.setText(item.getMainColor().getLocalizeName() + ", " + item.getSecondColor().getLocalizeName());
        size.setText(item.getMinSize() + " - " + item.getMaxSize());
        retail.setText("" + item.getPriceRetail());
        whole.setText("" + item.getPriceWhole());
        label.setText(item.getLabel());
        manuf.setText(item.getManufacturer());
        ID.setText("" + item.getID());
        File image = item.getImageFile(this);
        if(image != null)
            ((ImageView)findViewById(R.id.shoes_image_siw)).setImageBitmap(item.loadImage(this));
    }

    private void onEditClick() {
        mainActivity.startEditShoesItemActivity(item);
    }

    private void onDeleteClick() {
        Intent result = new Intent();
        result.putExtra(ShoesItem.ACCESS, item);
        this.setResult(Main2Activity.ResultType.DELETE_ITEM.getId(), result);
        finish();
    //    if(mainActivity.getItems().deleteItem(item)) {
    //        Toast.makeText(this, getString(R.string.popup_sitem_deleted), Toast.LENGTH_SHORT).show();
    //        mainActivity.updateListAdapter();
    //    }
    }
}
