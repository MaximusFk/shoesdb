package shoes.hml.shoesdb.items;

import android.content.ContentValues;

import java.io.Serializable;

import shoes.hml.shoesdb.database.ContentValuesSerializable;

/**
 * Created by maximusfk on 26.02.16.
 */
public class ItemEntry implements ContentValuesSerializable, Serializable {

    public final static String ACCESS = "itemEntry";

    public final static String TABLE_NAME = "Item_Entries";
    public final static String ENTRY_ID = "entry_id";
    public final static String ITEM_ID = "item_id";
    public final static String ITEMS_COUNT = "items_count";
    public final static String PARENT_ID = "parent_id";

    protected ShoesItem item;
    protected long count;
    protected long entryID = (new java.util.Random()).nextLong();
    protected long parentID = 0L;

    public ItemEntry(ShoesItem item) {
        count = 0;
        this.item = item;
    }

    public ItemEntry(ShoesItem item, long count) {
        this.item = item;
        this.count = count;
    }

    public ItemEntry(ShoesItem item, long count, long entryID) {
        this.item = item;
        this.count = count;
        this.entryID = entryID;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public void addCount(long count) {
        this.count += count;
    }

    public long getEntryID() {
        return entryID;
    }

    public void setEntryID(long entryID) {
        this.entryID = entryID;
    }

    public ShoesItem getItem() {
        return item;
    }

    public void setItem(ShoesItem item) {
        this.item = item;
    }

    public long getParentID() {
        return parentID;
    }

    public void setParentID(long parentID) {
        this.parentID = parentID;
    }

    public float getWholeSum() {
        return getItem().getPriceWhole() * getCount();
    }

    public boolean isItem(ShoesItem item) {
        return this.item.equals(item);
    }

    public boolean isItem(ItemEntry another) {
        return this.item.equals(another.item);
    }

    public float getRetailSum() {
        return getItem().getPriceRetail() * getCount();
    }

    public void updateDataFrom(ItemEntry another) {
        if(this.getItem().equals(another.getItem()) || this.getEntryID() == another.getEntryID()) {
            this.entryID = another.entryID;
            this.parentID = another.parentID;
            this.count = another.count;
            this.setItem(another.getItem());
        }
    }

    @Override
    public ContentValues getContent() {
        ContentValues values = new ContentValues();
        values.put(ENTRY_ID, entryID);
        values.put(ITEM_ID, item.getID());
        values.put(ITEMS_COUNT, count);
        if(parentID != 0L)
            values.put(PARENT_ID, parentID);
        return values;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPriorityIdKey() {
        return ENTRY_ID;
    }

    @Override
    public String getPriorityIdKeyWithValue() {
        return getPriorityIdKey() + '=' + entryID;
    }

    @Override
    public String toString() {
        String color = this.getItem().getMainColor() != ShoesColor.None ? this.getItem().getMainColor().getLocalizeName() : "";
        color += this.getItem().getSecondColor() != ShoesColor.None ? "/" + this.getItem().getSecondColor().getLocalizeName() : "";
        String label = !this.getItem().getLabel().isEmpty() ? "(" + this.getItem().getLabel() + ")" : "";
        String sizes = new StringBuilder().append('[').append(getItem().getMinSize()).append('-')
                .append(getItem().getMaxSize()).append(']').append(' ').toString();
        return new StringBuilder().append(sizes).append(this.getItem().getName()).append(label).append(' ')
                .append(color).append('\t').append(this.getCount()).toString();
    }

    @Override
    public Object clone() {
        ItemEntry entry = new ItemEntry(this.item, this.count);
        entry.setEntryID(this.getEntryID());
        entry.setParentID(this.getParentID());
        return entry;
    }
}
