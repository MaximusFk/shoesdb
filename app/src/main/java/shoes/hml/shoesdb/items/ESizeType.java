package shoes.hml.shoesdb.items;

import shoes.hml.shoesdb.Main2Activity;
import shoes.hml.shoesdb.R;

/**
 * Created by maximusfk on 01.02.16.
 */
public enum ESizeType {
    Children(29, 32, Main2Activity.once.getString(R.string.size_children)),
    Junior(32, 39, Main2Activity.once.getString(R.string.size_junior)),
    Teen(35, 39, Main2Activity.once.getString(R.string.size_teen)),
    Woman(34, 42, Main2Activity.once.getString(R.string.size_woman)),
    Man_s(39, 45, Main2Activity.once.getString(R.string.size_man)),
    Man(40, 45, Main2Activity.once.getString(R.string.size_man)),
    Large(46, 52, Main2Activity.once.getString(R.string.size_large)),
    Giant(52, 62, Main2Activity.once.getString(R.string.size_giant));

    private int minSize;
    private int maxSize;
    String localeName;

    ESizeType(int min, int max){
        minSize = min;
        maxSize = max;
        localeName = this.name();
    }

    ESizeType(int minSize, int maxSize, String localeName) {
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.localeName = localeName;
    }

    public int getMinSize(){
        return minSize;
    }

    public int getMaxSize(){
        return maxSize;
    }

    public String getLocaleName() {
        return localeName;
    }

    public String getLocaleNameWithSize() {
        return localeName + '(' + minSize + '-' + maxSize + ')';
    }
}
