package shoes.hml.shoesdb.items;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import shoes.hml.shoesdb.database.ContentValuesSerializable;

/**
 * Created by maximusfk on 01.02.16.
 */
public class ShoesItem implements Serializable, Comparable, ContentValuesSerializable {

    public static final String ACCESS = "shoesItem";

    public static final String TABLE_NAME = "shoesmodel";
    public static final String ITEM_NAME = "itemname";
    public static final String ITEM_LABEL = "itemlabel";
    public static final String ITEM_ID = "_id";
    public static final String ITEM_MIN_SIZE = "minsize";
    public static final String ITEM_MAX_SIZE = "maxsize";
    public static final String ITEM_PRICE_RETAIL = "retail";
    public static final String ITEM_PRICE_WHOLE = "whole";
    public static final String ITEM_PRICE_COST = "cost";
    public static final String ITEM_COLOR_MAIN = "maincolor";
    public static final String ITEM_COLOR_SECOND = "secondcolor";
    public static final String ITEM_MANUFACTURER = "manufacturer";
    public static final String ITEM_LABELS_IDS = "labelids";
    public static final String ITEM_CATEGORY = "category";


    String name;
    long ID = (new java.util.Random().nextLong());
    int minSize;
    int maxSize;
    float priceRetail;
    float priceWhole;
    float cost;
    ShoesColor color1;
    ShoesColor color2;
    String label;
    String manufacturer;
    transient Bitmap image;
    ArrayList<Integer> labelIds;

    ShoesCategory mainCategory = ShoesCategory.Default;

    public ShoesItem(String name, ESizeType size, float retail, float whole){
        this.name = name;
        this.minSize = size.getMinSize();
        this.maxSize = size.getMaxSize();
        this.priceRetail = retail;
        this.priceWhole = whole;
        this.cost = -1;
        this.color1 = ShoesColor.None;
        this.color2 = ShoesColor.None;
    }

    public ShoesItem(String name, ESizeType size, float retail, float whole, ShoesColor color1) {
        this(name, size, retail, whole);
        this.color1 = color1;
    }

    public ShoesItem(String name, ESizeType size, float retail, float whole, ShoesColor color1, ShoesColor color2) {
        this(name, size, retail, whole, color1);
        this.color2 = color2;
    }

    public ShoesItem(String name, int minSize, int maxSize, float priceRetail, float priceWhole, ShoesColor color1, ShoesColor color2, float cost) {
        this.name = name;
        this.minSize = minSize;
        this.maxSize = maxSize;
        this.priceRetail = priceRetail;
        this.priceWhole = priceWhole;
        this.color1 = color1;
        this.color2 = color2;
        this.cost = cost;
    }

    public ShoesCategory getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(ShoesCategory mainCategory) {
        this.mainCategory = mainCategory;
    }

    public String getName(){
        return name;
    }

    public int getMinSize(){
        return minSize;
    }

    public int getMaxSize(){
        return maxSize;
    }

    public float getPriceRetail(){
        return priceRetail;
    }

    public float getPriceWhole(){
        return priceWhole;
    }

    public float getCost(){
        return cost;
    }

    public  long getID() {
        return ID;
    }

    public ShoesColor getMainColor() {
        return color1;
    }

    public ShoesColor getSecondColor() {
        return color2;
    }

    public String getLabel() {
        return label;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public Bitmap getImage(Context parent) {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public void setID(long _id) {
        this.ID = _id;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setMinSize(int min){
        this.minSize = min;
    }

    public void setMaxSize(int max){
        this.maxSize = max;
    }

    public void setPriceRetail(float price){
        this.priceRetail = price;
    }

    public void setPriceWhole(float price){
        this.priceWhole = price;
    }

    public void setCost(float cost){
        this.cost = cost;
    }

    public void setMainColor(ShoesColor color) {
        this.color1 = color;
    }

    public void setSecondColor(ShoesColor color) {
        this.color2 = color;
    }

    @Override
    public int compareTo(Object another) {
        if(another instanceof ShoesItem) {
            ShoesItem item = (ShoesItem) another;
            int eq = this.name.compareTo(item.name);
            if(eq == 0)
                eq = this.maxSize != item.maxSize ? (this.maxSize < item.maxSize ? -1 : 1) : 0;
            if(eq == 0)
                eq = this.minSize != item.minSize ? (this.minSize < item.minSize ? -1 : 1) : 0;
            if(eq == 0 && this.color1 != ShoesColor.None && item.color1 != ShoesColor.None)
                eq = this.color1.getColorId() != item.color1.getColorId() ? (this.color1.getColorId() < item.color1.getColorId() ? -1 : 1) : 0;
            if(eq == 0 && this.color2 != ShoesColor.None && item.color2 != ShoesColor.None)
                eq = this.color2.getColorId() != item.color2.getColorId() ? (this.color2.getColorId() < item.color2.getColorId() ? -1 : 1) : 0;

            return eq;
        }
        return 0;
    }

    public File getImageFile(Context context) {
        if(context == null)
            return null;
        final File external = context.getExternalFilesDir("images");
        if(!external.exists())
            external.mkdirs();
        return new File(external, "" + this.getID() + ".jpg");
    }

    public Bitmap loadImage(Context context) {
        try {
            FileInputStream stream = new FileInputStream(getImageFile(context));
            return image = BitmapFactory.decodeStream(stream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

//    public byte[] _getID() {
//        final int NUMBER_OF_BYTES = 8;
//        final long MASK = 0x00000000000000FF;
//        byte[] bytes = new byte[NUMBER_OF_BYTES];
//        long number = ID;
//        for (int i = 1; i <= NUMBER_OF_BYTES; i++) {
//            byte lastByte = (byte) (number & MASK);
//            bytes[NUMBER_OF_BYTES - i] = (lastByte);
//            number >>= NUMBER_OF_BYTES;
//        }
//        return bytes;
//    }

    public byte[] getIDBytes() {
        ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE);
        buffer.putLong(ID);
        return buffer.array();
    }

    public void setIDFromBytes(byte[] id) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE);
        buffer.put(id);
        buffer.flip();
        ID = buffer.getLong();
    }

    @Override
    public boolean equals(Object ob) {
        if(ob instanceof ShoesItem) {
            ShoesItem item = (ShoesItem) ob;
            return name.equals(item.name) &&
                    color1 == item.color1 &&
                    color2 == item.color2 &&
                    priceRetail == item.priceRetail &&
                    priceWhole == item.priceWhole &&
                    mainCategory.getID() == item.mainCategory.getID() &&
                    label.equals(item.getLabel()) || ID == item.ID;
        }
        if(ob instanceof Long) {
            Long number = (Long) ob;
            return number == this.getID();
        }
        else
            return false;
    }

    @Override
    public ContentValues getContent() {
        ContentValues values = new ContentValues();
        values.put(ITEM_NAME, name);
        values.put(ITEM_ID, ID);
        values.put(ITEM_LABEL, label);
        values.put(ITEM_MIN_SIZE, minSize);
        values.put(ITEM_MAX_SIZE, maxSize);
        values.put(ITEM_PRICE_WHOLE, priceWhole);
        values.put(ITEM_PRICE_RETAIL, priceRetail);
        values.put(ITEM_PRICE_COST, cost);
        values.put(ITEM_COLOR_MAIN, color1.getColorId());
        values.put(ITEM_COLOR_SECOND, color2.getColorId());
        values.put(ITEM_MANUFACTURER, manufacturer);
        values.put(ITEM_CATEGORY, mainCategory.getID());
        return values;
    }

    public ContentValues getContentWithNoID() {
        ContentValues values = new ContentValues();
        values.put(ITEM_NAME, name);
        values.put(ITEM_LABEL, label);
        values.put(ITEM_MIN_SIZE, minSize);
        values.put(ITEM_MAX_SIZE, maxSize);
        values.put(ITEM_PRICE_WHOLE, priceWhole);
        values.put(ITEM_PRICE_RETAIL, priceRetail);
        values.put(ITEM_PRICE_COST, cost);
        values.put(ITEM_COLOR_MAIN, color1.getColorId());
        values.put(ITEM_COLOR_SECOND, color2.getColorId());
        values.put(ITEM_MANUFACTURER, manufacturer);
        values.put(ITEM_CATEGORY, mainCategory.getID());
        return values;
    }

    public void insertDataFrom(ShoesItem another) {
        this.color1 = another.color1;
        this.color2 = another.color2;
        this.maxSize = another.maxSize;
        this.minSize = another.minSize;
        this.name = another.name;
        this.label = another.label;
        this.priceWhole = another.priceWhole;
        this.priceRetail = another.priceRetail;
        this.manufacturer = another.manufacturer;
        this.cost = another.cost;
        this.mainCategory = another.mainCategory;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPriorityIdKey() {
        return ITEM_ID;
    }

    @Override
    public String getPriorityIdKeyWithValue() {
        return getPriorityIdKey() + '=' + ID;
    }
}
