package shoes.hml.shoesdb.items;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by maximusfk on 02.02.16.
 */
public class Label {
    private static HashMap<Integer, String> labels = new HashMap<>();

    public static String getLabelByID(int id) { return labels.get(id); }

    public static boolean addLabel(String label){
        if(labels.containsValue(label))
            return false;
        Random rand = new Random();
        Integer id;
        while(labels.containsKey(id = rand.nextInt()));
        labels.put(id, label);
        return true;
    }

    public static boolean containsLabel(String label){
        return labels.containsValue(label);
    }

    public static int getIdByLabel(String label){
        for(Map.Entry<Integer, String> ent : labels.entrySet())
            if(ent.getValue().equals(label))
                return ent.getKey();
        return -1;
    }

}
