package shoes.hml.shoesdb.items;

import android.content.ContentValues;

import java.io.Serializable;

import shoes.hml.shoesdb.database.ContentValuesSerializable;

/**
 * Created by maximusfk on 27.09.16.
 */

public final class ShoesCategory implements Serializable, ContentValuesSerializable, Comparable<ShoesCategory> {

    ///TODO: Сделать расшифровку кода категории из long в массив обьектов

    public static final String ACCESS = "SHOES_CATEGORY";

    public static final String TABLE_NAME = "shoes_category";

    public static final String LABEL = "label";
    public static final String CID = "id";
    public static final String VISIBLE = "visible";

    public static final ShoesCategory Default = new ShoesCategory("default", (short) 0);

    private String label;
    private short ID;
    private boolean visible;

    public ShoesCategory(String label) {
        this.label = label;
        this.visible = true;
    }

    public ShoesCategory(String label, short ID) {
        this.label = label;
        this.ID = ID;
        this.visible = true;
    }

    public ShoesCategory(String label, boolean visible) {
        this.label = label;
        this.visible = visible;
    }

    public ShoesCategory(String label, boolean visible, short ID) {
        this.label = label;
        this.ID = ID;
        this.visible = visible;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public short getID() {
        return ID;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public ContentValues getContent() {
        ContentValues values = new ContentValues();
        values.put(LABEL, label);
        values.put(CID, ID);
        values.put(VISIBLE, visible ? 1 : 0);
        return values;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPriorityIdKey() {
        return CID;
    }

    @Override
    public String getPriorityIdKeyWithValue() {
        return getPriorityIdKey() + '=' + getID();
    }

    @Override
    public String toString() {
        return label;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof ShoesCategory) {
            ShoesCategory category = (ShoesCategory) o;
            return category.getLabel().equals(this.getLabel()) ||
                    category.getID() == this.getID();
        } else if(o instanceof Number) {
            Number number = (Number) o;
            return number.shortValue() == this.getID();
        } else if(o instanceof String) {
            String s = (String) o;
            return s.equals(this.getLabel());
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(ShoesCategory o) {
        return getID() == o.getID() ? 0 :
                getID() < o.getID() ? -1 : 1;
    }
}
