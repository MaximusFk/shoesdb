package shoes.hml.shoesdb.items;

import java.util.ArrayList;
import java.util.HashMap;

import shoes.hml.shoesdb.handlers.ShoesItemsList;

/**
 * Created by maximusfk on 28.03.16.
 */
public class ShoesItemGroup {
    private HashMap<String, ArrayList<ShoesItem>> itemHashMap;

    public ShoesItemGroup(ShoesItemsList list) {
        itemHashMap = new HashMap<>();
        for(ShoesItem item : list.getItems()) {
            if(itemHashMap.containsKey(item.getName())) {
                itemHashMap.get(item.getName()).add(item);
            } else {
                ArrayList<ShoesItem> newA = new ArrayList<>();
                newA.add(item);
                itemHashMap.put(item.getName(), newA);
            }
        }
    }
}
