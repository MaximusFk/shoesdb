package shoes.hml.shoesdb.items;

import android.graphics.Color;
import android.support.annotation.StringRes;

import shoes.hml.shoesdb.AppSettings;
import shoes.hml.shoesdb.R;

/**
 * Created by maximusfk on 04.02.16.
 */
public enum ShoesColor {
    Black(0, R.string.color_black, R.string.color_black_short, Color.BLACK),
    White(1, R.string.color_white, R.string.color_white_short, Color.WHITE),
    Red(2, R.string.color_red, R.string.color_red_short, Color.RED),
    Blue(3, R.string.color_blue, R.string.color_blue_short, Color.BLUE),
    LightBlue(4, R.string.color_lightblue, R.string.color_lightblue_short, Color.rgb(66, 170, 255)),
    Green(5, R.string.color_green, R.string.color_green_short, Color.GREEN),
    Orange(6, R.string.color_orange, R.string.color_orange_short, Color.rgb(255, 165, 0)),
    Brown(7, R.string.color_brown, R.string.color_brown_short, Color.rgb(150, 75, 0)),
    Grey(8, R.string.color_grey, R.string.color_grey_short, Color.GRAY),
    Yellow(9, R.string.color_yellow, R.string.color_yellow_short, Color.YELLOW),
    Tobacco(10, R.string.color_tobacco, R.string.color_tobacco_short, Color.rgb(123, 63, 0)),
    Vinous(11, R.string.color_vinous, R.string.color_vinous_short, Color.rgb(155, 45, 48)),
    Ginger(12, R.string.color_ginger, R.string.color_ginger_short, Color.rgb(215, 125, 49)),
    Lime(13, R.string.color_lime, R.string.color_lime_short, Color.rgb(153, 255, 153)),
    Olive(14, R.string.color_olive, R.string.color_olive_short, Color.rgb(128, 128, 0)),
    None(-1, R.string.color_none);

    private int colorId;
    @StringRes private int stringId;
    @StringRes private int shortStringId;
    private final int drawColor;

    ShoesColor(int colorId, @StringRes int resId) {
        this.colorId = colorId;
        stringId = resId;
        drawColor = Color.TRANSPARENT;
    }

    ShoesColor(int colorId, @StringRes int resId, int drawColor) {
        this.colorId = colorId;
        this.stringId = resId;
        this.drawColor = drawColor;
    }

    ShoesColor(int colorId, @StringRes int resId, @StringRes int shId, int drawColor) {
        this(colorId, resId, drawColor);
        shortStringId = shId;
    }

    public @StringRes int getStringId() {
        return stringId;
    }

    public @StringRes int getShortStringId() {
        return shortStringId;
    }

    public String getLocalizeName() {
        return AppSettings.resources.getString(stringId);
    }

    public int getColorId() {
        return colorId;
    }

    public int getColorCode() {
        return drawColor;
    }

    public static ShoesColor getColor(int colorId) {
        switch (colorId) {
            case 0: return Black;
            case 1: return White;
            case 2: return Red;
            case 3: return Blue;
            case 4: return LightBlue;
            case 5: return Green;
            case 6: return Orange;
            case 7: return Brown;
            case 8: return Grey;
            case 9: return Yellow;
            case 10: return Tobacco;
            case 11: return Vinous;
            case 12: return Ginger;
            case 13: return Lime;
            case 14: return Olive;
            default: return None;
        }
    }
}
