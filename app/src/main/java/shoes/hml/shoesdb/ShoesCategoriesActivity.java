package shoes.hml.shoesdb;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import shoes.hml.shoesdb.handlers.CategoryList;
import shoes.hml.shoesdb.handlers.MainHandler;
import shoes.hml.shoesdb.items.ShoesCategory;
import shoes.hml.shoesdb.utils.ActivityArgs1Param;

public class ShoesCategoriesActivity extends AppCompatActivity implements View.OnClickListener, EasyTextFieldLayoutHandler.OnAcceptClickListener {

    CategoryListAdapter categoryListAdapter;
    MainHandler mainHandler;

    ListView list;
    ImageButton add;

    EasyTextFieldLayoutHandler textLayout;

    public static final ActivityArgs1Param<MainHandler> handler = new ActivityArgs1Param<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoes_categories);
        list = (ListView) findViewById(R.id.shoes_category_list);
        list.setChoiceMode(AbsListView.CHOICE_MODE_NONE);
        add = (ImageButton) findViewById(R.id.shoes_add_category_button);
        add.setOnClickListener(this);
        if(handler.getArg() != null) {
            categoryListAdapter = new CategoryListAdapter(this, handler.getArg().getCategoryList());
            mainHandler = handler.getArg();
        }
        list.setAdapter(categoryListAdapter);
        textLayout = new EasyTextFieldLayoutHandler(this);
        textLayout.setOnAcceptClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.shoes_add_category_button:
                textLayout.show(findViewById(R.id.action_bar));
                break;
        }
    }

    @Override
    public void OnClick(String text) {
        mainHandler.getCategoryList().addCategory(text);
        categoryListAdapter.notifyDataSetChanged();
    }

    public static class CategoryListAdapter extends BaseAdapter implements SpinnerAdapter {

        CategoryList categoryList;
        Context parent;
        ArrayList<ShoesCategory> categories;

        ViewType viewType = ViewType.CHECKABLE;

        public CategoryListAdapter(Context parent, CategoryList categoryList) {
            this.categoryList = categoryList;
            this.parent = parent;
            this.categories = this.categoryList.getArray();
        }

        public CategoryListAdapter(Context parent, CategoryList categoryList, ViewType type) {
            this(parent, categoryList);
            this.viewType = type;
        }

        @Override
        public void notifyDataSetChanged() {
            categories = categoryList.getArray();
            super.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return this.categories.size();
        }

        int getPositionByID(long ID) {
            for(int i = 0; i < categories.size(); ++i)
                if(categories.get(i).getID() == ID)
                    return i;
            return 0;
        }

        @Override
        public Object getItem(int i) {
            return this.categories.get(i);
        }

        @Override
        public long getItemId(int i) {
            return this.categories.get(i).getID();
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            final CheckedTextView textView = new CheckedTextView(parent);
            final ShoesCategory category = (ShoesCategory) getItem(i);
            textView.setText(category.getLabel());
            textView.setChecked(category.isVisible());
            textView.setMinHeight(50);
            textView.setCheckMarkDrawable(R.drawable.ic_accept_button);
            return textView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            switch (viewType) {
                case CHECKABLE:
                    return getView(position, convertView, parent);
                case LIST:
                    TextView textView = new TextView(this.parent);
                    ShoesCategory category = (ShoesCategory) getItem(position);
                    textView.setText(category.getLabel());
                    textView.setMinHeight(35);
                    return textView;
                default: return null;
            }
        }

        @Override
        public boolean isEmpty() {
            return categories.isEmpty();
        }

        public enum ViewType {
            CHECKABLE,
            LIST
        }
    }
}
