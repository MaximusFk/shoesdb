package shoes.hml.shoesdb;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;

import shoes.hml.shoesdb.actions.ReceiptMove;

public class MakeProviderEntry extends ActionBarActivity {

    Button accept;
    Button cancel;
    ReceiptMove.ProviderEntry providerEntry;
    long receiptMoveID;

    public final static int RETURN_TYPE_EDIT = 153;
    public final static int RETURN_TYPE_CREATE = 841;
    private PopupMenu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_provider_entry);
        final Intent intent = this.getIntent();
        providerEntry = (ReceiptMove.ProviderEntry) intent.getSerializableExtra(ReceiptMove.ProviderEntry.ACCESS);
        receiptMoveID = intent.getLongExtra("MoveID", -1);
        if(providerEntry != null) {
            ((EditText) findViewById(R.id.editText)).setText(providerEntry.getName());
            ((EditText) findViewById(R.id.editText2)).setText(new StringBuilder().append(providerEntry.getCount()));
        }
        accept = new Button(this);
        accept.setText(getString(R.string.answ_ok));
        accept.setBackgroundColor(Color.TRANSPARENT);
        accept.setTextColor(Color.WHITE);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int returnType;
                if (providerEntry == null) {
                    returnType = RETURN_TYPE_CREATE;
                    providerEntry = new ReceiptMove.ProviderEntry(
                            ((EditText) findViewById(R.id.editText)).getText().toString(),
                            AddShoesItem.getLong(((EditText) findViewById(R.id.editText2)).getText().toString()),
                            receiptMoveID
                    );
                } else {
                    returnType = RETURN_TYPE_EDIT;
                    providerEntry.setName(((EditText) findViewById(R.id.editText)).getText().toString());
                    providerEntry.setCount(AddShoesItem.getLong(((EditText) findViewById(R.id.editText2)).getText().toString()));
                }
                Intent result = new Intent();
                result.putExtra(ReceiptMove.ProviderEntry.ACCESS, providerEntry);
                setResult(returnType, result);
                finish();
            }
        });

        cancel = new Button(this);
        cancel.setText(getString(R.string.answ_cancel));
        cancel.setBackgroundColor(Color.TRANSPARENT);
        cancel.setTextColor(Color.WHITE);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Toolbar bar = (Toolbar) findViewById(R.id.action_bar);
        bar.addView(accept);
        bar.addView(cancel);
        (findViewById(R.id.editText2)).setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER)
                    accept.callOnClick();
                return false;
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        final EditText provider = ((EditText) findViewById(R.id.editText));
        if(ReceiptMove.hasAvaibleProviders() && this.getCurrentFocus().getId() == provider.getId()
                && hasFocus && menu == null) {
            menu = new PopupMenu(this, provider);
            for(String name : ReceiptMove.getAvaibleProviders())
                menu.getMenu().add(name);
            menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    provider.setText(item.getTitle());
                    (findViewById(R.id.editText2)).requestFocus();
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                    return true;
                }
            });
            menu.show();
        }
    }
}
