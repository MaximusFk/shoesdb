package shoes.hml.shoesdb;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

/**
 * Created by maximusfk on 13.10.16.
 */

public class EasyTextFieldLayoutHandler implements View.OnClickListener, View.OnFocusChangeListener {

    Context parent;
    PopupWindow popupWindow;
    LinearLayout layout;
    EditText text;
    OnAcceptClickListener listener;
    private final String template;

    public EasyTextFieldLayoutHandler(@NonNull Context parent) {
        popupWindow = new PopupWindow(parent);
        layout = (LinearLayout) LayoutInflater.from(parent).inflate(R.layout.easy_text_layout, null);
        popupWindow.setContentView(layout);
        layout.findViewById(R.id.easy_accept).setOnClickListener(this);
        layout.findViewById(R.id.easy_cancel).setOnClickListener(this);
        text = (EditText) layout.findViewById(R.id.easy_text_text);
        text.setOnFocusChangeListener(this);
        template = (text).getText().toString();
    }

    public void show(View anchor) {
        if(popupWindow.isShowing())
            popupWindow.dismiss();
        popupWindow.setWindowLayoutMode(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setHeight(layout.getHeight());
        popupWindow.setWidth(layout.getWidth());
        popupWindow.showAsDropDown(anchor);
        popupWindow.setFocusable(true);
        popupWindow.setClippingEnabled(true);
        popupWindow.update();
    }

    public void setOnAcceptClickListener(OnAcceptClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.easy_accept:
                if(listener != null)
                    listener.OnClick(text.getText().toString());
            case R.id.easy_cancel:
                text.setText(template);
                popupWindow.dismiss();
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if(view.getId() == R.id.easy_text_text) {
            EditText editText = (EditText) view;
            if(b && template.equals(editText.getText().toString())) {
                editText.setText("");
            } else if(editText.getText().toString().isEmpty()) {
                editText.setText(template);
            }
        }
    }

    public interface OnAcceptClickListener {
        void OnClick(String text);
    }
}
