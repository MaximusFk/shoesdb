package shoes.hml.shoesdb;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import shoes.hml.shoesdb.actions.SaleMove;
import shoes.hml.shoesdb.adapters.ReceiptItemEntryAdapter;
import shoes.hml.shoesdb.adapters.ShoesItemListAdapter;
import shoes.hml.shoesdb.handlers.MainHandler;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.utils.ActivityArgs1Param;
import shoes.hml.shoesdb.utils.ActivityArgs2Param;
import shoes.hml.shoesdb.views.ItemEntry_w_add;

import static shoes.hml.shoesdb.AddShoesItem.getInt;

public class SaleActivity extends AppCompatActivity implements ItemEntry_w_add.OnAddButtonClick, AdapterView.OnItemClickListener, ItemEntryLayoutHandler.OnItemChanged, View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener {

    /* Static params */
    public static final ActivityArgs2Param<MainHandler, SaleMove> args = new ActivityArgs2Param<>();
    public static final ActivityArgs1Param<SaleMove> result = new ActivityArgs1Param<>();

    /* Main data */
    private ShoesItemListAdapter shoesItemListAdapter;
    private SaleMove saleMove;
    private MainHandler mainHandler;
    private SaleTypeSpinnerAdapter saleTypeSpinnerAdapter;
    private ReceiptItemEntryAdapter itemEntryAdapter;

    Date current = GregorianCalendar.getInstance().getTime();
    Calendar cdate;

    private boolean isEdited = false;

    /* UI */
    TextView sum;
    TextView odd;
    TextView count;
    TextView date;
    Spinner saleType;
    Button add;
    ListView sitems;
    EditText cash;

    /* Dynamic UI */
    Button accept;
    Button cancel;
    ImageButton menu;
    ItemEntryLayoutHandler entryLayoutHandler;
    PopupWindow popupWindow;
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale);
        isEdited = false;
        /* Инициализация UI */
        sum = (TextView) findViewById(R.id.sum);
        odd = (TextView) findViewById(R.id.sale_odd);
        count = (TextView) findViewById(R.id.sale_all_count);
        date = (TextView) findViewById(R.id.sale_date);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });
        saleType = (Spinner) findViewById(R.id.sale_type);
        add = (Button) findViewById(R.id.sale_add_item_button);
        sitems = (ListView) findViewById(R.id.sale_items);
        cash = (EditText) findViewById(R.id.sale_cash);
        cash.setOnFocusChangeListener(this);

        cdate = new GregorianCalendar();

        initDynamicUI();

        saleTypeSpinnerAdapter = new SaleTypeSpinnerAdapter(this);
        mainHandler = args.getArg1();
        if(mainHandler == null)
            finish();

        saleMove = args.getArg2() != null ? (SaleMove) args.getArg2().clone() : new SaleMove(
                mainHandler.getMainStorage(),
                current,
                current,
                SaleMove.SaleType.WHOLE
        );

        shoesItemListAdapter = new ShoesItemListAdapter(this, mainHandler.getItemsList());
        entryLayoutHandler = new ItemEntryLayoutHandler(this, popupWindow);
        entryLayoutHandler.setAdapter(shoesItemListAdapter);
        entryLayoutHandler.setOnItemChanged(this);
        itemEntryAdapter = new ReceiptItemEntryAdapter(this, saleMove);
        itemEntryAdapter.setAddButtonClick(this);

        updateAdapter();
        sitems.setOnItemClickListener(this);

        saleType.setAdapter(saleTypeSpinnerAdapter);
        saleType.setOnItemSelectedListener(this);

        initUIBySaleObject(saleMove);
    }

    @Override
    public void finish() {
        args.clear();
        super.finish();
    }

    @Override
    protected void onStop() {
        args.setArg2(saleMove);
        super.onStop();
    }

    private void initDynamicUI() {
        accept = new Button(this);
        cancel = new Button(this);
        popupWindow = new PopupWindow(this);
        accept.setText(R.string.answ_ok);
        accept.setBackgroundColor(Color.TRANSPARENT);
        accept.setTextColor(Color.WHITE);
        cancel.setText(R.string.answ_cancel);
        cancel.setBackgroundColor(Color.TRANSPARENT);
        cancel.setTextColor(Color.WHITE);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accept();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.action_bar);
        if(toolbar != null){
            toolbar.addView(accept);
            toolbar.addView(cancel);
        }
        datePickerDialog = new DatePickerDialog(
                this,
                this,
                cdate.get(Calendar.YEAR),
                cdate.get(Calendar.MONTH),
                cdate.get(Calendar.DAY_OF_MONTH)
        );
    }

    private void initUIBySaleObject(SaleMove move) {
        if(move != null) {
            SaleMove.SaleType type = move.getSaleType();
            saleType.setSelection(type.ordinal());
            cdate.setTime(move.getCustomDate());
            initDate();
            sum.setText(String.valueOf(move.getCreditPrice()));
            cash.setText(String.valueOf((int)move.getDebit()));
            odd.setText(String.valueOf(move.getDebit() - move.getCreditPrice()));
            count.setText(String.valueOf(move.getCount()));
        }
    }

    private void showItemEntryChooser(ItemEntry entry, boolean edit) {
        View view = findViewById(R.id.action_bar);
        if(edit && entry != null)
            entryLayoutHandler.show(view, entry);
        else if(entry != null)
            entryLayoutHandler.showWithNoEdit(view, entry);
        else
            entryLayoutHandler.show(view);
        itemEntryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAddClick(ItemEntry entry, int listPosition, ClickType clickType) {
        switch (clickType) {
            case ADD:
                showItemEntryChooser(entry, false);
                break;
            case SET:
                showItemEntryChooser(entry, true);
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (itemEntryAdapter.isAddButton(i))
            showItemEntryChooser(null, false);
        else
            showItemEntryChooser(itemEntryAdapter.getItemEntry(i), true);
    }

    @Override
    public void onChanged(ItemEntry entry, boolean edited) {
        if(!edited)
            saleMove.addItem(entry);
        isEdited = true;
        updateAdapter();
        computeCounts();
    }

    @Override
    public void onBackPressed() {
        if(popupWindow.isShowing()) {
            popupWindow.dismiss();
        }
        else if(isEdited) {
            accept();
        } else {
            setResult(RESULT_CANCELED);
            super.onBackPressed();
        }
    }

    private void updateAdapter() {
        sitems.setAdapter(itemEntryAdapter);
    }

    private void computeCounts() {
        long itemCount = saleMove.getCount();
        float allPrice = saleMove.getCreditPrice();
        long currentCash = getInt(cash.getText().toString());
        float currentOdd = currentCash - allPrice;
        odd.setText(String.valueOf(currentOdd));
        sum.setText(String.valueOf(allPrice));
        count.setText(String.valueOf(itemCount));
    }

    void initDate() {
        if(cdate != null) {
            date.setText(new StringBuilder().append(cdate.get(Calendar.DAY_OF_MONTH)).append('/')
                    .append(cdate.get(Calendar.MONTH) + 1).append('/')
                    .append(cdate.get(Calendar.YEAR)).toString());
        }
    }

    void accept() {
        setResult(RESULT_OK);
        result.setArg(saleMove);
        finish();
    }

    void cancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if(view.getId() == cash.getId()) {
            saleMove.setDebit(getInt(cash.getText().toString()));
            computeCounts();
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        cdate.set(i, i1, i2);
        saleMove.setCustomDate(cdate.getTime());
        initDate();
        isEdited = true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        SaleTypeSpinnerAdapter adapter = (SaleTypeSpinnerAdapter) adapterView.getAdapter();
        saleMove.setSaleType((SaleMove.SaleType) adapter.getItem(i));
        computeCounts();
        isEdited = true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public static class SaleTypeSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {

        Context parent;

        public SaleTypeSpinnerAdapter(Context parent) {
            this.parent = parent;
        }

        @Override
        public int getCount() {
            return SaleMove.SaleType.values().length;
        }

        @Override
        public Object getItem(int i) {
            return SaleMove.SaleType.values()[i];
        }

        @Override
        public long getItemId(int i) {
            return SaleMove.SaleType.values()[i].getTypeId();
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            TextView textView = new TextView(parent);
            SaleMove.SaleType type = (SaleMove.SaleType) getItem(i);
            textView.setText(type.getStringId());
            return textView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }
    }
}
