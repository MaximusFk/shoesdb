package shoes.hml.shoesdb;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.StringRes;

import java.io.File;

/**
 * Created by maximusfk on 08.04.16.
 */
public class AppSettings {

    public static final String CURRENTDB = "_current_db";
    public static final String IMAGEQUAL = "_image_quality";
    public static Resources resources;

    private String foundedDataBases[];
    private File settingFile;
    private String currentDataBase;
    private int imageQuality;
    private int displayHeight;
    private int displayWidth;

    private Context mainContext;

    public AppSettings(Context context) {
        resources = context.getResources();
        mainContext = context;
        foundedDataBases = mainContext.databaseList();
        displayHeight = mainContext.getResources().getDisplayMetrics().heightPixels;
        displayWidth = mainContext.getResources().getDisplayMetrics().widthPixels;
    }

    public static String getString(@StringRes int stringResId) {
        return resources.getString(stringResId);
    }

    public String [] getAllowedDataBases() {
        return foundedDataBases;
    }

    public String getCurrentDatabse() {
        return currentDataBase;
    }

    public int getImageQuality() {
        return imageQuality;
    }

    public int getDisplayHeight() {
        return displayHeight;
    }

    public int getDisplayWidth() {
        return displayWidth;
    }
}
