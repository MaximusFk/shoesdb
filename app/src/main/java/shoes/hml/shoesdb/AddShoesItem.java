package shoes.hml.shoesdb;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import shoes.hml.shoesdb.handlers.MainHandler;
import shoes.hml.shoesdb.items.ESizeType;
import shoes.hml.shoesdb.items.ShoesCategory;
import shoes.hml.shoesdb.items.ShoesColor;
import shoes.hml.shoesdb.items.ShoesItem;
import shoes.hml.shoesdb.utils.ActivityArgs1Param;

public class AddShoesItem extends ActionBarActivity implements PopupMenu.OnMenuItemClickListener {

    private final static int CAMERA_TAKE = 1;
    private final static int PICTURE_TAKE = 2;
    public final static String IMAGEQUALITY = "imageQuality";

    Button accept;
    Button cancel;
    ImageButton choseDialog;
    PopupMenu sizeMenu;
    ImageButton setImage;
    Bitmap image = null;
    int imageQuality;
    Spinner category;

    public static final ActivityArgs1Param<MainHandler> handler = new ActivityArgs1Param<>();

    ShoesItem item = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_shoes_item);
        category = (Spinner) findViewById(R.id.shoes_item_category);
        Serializable object = this.getIntent().getSerializableExtra(ShoesItem.ACCESS);
        if(object instanceof ShoesItem)
            item = (ShoesItem) object;
        imageQuality = this.getIntent().getIntExtra(IMAGEQUALITY, 100);
        Spinner sp1 = (Spinner) findViewById(R.id.mainColor);
        sp1.setAdapter(new ColorSpinnerAdapter(this));
        Spinner sp2 = (Spinner) findViewById(R.id.secondColor);
        sp2.setAdapter(new ColorSpinnerAdapter(this));
        sp2.setSelection(sp2.getAdapter().getCount() - 1);
        category.setAdapter(new ShoesCategoriesActivity.CategoryListAdapter(
                this,
                handler.getArg().getCategoryList(),
                ShoesCategoriesActivity.CategoryListAdapter.ViewType.LIST
        ));
        Toolbar bar = (Toolbar) this.findViewById(R.id.action_bar);
        bar.setTitle("Add new Item");
        if(item != null)
            init(item);
        accept = new Button(this);
        accept.setText(getString(R.string.answ_ok));
        accept.setBackgroundColor(Color.TRANSPARENT);
        accept.setHighlightColor(Color.GRAY);
        accept.setTextColor(Color.WHITE);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2Activity.ResultType type;
                int minSize = getInt(((EditText) findViewById(R.id.minSize)).getText().toString());
                int maxSize = getInt(((EditText) findViewById(R.id.maxSize)).getText().toString());
                String name = ((EditText) findViewById(R.id.itemName)).getText().toString();
                String label = ((EditText) findViewById(R.id.label)).getText().toString();
                String manufacturer = ((EditText) findViewById(R.id.Manufacturer)).getText().toString();
                float retail = getFloat(((EditText) findViewById(R.id.priceRetail)).getText().toString());
                float whole = getFloat(((EditText) findViewById(R.id.priceWhole)).getText().toString());
                //float cost = getFloat(((EditText) findViewById(R.id.priceCost)).getText().toString());
                ColorSpinnerAdapter.ColorView colorView = (ColorSpinnerAdapter.ColorView) ((Spinner) findViewById(R.id.mainColor)).getSelectedView();
                ColorSpinnerAdapter.ColorView colorView2 = (ColorSpinnerAdapter.ColorView) ((Spinner) findViewById(R.id.secondColor)).getSelectedView();
                ShoesCategoriesActivity.CategoryListAdapter categoryListAdapter = (ShoesCategoriesActivity.CategoryListAdapter) category.getAdapter();
                if (colorView.color == ShoesColor.None && colorView2.color != ShoesColor.None) {
                    ColorSpinnerAdapter.ColorView buf = colorView;
                    colorView = colorView2;
                    colorView2 = buf;
                }
                if (item == null) {
                    item = new ShoesItem(name, minSize, maxSize, retail, whole, colorView.getColor(), colorView2.getColor(), 0.f);
                    type = Main2Activity.ResultType.CREATE_ITEM;
                } else {
                    item.setName(name);
                    item.setMinSize(minSize);
                    item.setMaxSize(maxSize);
                    item.setPriceRetail(retail);
                    item.setPriceWhole(whole);
                    item.setMainColor(colorView.getColor());
                    item.setSecondColor(colorView2.getColor());
                    type = Main2Activity.ResultType.EDIT_ITEM;
                }
                item.setManufacturer(manufacturer);
                item.setLabel(label);
                item.setMainCategory((ShoesCategory) categoryListAdapter.getItem(category.getSelectedItemPosition()));
                if(image != null) {
                    saveImage(image, item);
                }
                Intent result = new Intent();
                result.putExtra(ShoesItem.ACCESS, item);
                setResult(RESULT_OK, result);
                finish();
            }
        });
        setImage = (ImageButton) findViewById(R.id.takePhoto);
        setImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImageChoseDialog();
            }
        });
        cancel = new Button(this);
        cancel.setText(getString(R.string.answ_cancel));
        cancel.setBackgroundColor(Color.TRANSPARENT);
        cancel.setHighlightColor(Color.GRAY);
        cancel.setTextColor(Color.WHITE);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        choseDialog = (ImageButton) findViewById(R.id.choseSize);
        sizeMenu = new PopupMenu(this, choseDialog);
        sizeMenu.setOnMenuItemClickListener(this);
        for(ESizeType sizeType : ESizeType.values()) {
            sizeMenu.getMenu().add(0, sizeType.ordinal(), 0, sizeType.getLocaleNameWithSize());
        }
        choseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sizeMenu.show();
            }
        });
        bar.addView(accept);
        bar.addView(cancel);
//        ImageView imageView = (ImageView) findViewById(R.id.shoesPhoto);
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openImageDialog();
//            }
//        });
    }

    private void init(ShoesItem item) {
        EditText name = (EditText) findViewById(R.id.itemName),
                minSize = (EditText) findViewById(R.id.minSize),
                maxSize = (EditText) findViewById(R.id.maxSize),
                retail = (EditText) findViewById(R.id.priceRetail),
                whole = (EditText) findViewById(R.id.priceWhole),
                label = (EditText) findViewById(R.id.label),
                manuf = (EditText) findViewById(R.id.Manufacturer);
        Spinner mainColor = (Spinner) findViewById(R.id.mainColor),
                secondColor = (Spinner) findViewById(R.id.secondColor),
                category = (Spinner) findViewById(R.id.shoes_item_category);
        name.setText(item.getName());
        minSize.setText("" + item.getMinSize());
        maxSize.setText("" + item.getMaxSize());
        mainColor.setSelection(((ColorSpinnerAdapter) mainColor.getAdapter()).getPositionByItem(item.getMainColor()));
        secondColor.setSelection(((ColorSpinnerAdapter) secondColor.getAdapter()).getPositionByItem(item.getSecondColor()));
        category.setSelection(((ShoesCategoriesActivity.CategoryListAdapter)category.getAdapter()).getPositionByID(item.getMainCategory().getID()));
        retail.setText("" + item.getPriceRetail());
        whole.setText("" + item.getPriceWhole());
        label.setText(item.getLabel());
        manuf.setText(item.getManufacturer());
        if((item.getImageFile(this)).exists())
            ((ImageView) findViewById(R.id.shoesPhoto)).setImageBitmap(item.loadImage(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_OK)
            return;
        switch (requestCode) {
            case CAMERA_TAKE:
                final File temp = getTempFile();
                try {

                    image = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(temp));
                    ((ImageView)findViewById(R.id.shoesPhoto)).setImageBitmap(image);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case PICTURE_TAKE:
                try {
                    InputStream stream = getContentResolver().openInputStream(data.getData());
                    image = BitmapFactory.decodeStream(stream);
                    ((ImageView)findViewById(R.id.shoesPhoto)).setImageBitmap(image);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if(item.getItemId() < ESizeType.values().length) {
            ESizeType sizeType = ESizeType.values()[item.getItemId()];
            ((EditText)findViewById(R.id.minSize)).setText(new StringBuilder().append(sizeType.getMinSize()).toString());
            ((EditText)findViewById(R.id.maxSize)).setText(new StringBuilder().append(sizeType.getMaxSize()).toString());
            return true;
        }
        return false;
    }

    private File getTempFile() {
        final File path = this.getExternalCacheDir();
        if(!path.exists())
            path.mkdirs();
        return new File(path, "image.tmp");
    }

    private Bitmap getImage(File file) {
        if(file.exists()) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(file));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void saveImage(Bitmap bitmap, ShoesItem item) {
        File file = item.getImageFile(this);
        try {
            FileOutputStream stream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, imageQuality, stream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openCamera() {
        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getTempFile()));
        startActivityForResult(intent, CAMERA_TAKE);
    }

    private void openImage() {
        final Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, PICTURE_TAKE);
    }

    private void openImageChoseDialog() {
        PopupMenu popupMenu = new PopupMenu(this, setImage);
        popupMenu.inflate(R.menu.image_chose_menu);
        final Context context = this;
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem sitem) {
                switch (sitem.getItemId()) {
                    case R.id.shoes_take_photo:
                        openCamera();
                        break;
                    case R.id.shoes_chose_image:
                        openImage();
                        break;
                    case R.id.shoes_delete_image:
                        if(image != null) {
                            image.recycle();
                            image = null;
                        }
                        ((ImageView)findViewById(R.id.shoesPhoto)).setImageResource(R.drawable.ic_no_image);
                        if(item != null) {
                            item.getImageFile(context).delete();
                        }
                }
                return true;
            }
        });
        popupMenu.show();
    }

    public static float getFloat(String f) {
        try {
            return Float.parseFloat(f);
        } catch (Throwable t) {
            return 0.f;
        }
    }

    public static int getInt(String i) {
        try {
            return Integer.parseInt(i);
        } catch (Throwable t) {
            return 0;
        }
    }

    public static long getLong(String l) {
        try {
            return Long.parseLong(l);
        } catch (Throwable t) {
            return 0;
        }
    }

    private class ColorSpinnerAdapter implements SpinnerAdapter {

        Context parent;

        public ColorSpinnerAdapter(Context parent) {
            this.parent = parent;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView textView = new TextView(this.parent);
            textView.setText(ShoesColor.values()[position].getLocalizeName());
            textView.setMinHeight(30);
            textView.setTextColor(ShoesColor.values()[position].getColorCode());
            textView.setShadowLayer(2, 2, 2, Color.BLACK);
            return textView;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return ShoesColor.values().length;
        }

        @Override
        public Object getItem(int position) {
            return ShoesColor.values()[position];
        }

        @Override
        public long getItemId(int position) {
            return ShoesColor.values()[position].getColorId();
        }

        public int getPositionByItem(ShoesColor color) {
//            for(int pos = 0; pos < ShoesColor.values().length; ++pos)
//                if(ShoesColor.values()[pos].getColorId() == color.getColorId())
//                    return pos;
            return color.ordinal();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ColorView textView = new ColorView(this.parent, ShoesColor.values()[position]);
            textView.setTextColor(ShoesColor.values()[position].getColorCode());
            textView.setShadowLayer(2, 2, 2, Color.BLACK);
            textView.setMinimumHeight(25);
            return textView;
        }

        @Override
        public int getItemViewType(int position) {
            return 1;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        class ColorView extends TextView {
            ShoesColor color;

            public ColorView(Context context, ShoesColor color) {
                super(context);
                this.color = color;
                this.setText(this.color.getLocalizeName());
            }

            public ShoesColor getColor() {
                return color;
            }
        }
    }
}
