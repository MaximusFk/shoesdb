package shoes.hml.shoesdb;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;

import shoes.hml.shoesdb.adapters.ReceiptItemEntryAdapter;
import shoes.hml.shoesdb.handlers.MainHandler;
import shoes.hml.shoesdb.handlers.Storage;

public class StorageActivity extends ActionBarActivity implements View.OnClickListener, PopupMenu.OnMenuItemClickListener {

    Storage storage;
    MainHandler handler;

    public static final int RET_STORAGE_CREATE = Main2Activity.ResultType.CREATE_STORAGE.getId();
    public static final int RET_STORAGE_UPDATE = Main2Activity.ResultType.EDIT_STORAGE.getId();

    private PopupMenu menu;
    private PopupMenu menuContext;
    private ImageButton menuButton;

    private ReceiptItemEntryAdapter itemEntryAdapter;

    private Button accept;
    private Button cancel;

    private int current_return_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage);
        storage = (Storage) this.getIntent().getSerializableExtra(Storage.ACCESS);
        handler = (MainHandler) this.getIntent().getSerializableExtra(MainHandler.ACCESS);
        assert (handler != null) : "Error! Handler has not be null.";
        if(storage != null) {
            current_return_type = RET_STORAGE_UPDATE;
            itemEntryAdapter = new ReceiptItemEntryAdapter(this, storage.getItemEntries(), false);
            ((EditText)findViewById(R.id.storage_name)).setText(storage.getStorageName());
            ((ListView)findViewById(R.id.storage_items)).setAdapter(itemEntryAdapter);
        } else
            current_return_type = RET_STORAGE_CREATE;
        menuButton = new ImageButton(this);
        menuButton.setId(R.id.menu_button);
        accept = new Button(this);
        accept.setId(R.id.accept_button);
        cancel = new Button(this);
        cancel.setId(R.id.cancel_button);
        menuButton.setImageResource(R.drawable.ic_menu_button);
        menuButton.setBackgroundColor(Color.TRANSPARENT);
        menuButton.setOnClickListener(this);
        accept.setText(R.string.answ_ok);
        accept.setTextColor(Color.WHITE);
        accept.setBackgroundColor(Color.TRANSPARENT);
        accept.setOnClickListener(this);
        cancel.setText(R.string.answ_cancel);
        cancel.setTextColor(Color.WHITE);
        cancel.setBackgroundColor(Color.TRANSPARENT);
        cancel.setOnClickListener(this);
        menu = new PopupMenu(this, menuButton);
        menu.inflate(R.menu.storage_main_menu);
        menu.setOnMenuItemClickListener(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.action_bar);
        toolbar.addView(accept);
        toolbar.addView(cancel);
        toolbar.addView(menuButton);
    }

    @Override
    public void onClick(View v) {
        int curId = v.getId();
        if(curId == menuButton.getId()) {
            menu.show();
        } else if(curId == accept.getId()) {
            if(storage == null) {
                storage = new Storage("");
            }
            storage.setStorageName(((EditText)findViewById(R.id.storage_name)).getText().toString());
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Storage.ACCESS, storage);
            setResult(current_return_type, returnIntent);
            this.finish();
        } else if(curId == cancel.getId()) {
            this.finish();
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.storage_send:
            case R.id.storage_find:
        }
        return true;
    }
}
