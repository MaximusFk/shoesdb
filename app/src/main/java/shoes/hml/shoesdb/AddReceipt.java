package shoes.hml.shoesdb;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import shoes.hml.shoesdb.actions.ReceiptMove;
import shoes.hml.shoesdb.adapters.ReceiptItemEntryAdapter;
import shoes.hml.shoesdb.adapters.ReceiptProviderEntryAdapter;
import shoes.hml.shoesdb.adapters.ShoesItemListAdapter;
import shoes.hml.shoesdb.adapters.StoragesListAdapter;
import shoes.hml.shoesdb.handlers.MainHandler;
import shoes.hml.shoesdb.handlers.Storage;
import shoes.hml.shoesdb.handlers.StorageList;
import shoes.hml.shoesdb.items.ItemEntry;
import shoes.hml.shoesdb.utils.ActivityArgs1Param;
import shoes.hml.shoesdb.utils.ActivityArgs2Param;
import shoes.hml.shoesdb.views.ItemEntry_w_add;

public class AddReceipt extends ActionBarActivity implements DatePickerDialog.OnDateSetListener, PopupMenu.OnMenuItemClickListener {

    MainHandler mainHandler;
    ReceiptMove receiptMove;
    Button accept;
    Button cancel;
    ImageButton menu;
    Calendar current;
    DatePickerDialog datePickerDialog;
    TextView datetextView;
    TextView allCount;
    ReceiptItemEntryAdapter itemEntryAdapter;
    ReceiptProviderEntryAdapter providerEntryAdapter;
    Main2Activity.ResultType resultType;
    ShoesItemListAdapter shoesItemListAdapter;
    ContextMenu selectedMenu = null;

    ItemEntryLayoutHandler addItemEntry;

    PopupWindow popupWindow;
    private PopupMenu contextMenu;
    private PopupMenu menuAdditional;

    boolean isEdited = false;
    AlertDialog.Builder closeAlert;

    public static final ActivityArgs1Param<LayoutVariant> variant = new ActivityArgs1Param<>(LayoutVariant.WITH_PROVIDER);
    public static final ActivityArgs2Param<MainHandler, ReceiptMove> args = new ActivityArgs2Param<>();
    public static final ActivityArgs1Param<ReceiptMove> result = new ActivityArgs1Param<>();

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView itemEntryList = null;
        mainHandler = args.getArg1();
        receiptMove = args.getArg2();
        popupWindow = new PopupWindow(this);
        current = new GregorianCalendar();
        isEdited = false;
        shoesItemListAdapter = new ShoesItemListAdapter(this, mainHandler.getItemsList());
        addItemEntry = new ItemEntryLayoutHandler(this, popupWindow);
        addItemEntry.setAdapter(shoesItemListAdapter);
        if (receiptMove == null) {
            resultType = Main2Activity.ResultType.CREATE_RECEIPT;
            receiptMove = new ReceiptMove(null, new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));
        }
        else {
            resultType = Main2Activity.ResultType.EDIT_RECEIPT;
            receiptMove = (ReceiptMove) receiptMove.clone();
        }
        if(variant.getArg() == LayoutVariant.WITH_PROVIDER || !args.getArg2().getProviderEntries().isEmpty()) {
            setContentView(R.layout.activity_add_receipt);
            datetextView = (TextView) findViewById(R.id.receiptDate);
            itemEntryList = (ListView) findViewById(R.id.listView2);
            findViewById(R.id.receipt_add_entry).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addItemEntry.show(findViewById(R.id.action_bar));
                }
            });
            allCount = (TextView) findViewById(R.id.allCount);
            ListView providerEntryList = (ListView) findViewById(R.id.listView);
            providerEntryList.setAdapter(providerEntryAdapter = new ReceiptProviderEntryAdapter(this, receiptMove));
            providerEntryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (providerEntryAdapter.isAddButton(position)) {
                        startMakeProviderEntryActivity();
                    } else {
                        startEditProviderEntryActivity((ReceiptMove.ProviderEntry) providerEntryAdapter.getItem(position));
                    }
                }
            });
            providerEntryList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    if (parent.getAdapter() instanceof ReceiptProviderEntryAdapter
                            && !((ReceiptProviderEntryAdapter) parent.getAdapter()).isAddButton(position)) {
                        selectedMenu = new ContextMenu();
                        selectedMenu.type = Type.DELETE_PROVIDER;
                        selectedMenu.position = position;
                        onOpenListViewContextMenu(view, position, 2);
                    }
                    return true;
                }
            });
        }
        else if(variant.getArg() == LayoutVariant.WITH_STORAGE) {
            setContentView(R.layout.activity_add_receipt_to_storage);
            datetextView = (TextView) findViewById(R.id.receipt_to_s_date);
            itemEntryList = (ListView) findViewById(R.id.items_to_s);
            findViewById(R.id.receipt_add_entry).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addItemEntry.show(findViewById(R.id.action_bar));
                }
            });
            allCount = (TextView) findViewById(R.id.allCount_s);
            Spinner storages = (Spinner) findViewById(R.id.receipt_storage);
            if(storages != null) {
                storages.setAdapter(new StorageSpinnerAdapter(mainHandler.getStorageList(), this));
                storages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        StorageSpinnerAdapter adapter = (StorageSpinnerAdapter) adapterView.getAdapter();
                        Storage storage = (Storage) adapter.getItem(i);
                        receiptMove.setTo(storage);
                        isEdited = true;
                    }
                });
            }
        }
        else {
            setResult(RESULT_CANCELED);
            finish();
        }
        initFields(receiptMove);
        Toolbar toolbar = (Toolbar) findViewById(R.id.action_bar);
        accept = new Button(this);
        accept.setText(getString(R.string.answ_ok));
        accept.setBackgroundColor(Color.TRANSPARENT);
        accept.setTextColor(Color.WHITE);
        cancel = new Button(this);
        cancel.setText(getString(R.string.answ_cancel));
        cancel.setBackgroundColor(Color.TRANSPARENT);
        cancel.setTextColor(Color.WHITE);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        menu = new ImageButton(this);
        menu.setImageResource(R.drawable.ic_menu_button);
        menu.setBackgroundColor(Color.TRANSPARENT);
        datePickerDialog = new DatePickerDialog(this, this, current.get(Calendar.YEAR), current.get(Calendar.MONTH), current.get(Calendar.DAY_OF_MONTH));
        datetextView.setClickable(true);
        datetextView.setText(new StringBuilder().append(current.get(Calendar.DAY_OF_MONTH)).append('/')
                .append(current.get(Calendar.MONTH) + 1).append('/')
                .append(current.get(Calendar.YEAR)).toString());
        datetextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });
        itemEntryList.setAdapter(itemEntryAdapter = new ReceiptItemEntryAdapter(this, receiptMove));

        itemEntryAdapter.setAddButtonClick(new ItemEntry_w_add.OnAddButtonClick() {
            @Override
            public void onAddClick(ItemEntry entry, int listPosition, ClickType clickType) {
                switch (clickType) {
                    case ADD:
                        addItemEntry.showWithNoEdit(findViewById(R.id.action_bar), entry);
                        break;
                    case SET:
                        addItemEntry.show(findViewById(R.id.action_bar), entry);
                        break;
                }
            }
        });
        itemEntryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (itemEntryAdapter.isAddButton(position)) {
                    addItemEntry.show(findViewById(R.id.action_bar));
                } else {
                    if (parent.getAdapter() instanceof ReceiptItemEntryAdapter) {
                        ItemEntry entry = ((ReceiptItemEntryAdapter) parent.getAdapter()).getItemEntry(position);
                        addItemEntry.show(findViewById(R.id.action_bar), entry);
                    }
                }

            }
        });
        itemEntryList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getAdapter() instanceof ReceiptItemEntryAdapter
                        && !((ReceiptItemEntryAdapter) parent.getAdapter()).isAddButton(position)) {
                    selectedMenu = new ContextMenu();
                    selectedMenu.type = Type.DELETE_ITEM_ENTRY;
                    selectedMenu.position = position;
                    onOpenListViewContextMenu(view, position, 1);
                }
                return true;
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                receiptMove.setCustomDate(current.getTime());
                final Intent rresult = new Intent();
                //result.putExtra(ReceiptMove.ACCESS, receiptMove);
                result.setArg(receiptMove);
                setResult(Activity.RESULT_OK, rresult);
                args.clear();
                finish();
            }
        });
        toolbar.addView(accept);
        toolbar.addView(cancel);
        toolbar.addView(menu);
        //TODO изменить отступ на подходящий для разных рамеров экрана расчет
        menu.setPadding(55, 0, 0, 0);

        addItemEntry.setOnItemChanged(new ItemEntryLayoutHandler.OnItemChanged() {
            @Override
            public void onChanged(ItemEntry entry, boolean edited) {
                if (!edited) {
                    receiptMove.addItem(entry);
                }
                isEdited = true;
                itemEntryAdapter.notifyDataSetChanged();
                updateAllCount();
            }
        });
        menuAdditional = new PopupMenu(this, menu);
        menuAdditional.setOnMenuItemClickListener(this);
        menuAdditional.inflate(R.menu.additional_receipt_menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuAdditional.show();
            }
        });

        closeAlert = new AlertDialog.Builder(this);
        closeAlert.setTitle(getString(R.string.alert_close_head))
                .setMessage(getString(R.string.alert_close))
                .setPositiveButton(getString(R.string.answ_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.answ_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
    }

    @Override
    public void finish() {
        args.clear();
        super.finish();
    }

    @Override
    public void onBackPressed() {
        if(popupWindow.isShowing()) {
            popupWindow.dismiss();
        }
        else if(isEdited) {
            //closeAlert.show();
            accept.callOnClick();
        } else {
            setResult(Activity.RESULT_CANCELED);
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_MENU && event.getAction() == KeyEvent.ACTION_DOWN)
            return menu.callOnClick();
        else
            return super.onKeyDown(keyCode, event);
    }

    private void initFields(ReceiptMove move) {
        current = new GregorianCalendar();
        current.setTime(move.getCustomDate());
        datetextView.setText(new StringBuilder().append(current.get(Calendar.DAY_OF_MONTH)).append('/')
                .append(current.get(Calendar.MONTH) + 1).append('/')
                .append(current.get(Calendar.YEAR)).toString());
        updateAllCount();
    }

    private void onOpenListViewContextMenu(View view, int position, int group) {
        contextMenu = new PopupMenu(this, view);
        contextMenu.setOnMenuItemClickListener(this);
        contextMenu.getMenu().add(group, 0, position, getString(R.string.text_delete));
        contextMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();
        switch (item.getGroupId()) {
            case 1:
                receiptMove.removeItem(itemEntryAdapter.getItemEntry(item.getOrder()));
                isEdited = true;
                ((ListView) findViewById(R.id.listView2)).setAdapter(itemEntryAdapter);
                break;
            case 2:
                receiptMove.removeProviderEntry((ReceiptMove.ProviderEntry) providerEntryAdapter.getItem(item.getOrder()));
                isEdited = true;
                ((ListView) findViewById(R.id.listView)).setAdapter(providerEntryAdapter);
                break;
            default:
                switch (id) {
                    case R.id.create_shoes_item:
                        Main2Activity.once.startCreateShoesItemActivity();
                        break;
                    case R.id.send_receipt_to:
                        final Intent sender = new Intent();
                        sender.setAction(Intent.ACTION_SEND);
                        sender.putExtra(Intent.EXTRA_TEXT, receiptMove.toSenderString());
                        sender.setType("text/plain");
                        startActivity(Intent.createChooser(sender, getString(R.string.menu_send_receipt)));
                        break;
                }
        }

        return true;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        datetextView.setText(new StringBuilder().append(dayOfMonth).append('/')
                .append(monthOfYear + 1).append('/')
                .append(year).toString());
        current.set(year, monthOfYear, dayOfMonth);
        isEdited = true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Main2Activity.ResultType resultType = Main2Activity.ResultType.getResultType(resultCode);
        if(resultType != null) {
            ItemEntry entry = (ItemEntry) data.getSerializableExtra(ItemEntry.ACCESS);
            switch (resultType) {
                case CREATE_ITEM_ENTRY:
                    //itemEntryAdapter.addEntry(entry);
                    entry.setParentID(receiptMove.getID());
                    receiptMove.addItem(entry);
                    isEdited = true;
                    ((ListView) findViewById(R.id.listView2)).setAdapter(itemEntryAdapter);
                    break;
            }
        } else {
            switch (resultCode) {
                case MakeProviderEntry.RETURN_TYPE_CREATE:
                    final ReceiptMove.ProviderEntry providerEntry = (ReceiptMove.ProviderEntry) data.getSerializableExtra(ReceiptMove.ProviderEntry.ACCESS);
                    receiptMove.addProvider(providerEntry);
                    isEdited = true;
                    ((ListView) findViewById(R.id.listView)).setAdapter(providerEntryAdapter);
                    break;
                case MakeProviderEntry.RETURN_TYPE_EDIT:
                    final ReceiptMove.ProviderEntry providerEntry1 = (ReceiptMove.ProviderEntry) data.getSerializableExtra(ReceiptMove.ProviderEntry.ACCESS);
                    for(ReceiptMove.ProviderEntry entry : receiptMove.getProviderEntries()) {
                        if (entry.getUniqueID() == providerEntry1.getUniqueID()) {
                            entry.insertDataFrom(providerEntry1);
                            break;
                        }
                    }
                    isEdited = true;
                    ((ListView) findViewById(R.id.listView)).setAdapter(providerEntryAdapter);
                    break;
            }
        }
        updateAllCount();
    }

    public void startEditProviderEntryActivity(ReceiptMove.ProviderEntry entry) {
        Intent intent = new Intent(this, MakeProviderEntry.class);
        intent.putExtra("MoveID", receiptMove.getID());
        intent.putExtra(ReceiptMove.ProviderEntry.ACCESS, entry);
        startActivityForResult(intent, MakeProviderEntry.RETURN_TYPE_EDIT);
    }

    public void startMakeProviderEntryActivity() {
        Intent intent = new Intent(this, MakeProviderEntry.class);
        intent.putExtra("MoveID", receiptMove.getID());
        startActivityForResult(intent, MakeProviderEntry.RETURN_TYPE_CREATE);
    }

    public void updateAllCount() {
        allCount.setText(new StringBuilder().append(receiptMove.getCountForProvider()).append('/').append(receiptMove.getCount()).toString());
    }

    private class ContextMenu {
        public int position;
        Type type;
    }

    public enum LayoutVariant {
        WITH_PROVIDER,
        WITH_STORAGE
    }
    enum Type {
        DELETE_ITEM_ENTRY,
        DELETE_PROVIDER;
    }

    private class StorageSpinnerAdapter implements SpinnerAdapter {

        private StorageList storageList;
        private Context parent;

        public StorageSpinnerAdapter(StorageList storageList, Context parent) {
            this.storageList = storageList;
            this.parent = parent;
        }

        @Override
        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            TextView textView = new TextView(parent);
            Storage item = (Storage) getItem(i);
            textView.setText(item.getStorageName());
            textView.setMinHeight(20);
            return textView;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver dataSetObserver) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

        }

        @Override
        public int getCount() {
            return storageList.getStorages().size();
        }

        @Override
        public Object getItem(int i) {
            return storageList.getStorages().get(i);
        }

        @Override
        public long getItemId(int i) {
            return storageList.getStorages().get(i).getID();
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            return getDropDownView(i, view, viewGroup);
        }

        @Override
        public int getItemViewType(int i) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return storageList.getStorages().isEmpty();
        }
    }
}
