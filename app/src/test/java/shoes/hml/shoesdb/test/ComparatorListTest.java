package shoes.hml.shoesdb.test;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import shoes.hml.shoesdb.handlers.comparators.ComparatorList;

import static org.junit.Assert.*;

/**
 * Created by maximusfk on 03.10.16.
 */
public class ComparatorListTest {
    class TestString implements Comparator<String> {

        @Override
        public int compare(String s, String t1) {
            return s.compareTo(t1);
        }
    }
    class TestStringLength implements Comparator<String> {

        @Override
        public int compare(String s, String t1) {
            return s.length() == t1.length() ? 0 :
                    s.length() < t1.length() ? -1 : 1;
        }
    }
    @Test
    public void compare() throws Exception {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("Test1");
        strings.add("Test2");
        strings.add("Test15");
        strings.add("Test3");
        strings.add("Test4");
        strings.add("Test5");
        strings.add("Test7");
        strings.add("Test8");
        strings.add("Test6");
        strings.add("Test9");
        strings.add("Test11");
        strings.add("Test12");
        strings.add("Test13");
        strings.add("Test14");
        strings.add("Test10");
        strings.add("Test16");
        strings.add("Test17");
        for(String s : strings)
            System.out.println(s);
        Collections.sort(strings, new ComparatorList<>(new TestStringLength(), new TestString()));
        for(String s : strings)
            System.out.println(s);
    }

}